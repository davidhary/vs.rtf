Imports System.Reflection
Imports System.Resources

<Assembly: AssemblyCompany("Integrated Scientific Resources")>
<Assembly: AssemblyCopyright("(c) 2008 Razi Syed. All rights reserved.")>
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyCulture("")>
