﻿Imports System.Drawing.Printing
Imports System.ComponentModel
Imports isr.Controls.RichTextFormat.RichTextBoxExtensions

Public Class EditorForm

    '   font-size change options
    Private Enum SelectionOption
        NoAction
        Add1
        Subtract1
        MultiplyBy2
        DivideBy2
    End Enum

    '    private variables
    Private _KeyPressed As Keys = 0

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    ''' <summary> Form 1 load. Set up events for underlying rich-text box. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '   catch certain events of underlying rich-text box
        With Me._RichTextFormatControl.RichTextBox
            AddHandler .DoubleClick, AddressOf HandleRichTextControlDoubleClick
            AddHandler .SelectionChanged, AddressOf HandleRichTextFormatControlSelectionChanged
            AddHandler .TextChanged, AddressOf HandleRichTextFormatControlSelectionChanged
            AddHandler .HScroll, AddressOf HandleRichTextFormatControlSelectionChanged
            AddHandler .VScroll, AddressOf HandleRichTextFormatControlSelectionChanged
            .EnableAutoDragDrop = True
            .AllowDrop = True
        End With
    End Sub

    ''' <summary> Enables the spell check based on the box checked state. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EnableSpellCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EnableSpellCheckBox.CheckedChanged
        _RichTextFormatControl.AllowSpellCheck = DirectCast(sender, CheckBox).Checked
    End Sub

    ''' <summary> Enables the custom links based on the check box checked state. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EnableCustomLinksCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EnableCustomLinksCheckBox.CheckedChanged
        _RichTextFormatControl.DoCustomLinks = DirectCast(sender, CheckBox).Checked
    End Sub


    ''' <summary> Handles the rich text control double click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleRichTextControlDoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try

            activity = "printing"
            Me._RichTextFormatControl.Print(True)
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex.ToString}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Handles the rich text format control selection changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleRichTextFormatControlSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '   display character codes of selected text and scroll position
        With _RichTextFormatControl.RichTextBox
            Dim s As String = .SelectedText
            Dim ss As Integer = .SelectionStart, sl As Integer = .SelectionLength
            Dim c As String = $"Maximum Text Width: { .GetMaximumWidth()}{ControlChars.CrLf} Scroll Position: { .GetScrollPosition.X},{ .GetScrollPosition.Y}"
            c = $"{c}{ControlChars.CrLf}Codes for SELECTED characters: "
            For Index As Integer = 0 To sl - 1
                c = $"{c}{Char.ConvertToUtf32(s, Index)} "
            Next
            c = c.Trim()
            _InfoTextBox.Text = c
        End With
    End Sub

    ''' <summary>
    ''' Event handler. Called by _RichTextFormatControl for insert RTF text events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Insert RTF text event information. </param>
    Private Sub HandleInsertRtfText(ByVal sender As Object, ByVal e As isr.Controls.RichTextFormat.InsertRtfTextEventArgs) Handles _RichTextFormatControl.InsertRtfText
        '   take care of allowing fractions and printing
        If e.KeyEventArgs.Control Then
            Select Case e.KeyEventArgs.KeyCode
                Case Keys.D2, Keys.NumPad2
                    '   [Ctrl] + [2] = 1/2 ("½")
                    e.RtfText = "½"
                Case Keys.D3, Keys.NumPad3
                    '   [Ctrl] + [3] = 3/4 ("¾")
                    e.RtfText = "¾"
                Case Keys.D4, Keys.NumPad4
                    '   [Ctrl] + [4] = 1/4 ("¼")
                    e.RtfText = "¼"
                Case Keys.Enter
                    If e.KeyEventArgs.Shift Then
                        '   [Ctrl] + [Shift] + [Enter] = page break
                        e.RtfText = "\page"
                    End If
                Case Keys.P
                    '   [Ctrl] + [P] = print text
                    HandleRichTextControlDoubleClick(Me._RichTextFormatControl.RichTextBox, New EventArgs())
                    e.RtfText = String.Empty
                    e.KeyEventArgs.SuppressKeyPress = True
                Case Keys.S
                    '   [Ctrl] + [S] = save text
                    Me._RichTextFormatControl.SaveFile(Me._RichTextFormatControl.FileName)
                    e.RtfText = String.Empty
                    e.KeyEventArgs.SuppressKeyPress = True
                Case Keys.O
                    '   [Ctrl] + [O] = load text
                    Dim args As CancelEventArgs = New CancelEventArgs()
                    Me._RichTextFormatControl.TryCloseFile(args)
                    If Not args.Cancel Then Me._RichTextFormatControl.LoadFile()
                    e.RtfText = String.Empty
                    e.KeyEventArgs.SuppressKeyPress = True
                Case Keys.Oemcomma, Keys.OemPeriod
                    '   [Ctrl] + [.] = add 1 point to font size,
                    '   [Ctrl] + [Shift] + [>] = double font size
                    '   [Ctrl] + [,] = subtract 1 point from font size,
                    '   [Ctrl] + [Shift] + [<] = halve font size
                    Dim Action As SelectionOption = SelectionOption.NoAction
                    If e.KeyEventArgs.KeyCode = Keys.Oemcomma Then
                        '   smaller font
                        If e.KeyEventArgs.Shift Then
                            Action = SelectionOption.DivideBy2
                        Else
                            Action = SelectionOption.Subtract1
                        End If
                    Else
                        '   bigger font
                        If e.KeyEventArgs.Shift Then
                            Action = SelectionOption.MultiplyBy2
                        Else
                            Action = SelectionOption.Add1
                        End If
                    End If
                    '   change font size
                    _RichTextFormatControl.EditWithLinksUnprotected(Action)
                    If Action = SelectionOption.NoAction Then
                        Beep()
                        MessageBox.Show("Font size can't go any further!")
                    End If
                    _KeyPressed = 0
                    e.RtfText = String.Empty
                    e.KeyEventArgs.SuppressKeyPress = True
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by _RichTextFormatControl for smart RTF text events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Smart RTF text event information. </param>
    Private Sub HandleSmartRtfText(ByVal sender As Object, ByVal e As SmartRtfTextEventArgs) Handles _RichTextFormatControl.SmartRtfText
        With _RichTextFormatControl
            '   get preceding text
            Dim PrecedingText As String = String.Empty
            If .RichTextBox.SelectionStart > 1 Then
                PrecedingText = .Text.Substring(.RichTextBox.SelectionStart - 2, 2)
            End If
            '   see if we're to make a fraction
            Select Case e.KeyPressEventArgs.KeyChar
                Case "2"c
                    If PrecedingText = "1/" Then
                        '   "1/2" = "½"
                        e.RtfText = "½"
                        e.PrecedingCharacterCount = 2
                    End If
                Case "4"c
                    If PrecedingText = "1/" Then
                        '   "1/4" = "¼"
                        e.RtfText = "¼"
                        e.PrecedingCharacterCount = 2
                    ElseIf PrecedingText = "3/" Then
                        '   "3/4" = "¾"
                        e.RtfText = "¾"
                        e.PrecedingCharacterCount = 2
                    End If
            End Select
        End With
    End Sub

    ''' <summary>
    ''' Event handler. Called by _RichTextFormatControl for hyperlink clicked events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Custom link information event information. </param>
    Private Sub HandleHyperlinkClicked(ByVal sender As Object, ByVal e As CustomLinkInfoEventArgs) Handles _RichTextFormatControl.HyperlinkClicked
        '   see if user wants to follow link
        If e.CustomLinkInfo IsNot Nothing Then
            Dim Result As DialogResult =
            MessageBox.Show($"This link goes to '{e.CustomLinkInfo.Hyperlink}'{ControlChars.CrLf}Follow it?", "Link Clicked", MessageBoxButtons.YesNo)
            If Result = DialogResult.Yes Then
                Process.Start(e.CustomLinkInfo.Hyperlink)
            End If
        End If
    End Sub

    ''' <summary> Form 1 closing. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub Form1_Closing(ByVal sender As Object, ByVal e As CancelEventArgs) Handles Me.Closing
        Me._RichTextFormatControl.TryCloseFile(e)
    End Sub

    ''' <summary>
    ''' Event handler. Called by _RichTextFormatControl for editing with links unprotected events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Parameter event information. </param>
    Private Sub HandleEditingWithLinksUnprotected(ByVal sender As Object, ByVal e As ParameterEventArgs) Handles _RichTextFormatControl.EditingWithLinksUnprotected
        '   change size of highlighted text
        '      [Ctrl] + [,] = subtract 1 point, [Ctrl] + [.] = add 1 point,
        '      [Ctrl] + [Shift] + [<] = divide by 2, [Ctrl] + [Shift] + [>] = multiply by 2
        With _RichTextFormatControl.RichTextBox
            '   get font
            '.SelectedText = "ME"
            ' Exit Sub
            Dim font As Font = .SelectionFont
            If font Is Nothing Then
                '   mixed font--get font at beginning of selection
                .SetRedrawMode(False)
                Dim length As Integer = .SelectionLength
                .SelectionLength = 0
                font = .SelectionFont
                .SelectionLength = length
                .SetRedrawMode(True)
            End If
            '   change font size
            Try
                Select Case DirectCast(e.Parameters, SelectionOption)
                    Case SelectionOption.Add1
                        .SelectionFont = New Font(font.Name, font.SizeInPoints + 1D, font.Style)
                    Case SelectionOption.Subtract1
                        .SelectionFont = New Font(font.Name, font.SizeInPoints - 1D, font.Style)
                    Case SelectionOption.MultiplyBy2
                        .SelectionFont = New Font(font.Name, font.SizeInPoints * 2D, font.Style)
                    Case SelectionOption.DivideBy2
                        .SelectionFont = New Font(font.Name, font.SizeInPoints / 2D, font.Style)
                End Select
            Catch ex As Exception
                '   can't go further
                e.Parameters = SelectionOption.NoAction
            End Try
        End With
    End Sub

    ''' <summary> Event handler. Called by _RichTextFormatControl for drag drop events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>
    Private Sub HandleDragDrop(ByVal sender As Object, ByVal e As DragEventArgs) Handles _RichTextFormatControl.DragDrop
        With _RichTextFormatControl
            Dim x = 0
            Dim y = 1 \ x
            If .AutoDragDropInProgress Then
                Beep()
                Exit Sub 'internal text-box drop--don't handle this
            End If
            If e.Data.GetDataPresent(GetType(String)) Then
                .RichTextBox.SelectedText = e.Data.GetData(GetType(String)).ToString
            End If
            e.Effect = DragDropEffects.None 'indicate no further handling necessary
        End With
    End Sub

    ''' <summary> Information text box mouse move. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub InfoTextBox_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _InfoTextBox.MouseMove
        If e.Button <> MouseButtons.None AndAlso Not _InfoTextBox.DisplayRectangle.Contains(e.X, e.Y) Then
            _InfoTextBox.DoDragDrop(Me._RichTextFormatControl.RichTextBox.EscapedRTFText(_InfoTextBox.Text), DragDropEffects.Move)
        End If
    End Sub

    ''' <summary> Event handler. Called by _RichTextFormatControl for drag over events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>

    Private Sub HandleDragOver(ByVal sender As Object, ByVal e As DragEventArgs) Handles _RichTextFormatControl.DragOver
        Const CtrlKeyPressed As Integer = 8
        If e.KeyState And CtrlKeyPressed Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.Move
        End If
    End Sub
End Class

#Region " UNUSED "
#If False Then

        Me._RichTextFormatControl.Rtf = "{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}}" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "\" &
    "viewkind4\uc1\pard\fs20\par" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "}" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)


    ''' <summary> Form 1 resize. Size extended rich-text box to form. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        _RichTextFormatControl.SetBounds(0, 0, Me.ClientSize.Width,
                                         Me.ClientSize.Height - _EnableSpellCheckBox.Height - _EnableCustomLinksCheckBox.Height - _InfoTextBox.Height)
        _EnableSpellCheckBox.SetBounds((Me.ClientSize.Width - _EnableSpellCheckBox.Width) \ 2, _RichTextFormatControl.Bottom,
                                       _EnableSpellCheckBox.Width, _EnableSpellCheckBox.Height)
        _EnableCustomLinksCheckBox.SetBounds((Me.ClientSize.Width - _EnableCustomLinksCheckBox.Width) \ 2, _EnableSpellCheckBox.Bottom,
                                             _EnableCustomLinksCheckBox.Width, _EnableCustomLinksCheckBox.Height)
        _InfoTextBox.SetBounds((Me.ClientSize.Width - _InfoTextBox.Width) \ 2, _EnableCustomLinksCheckBox.Bottom,
                               _InfoTextBox.Width, _InfoTextBox.Height)
        _RichTextFormatControl.RichTextBox.ShowSelectionMargin = True
        _RichTextFormatControl.RichTextBox.WordWrap = False
        _RichTextFormatControl.IsTextChanged = False
    End Sub
#End If
#End Region
