﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditorForm
	 Inherits System.Windows.Forms.Form

	 'Form overrides dispose to clean up the component list.
	 <System.Diagnostics.DebuggerNonUserCode()> _
	 Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		  Try
				If disposing AndAlso components IsNot Nothing Then
					 components.Dispose()
				End If
		  Finally
				MyBase.Dispose(disposing)
		  End Try
	 End Sub

	 'Required by the Windows Form Designer
	 Private components As System.ComponentModel.IContainer

	 'NOTE: The following procedure is required by the Windows Form Designer
	 'It can be modified using the Windows Form Designer.  
	 'Do not modify it using the code editor.
	 <System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditorForm))
        Me._PrintDialog = New System.Windows.Forms.PrintDialog()
        Me._PrintPreviewDialog = New System.Windows.Forms.PrintPreviewDialog()
        Me._EnableSpellCheckBox = New System.Windows.Forms.CheckBox()
        Me._EnableCustomLinksCheckBox = New System.Windows.Forms.CheckBox()
        Me._PageSetupDialog = New System.Windows.Forms.PageSetupDialog()
        Me._InfoTextBox = New System.Windows.Forms.TextBox()
        Me._RichTextFormatControl = New isr.Controls.RichTextFormat.RichTextFormatControl()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._ControlsLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._BottomLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._Layout.SuspendLayout()
        Me._ControlsLayout.SuspendLayout()
        Me._BottomLayout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_PrintDialog
        '
        Me._PrintDialog.UseEXDialog = True
        '
        '_PrintPreviewDialog
        '
        Me._PrintPreviewDialog.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me._PrintPreviewDialog.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me._PrintPreviewDialog.ClientSize = New System.Drawing.Size(400, 300)
        Me._PrintPreviewDialog.Enabled = True
        Me._PrintPreviewDialog.Icon = CType(resources.GetObject("_PrintPreviewDialog.Icon"), System.Drawing.Icon)
        Me._PrintPreviewDialog.Name = "_PrintPreviewDialog"
        Me._PrintPreviewDialog.Visible = False
        '
        '_EnableSpellCheckBox
        '
        Me._EnableSpellCheckBox.AutoSize = True
        Me._EnableSpellCheckBox.Checked = True
        Me._EnableSpellCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._EnableSpellCheckBox.Location = New System.Drawing.Point(75, 3)
        Me._EnableSpellCheckBox.Name = "_EnableSpellCheckBox"
        Me._EnableSpellCheckBox.Size = New System.Drawing.Size(119, 17)
        Me._EnableSpellCheckBox.TabIndex = 1
        Me._EnableSpellCheckBox.Text = "Enable &Spell Check"
        Me._EnableSpellCheckBox.UseVisualStyleBackColor = True
        '
        '_EnableCustomLinksCheckBox
        '
        Me._EnableCustomLinksCheckBox.AutoSize = True
        Me._EnableCustomLinksCheckBox.Checked = True
        Me._EnableCustomLinksCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._EnableCustomLinksCheckBox.Location = New System.Drawing.Point(200, 3)
        Me._EnableCustomLinksCheckBox.Name = "_EnableCustomLinksCheckBox"
        Me._EnableCustomLinksCheckBox.Size = New System.Drawing.Size(125, 17)
        Me._EnableCustomLinksCheckBox.TabIndex = 2
        Me._EnableCustomLinksCheckBox.Text = "Enable &Custom Links"
        Me._EnableCustomLinksCheckBox.UseVisualStyleBackColor = True
        '
        '_InfoTextBox
        '
        Me._InfoTextBox.Location = New System.Drawing.Point(182, 36)
        Me._InfoTextBox.Multiline = True
        Me._InfoTextBox.Name = "_InfoTextBox"
        Me._InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._InfoTextBox.Size = New System.Drawing.Size(400, 54)
        Me._InfoTextBox.TabIndex = 3
        '
        '_RichTextFormatControl
        '
        Me._RichTextFormatControl.AllowDefaultInsertText = True
        Me._RichTextFormatControl.AllowDefaultSmartText = True
        Me._RichTextFormatControl.AllowDrop = True
        Me._RichTextFormatControl.AllowHyphenation = True
        Me._RichTextFormatControl.AllowLists = True
        Me._RichTextFormatControl.AllowPictures = True
        Me._RichTextFormatControl.AllowSpellCheck = True
        Me._RichTextFormatControl.AllowSymbols = True
        Me._RichTextFormatControl.AllowTabs = True
        Me._RichTextFormatControl.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me._RichTextFormatControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._RichTextFormatControl.DoCustomLinks = True
        Me._RichTextFormatControl.FilePath = String.Empty
        Me._RichTextFormatControl.KeepHypertextOnRemove = False
        Me._RichTextFormatControl.Location = New System.Drawing.Point(6, 3)
        Me._RichTextFormatControl.MaintainSelection = False
        Me._RichTextFormatControl.Name = "_RichTextFormatControl"
        Me._RichTextFormatControl.RightMargin = 705
        Me._RichTextFormatControl.SetColorWithFont = True
        Me._RichTextFormatControl.ShowRuler = True
        Me._RichTextFormatControl.ShowToolStrip = True
        Me._RichTextFormatControl.Size = New System.Drawing.Size(764, 303)
        Me._RichTextFormatControl.SkipLinksOnReplace = False
        Me._RichTextFormatControl.TabIndex = 4
        Me._RichTextFormatControl.UnitsForRuler = isr.Controls.RichTextFormat.RulerUnitType.Inches
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.Controls.Add(Me._BottomLayout, 1, 1)
        Me._Layout.Controls.Add(Me._RichTextFormatControl, 1, 0)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 2
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.Size = New System.Drawing.Size(776, 415)
        Me._Layout.TabIndex = 5
        '
        '_ControlsLayout
        '
        Me._ControlsLayout.ColumnCount = 4
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ControlsLayout.Controls.Add(Me._EnableSpellCheckBox, 1, 0)
        Me._ControlsLayout.Controls.Add(Me._EnableCustomLinksCheckBox, 2, 0)
        Me._ControlsLayout.Dock = System.Windows.Forms.DockStyle.Top
        Me._ControlsLayout.Location = New System.Drawing.Point(182, 3)
        Me._ControlsLayout.Name = "_ControlsLayout"
        Me._ControlsLayout.RowCount = 2
        Me._ControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ControlsLayout.Size = New System.Drawing.Size(400, 27)
        Me._ControlsLayout.TabIndex = 6
        '
        '_BottomLayout
        '
        Me._BottomLayout.ColumnCount = 3
        Me._BottomLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._BottomLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._BottomLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._BottomLayout.Controls.Add(Me._InfoTextBox, 1, 1)
        Me._BottomLayout.Controls.Add(Me._ControlsLayout, 1, 0)
        Me._BottomLayout.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._BottomLayout.Location = New System.Drawing.Point(6, 312)
        Me._BottomLayout.Name = "_BottomLayout"
        Me._BottomLayout.RowCount = 3
        Me._BottomLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._BottomLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._BottomLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._BottomLayout.Size = New System.Drawing.Size(764, 100)
        Me._BottomLayout.TabIndex = 7
        '
        'EditorForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(776, 415)
        Me.Controls.Add(Me._Layout)
        Me.Name = "EditorForm"
        Me.Text = "Form1"
        Me._Layout.ResumeLayout(False)
        Me._ControlsLayout.ResumeLayout(False)
        Me._ControlsLayout.PerformLayout()
        Me._BottomLayout.ResumeLayout(False)
        Me._BottomLayout.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _PrintDialog As System.Windows.Forms.PrintDialog
    Private WithEvents _PrintPreviewDialog As System.Windows.Forms.PrintPreviewDialog
    Private WithEvents _EnableSpellCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _EnableCustomLinksCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PageSetupDialog As System.Windows.Forms.PageSetupDialog
    Private WithEvents _InfoTextBox As System.Windows.Forms.TextBox
    Private WithEvents _RichTextFormatControl As isr.Controls.RichTextFormat.RichTextFormatControl
    Friend WithEvents _Layout As TableLayoutPanel
    Friend WithEvents _BottomLayout As TableLayoutPanel
    Friend WithEvents _ControlsLayout As TableLayoutPanel
End Class
