﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        Public Const AssemblyTitle As String = "Rich Text Format Library"
        Public Const AssemblyDescription As String = "Rich Text Format Library"
        Public Const AssemblyProduct As String = "Controls.RichTextFormat"

    End Class

End Namespace
