﻿
Imports System.Runtime.InteropServices
Imports System.Drawing.Printing
Imports System.Runtime.CompilerServices
Imports System.Text

Namespace RichTextBoxExtensions

    Partial Public Module Methods

#Region " SCROLLING/TEXT-WIDTH FUNCTIONS "

        ''' <summary> Gets font at character position. </summary>
        ''' <remarks> NOTE: Selection isn't changed if it's already where it needs to be,
        '''           lest recursion result if GetRightMostCharacterPosition is called within
        '''           the control's SelectionChanged event and recursion results!
        ''' </remarks>
        ''' <param name="richTextBox"> The rich control. </param>
        ''' <param name="charPos">     The character position. </param>
        ''' <returns> The font at character position. </returns>
        Private Function GetFontAtCharacterPosition(ByVal richTextBox As RichTextBox, ByVal charPos As Integer) As Font
            With richTextBox
                Dim charFont As Font = Nothing
                If .SelectionStart = charPos AndAlso .SelectionLength = 0 Then
                    '   temporarily change selection
                    Dim currentSS As Integer = .SelectionStart
                    Dim currentSL As Integer = .SelectionLength
                    .SuspendLayout()
                    .Select(charPos, 0)
                    charFont = .SelectionFont
                    .Select(currentSS, currentSL)
                    .ResumeLayout()
                Else
                    charFont = .SelectionFont
                End If
                Return _
                    charFont
            End With
        End Function

        ''' <summary> Gets right most character position. 
        '''           Gets width of widest line in text</summary>
        ''' <param name="richTextBox"> . </param>
        ''' <returns> The right most character position. </returns>
        Private Function GetRightMostCharacterPosition(ByVal richTextBox As RichTextBox) As Integer
            Dim rightMostPos As Integer = 0
            Dim lineLength, lineWidth, lineIndex, charIndex, charWidth As Integer
            Dim line, character As String
            Dim charFont As Font = Nothing
            With richTextBox
                '   go through each line
                For LineNumber As Integer = 0 To .Lines.GetUpperBound(0)
                    '   how long is this line?
                    line = .Lines(LineNumber)
                    lineLength = line.Length
                    If lineLength > 0 Then
                        '   get rightmost character in line
                        lineIndex = .GetFirstCharIndexFromLine(LineNumber)
                        charIndex = lineIndex + lineLength - 1
                        character = line.Substring(lineLength - 1, 1)
                        '   get width up to rightmost char
                        lineWidth = .GetPositionFromCharIndex(charIndex).X - .GetPositionFromCharIndex(lineIndex).X
                        '   get size of char
                        charFont = Methods.GetFontAtCharacterPosition(richTextBox, charIndex + 1)
                        Using g As Graphics = .CreateGraphics()
                            charWidth = CInt(.CreateGraphics.MeasureString(character, charFont).Width * richTextBox.ZoomFactor)
                        End Using
                        lineWidth += charWidth
                        '   see if new line is longer than previous ones
                        If lineWidth > rightMostPos Then
                            rightMostPos = lineWidth
                        End If
                    End If
                Next LineNumber
            End With
            Return rightMostPos
        End Function

#End Region

#Region " SCROLLING/TEXT-WIDTH METHODS "

        ''' <summary>
        ''' Get scroll position of RichTextBox
        ''' </summary>
        ''' <returns>Point structure containing current horizontal (.x)
        ''' and vertical (.y) scroll positions in pixels</returns>
        ''' <remarks></remarks>
        <Extension()>
        Public Function GetScrollPosition(ByVal richTextBox As RichTextBox) As Point
            Return SafeNativeMethods.GetScrollPosition(richTextBox.Handle)
        End Function

        ''' <summary>
        ''' Set scroll position of RichTextBox
        ''' </summary>
        ''' <param name="richTextBox"></param>
        ''' <param name="scrollPoint">Point structure containing new horizontal (.x)
        ''' and vertical (.y) scroll positions in pixels</param>
        ''' <remarks></remarks>
        <Extension()>
        Public Sub SetScrollPosition(ByVal richTextBox As RichTextBox, ByVal scrollPoint As Point)
            SafeNativeMethods.SetScrollPosition(richTextBox.Handle, scrollPoint)
        End Sub

        ''' <summary>
        ''' Get information about a RichTextBox scroll bar
        ''' </summary>
        ''' <param name="scrollBarType">ScrollBarType value (.Horizontal or .Vertical)</param>
        ''' <param name="ScrollBarMask">ScrollBarMask flags indicating what to get
        ''' (range, page size, position, track position; defaults to everything)</param>
        ''' <returns>ScrollInfo structure with requested info</returns>
        ''' <remarks></remarks>
        <Extension()>
        Private Function GetScrollBarInfo(ByVal richTextBox As RichTextBox, ByVal scrollBarType As UnsafeNativeMethods.ScrollBarType,
            Optional ScrollBarMask As UnsafeNativeMethods.ScrollBarMask = UnsafeNativeMethods.ScrollBarMask.Everything) As UnsafeNativeMethods.ScrollInfo
            Return SafeNativeMethods.GetScrollBarInfo(richTextBox.Handle, scrollBarType, ScrollBarMask)
        End Function

        ''' <summary>
        ''' Get effective maximum text width of RichTextBox in pixels
        ''' </summary>
        ''' <returns>Maximum available physical width for any text.
        ''' (-1 if we're in a recursive loop--see remarks)</returns>
        ''' <remarks>This value is calculated as follows:
        ''' 1. If control's RightMargin property is non-zero, then that us used
        ''' 2. Otherwise, if WordWrap is True, then the control's client-area width
        '''    minus any left-edge "selection" margin is used
        ''' 3. Otherwise, if horizontal scrollbars are enabled, then the "maximum horizontal
        '''    scroll position" plus the client width, or the width of the longest physical line,
        '''    whichever is longer, is used
        ''' 4. Otherwise, the width of the longest physical line is used</remarks>
        <Extension()>
        Public Function GetMaximumWidth(ByVal richTextBox As RichTextBox) As Integer
            With richTextBox
                '   see if text width is fixed
                If .RightMargin > 0 Then
                    '   yes, so return with fixed width
                    Return .RightMargin
                End If
                '   else start with width of text box
                Dim selectionOffset As Integer = + .Padding.Left + 1
                Dim maxTextWidth As Integer = .ClientRectangle.Width
                If .ShowSelectionMargin Then
                    '   account for any selection margin
                    selectionOffset += CInt(.ZoomFactor * SelectionMargin)
                End If
                maxTextWidth -= CInt(.ZoomFactor * SelectionMargin)
                '   determine maximum line width
                If Not .WordWrap Then
                    If (.ScrollBars And RichTextBoxScrollBars.Horizontal) _
                                = RichTextBoxScrollBars.Horizontal Then
                        '   determine rightmost character position from scrollbar info
                        maxTextWidth += .GetScrollBarInfo(UnsafeNativeMethods.ScrollBarType.Horizontal).MaximumScrollPosition
                    End If
                    '   make sure value is at least as large as widest line
                    maxTextWidth = Math.Max(maxTextWidth, GetRightMostCharacterPosition(richTextBox))
                End If
                '   return value
                Return maxTextWidth
            End With
        End Function

#End Region

#Region " RTF METHODS "

        ''' <summary>
        ''' Mark selected text in RichTextBox as a list, using ListStyle
        ''' </summary>
        ''' <param name="listStyle">Style of list (no list, bullets, numbers, lowercase letters,
        ''' uppercase letters, lowercase Roman numerals, or uppercase Roman numerals)</param>
        ''' <remarks>RichTextBox.SelectionBullet returns False for any list style other than
        ''' bullets when read; there is no easy way to GET the specific list-style of selected
        ''' text even when parsing RTF code (RichTextBox.SelRTF), since, for instance, "I" can
        ''' just as easily be Roman numeral for 1 OR letter "I", given the way the control
        ''' defines lists</remarks>
        <Extension()>
        Public Sub SetListStyle(ByVal richTextBox As RichTextBox, ByVal listStyle As RichTextFormatListStyle)
            With richTextBox
                '   get active control
                Dim activeControl As Control = .GetContainerControl.ActiveControl
                '   set keyboard focus to rich-text box
                .SuspendLayout()
                .Select()
                '   set list style
                Select Case listStyle
                    Case RichTextFormatListStyle.NoList
                        .SelectionBullet = False
                    Case RichTextFormatListStyle.Bullets
                        .SelectionBullet = True
                    Case Else
                        '   cycle through styles until desired one is selected
                        .SelectionBullet = True
                        SendKeys.Send("^+({L " & CType(listStyle, Integer).ToString & "})")
                End Select
                '   restore original focus
                .ResumeLayout()

                If activeControl IsNot Nothing Then
                    activeControl.Select()
                End If
            End With
        End Sub

        ''' <summary>
        ''' Convert plain-text string into RTF string
        ''' </summary>
        ''' <param name="plainText">Plain-text string</param>
        ''' <returns>String with special characters ("{", "\", "}", and non-ASCII) escaped</returns>
        <Extension()>
        Public Function EscapedRTFText(ByVal richTextBox As RichTextBox, ByVal plainText As String) As String
            '   escape any special RTF characters in string
            Dim builder As StringBuilder = New StringBuilder("")
            For CharIndex As Integer = 0 To plainText.Length - 1
                '   parse plain-text string
                Dim character As String = plainText.Substring(CharIndex, 1)
                Select Case character
                    Case " ", "{", "\", "}"
                        '   escape character
                        builder.Append("\" & character)
                    Case Is < " ", Is > "~"
                        '   non-ASCII character
                        builder.Append("\u" & AscW(character).ToString & "?")
                    Case Else
                        '   normal character
                        builder.Append(character)
                End Select
            Next CharIndex
            '   return escaped result
            Return _
                builder.ToString()
        End Function

        ''' <summary>
        ''' Insert RTF text into rich-text box at a given position
        ''' </summary>
        ''' <param name="rtfTextToInsert">RTF-format text to insert</param>
        ''' <param name="position">position in rich-text box to make insertion
        ''' (defaults to current selection position if omitted)</param>
        ''' <remarks>This is the "safe" way to insert, as it accounts for "template RTF"
        ''' that's expected in any inserted RTF text</remarks>
        <Extension()>
        Public Sub InsertRtf(ByVal richTextBox As RichTextBox, ByVal rtfTextToInsert As String, Optional ByVal position As Integer = -1)
            With richTextBox
                '   position defaults to current selection point
                If position < 0 Or position > .TextLength Then
                    position = .SelectionStart
                Else
                    .SelectionLength = 0 'arbitrary insertion
                End If
                '   get RTF template from an empty selection and insert new RTF text
                Dim length As Integer = .SelectionLength
                .Select(position, 0)
                Dim rtfText As String = richTextBox.SelectedRtf
                .Select(position, length)
                Dim endBrace As Integer = rtfText.LastIndexOf("}"c) 'new text comes before RTF closing brace
                rtfText = rtfText.Insert(endBrace, rtfTextToInsert)
                richTextBox.SelectedRtf = rtfText
            End With
        End Sub

#End Region

#Region " DRAWING METHODS "

        ''' <summary>
        ''' Turns on or off redrawing of rich-text box
        ''' This can be used to make multiple changes to the text box while preventing
        ''' the intermediate rendering that would cause flicker
        ''' </summary>
        ''' <param name="isOn">True to activate auto-redraw, False to deactivate it</param>
        ''' <remarks>Specifying True forces the cumulative results
        ''' of previous operations to be rendered</remarks>
        <Extension()>
        Public Sub SetRedrawMode(ByVal control As Windows.Forms.Control, ByVal isOn As Boolean)
            SafeNativeMethods.SetRedrawMode(control.Handle, isOn)
            If isOn Then control.Invalidate() 'force redraw
        End Sub

#End Region

    End Module

End Namespace

''' <summary> Values that represent rich text box list styles. </summary>
Public Enum RichTextFormatListStyle
    NoList = -1
    Bullets
    Numbers
    LowercaseLetters
    UppercaseLetters
    LowercaseRomanNumerals
    UppercaseRomanNumerals
End Enum

