Imports System.Runtime.InteropServices
Imports System.Drawing.Printing
Imports System.Runtime.CompilerServices
Imports System.Text

Namespace RichTextBoxExtensions

    ''' <summary> Rich text box extension print methods. </summary>
    ''' <remarks> (c) 2019 Robert Gustafson. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2019-10-18, </para><para>
    ''' https://www.codeproject.com/Tips/1226034/General-purpose-RULER-Control-for-Use-with-RICH-TE. 
    ''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex </para></remarks>
    Partial Public Module Methods

#Region " PRINTOUT FUNCTIONS "

        '      variables
        Private _RichTextBox As System.Windows.Forms.RichTextBox
        Private _TextLength As Integer
        Private _PrintDocument As PrintDocument
        Private _CurrentPage, _FromPage, _ToPage As Integer
        Private _PageIndexes() As Integer, _PageCount As Integer
        Private _CharFont As Font = Nothing


        ''' <summary> Hundredth inch to Twips. </summary>
        ''' <param name="n"> An Integer to process. </param>
        ''' <returns> An Int32. </returns>
        Private Function HundredthInchToTwips(ByVal n As Integer) As Int32
            '   convert units
            Return Convert.ToInt32((n * 144) \ 10)
        End Function

        ''' <summary> Gets page indexes. 
        '''           Determines indexes for page beginnings, based on printer info</summary>
        Private Sub GetPageIndexes()
            Methods._PageCount = 0
            Dim firstCharOnPage As Integer = 0
            Do
                '   store index for start of current page
                ReDim Preserve Methods._PageIndexes(Methods._PageCount)
                Methods._PageIndexes(_PageCount) = firstCharOnPage
                '   measure current page
                firstCharOnPage = SafeNativeMethods.FormatRange(Methods._RichTextBox.Handle, Methods._PrintDocument.PrinterSettings.CreateMeasurementGraphics,
                                                                Methods._PrintDocument.DefaultPageSettings, firstCharOnPage, Methods._TextLength, False)
                '   prepare for next page
                Methods._PageCount += 1
            Loop While firstCharOnPage < Methods._TextLength
        End Sub

        ''' <summary> Gets page number. 
        ''' Searches for page containing a given caret Position</summary>
        ''' <param name="position"> The position. </param>
        ''' <returns> The page number. </returns>
        Private Function GetPageNumber(ByVal position As Integer) As Integer
            Dim pageNumber As Integer = Array.BinarySearch(_PageIndexes, position)
            If pageNumber < 0 Then
                pageNumber = (pageNumber Xor -1) 'caret is inside of page
            Else
                pageNumber += 1                  'caret is at beginning of page
            End If
            Return pageNumber
        End Function

        ''' <summary> Sets an up.
        '''           Prepare for print job </summary>
        ''' <param name="richTextBox">     . </param>
        ''' <param name="printDocument">   Instance of PrintDocument. </param>
        ''' <param name="pageIndexesOnly"> True to page indexes only. </param>
        Private Sub SetUp(ByVal richTextBox As RichTextBox, ByVal printDocument As PrintDocument, ByVal pageIndexesOnly As Boolean)
            '   prepare for print job
            Methods._RichTextBox = richTextBox
            Methods._TextLength = richTextBox.TextLength
            Methods._PrintDocument = printDocument
            Methods.GetPageIndexes()
            If pageIndexesOnly Then
                Exit Sub 'leave with page indexes
            End If
            '   else prepare to preview/print
            With Methods._PrintDocument
                '   wire up events
                '      (RemoveHandler is used before AddHandler to guard against double-firing
                '         of events in the event this routine is called multiple times)
                RemoveHandler .BeginPrint, AddressOf BeginPrint 'remove any pre-existing handler
                AddHandler .BeginPrint, AddressOf BeginPrint    'add a new handler
                RemoveHandler .PrintPage, AddressOf PrintPage
                AddHandler .PrintPage, AddressOf PrintPage
                RemoveHandler .EndPrint, AddressOf EndPrint
                AddHandler .EndPrint, AddressOf EndPrint
                '   determine which pages to print/preview
                With .PrinterSettings
                    Select Case .PrintRange
                        Case PrintRange.AllPages
                            '   all pages
                            _FromPage = 1
                            _ToPage = _PageCount
                        Case PrintRange.SomePages
                            '   range of pages
                            _FromPage = .FromPage - .MinimumPage + 1
                            _ToPage = .ToPage - .MinimumPage + 1
                        Case PrintRange.Selection
                            '   pages of selected text
                            Methods._FromPage = Methods.GetPageNumber(Methods._RichTextBox.SelectionStart)
                            If Methods._RichTextBox.SelectionLength = 0 Then
                                Methods._ToPage = Methods._FromPage 'no selection
                            Else
                                Methods._ToPage = Methods.GetPageNumber(Methods._RichTextBox.SelectionStart + Methods._RichTextBox.SelectionLength - 1)
                            End If
                        Case Else
                            '   page at caret position
                            Methods._FromPage = Methods.GetPageNumber(Methods._RichTextBox.SelectionStart)
                            Methods._ToPage = Methods._FromPage
                    End Select
                    '   validate page range
                    If Methods._FromPage < 1 Then
                        Methods._FromPage = 1 'page #'s are 1-based
                    ElseIf Methods._FromPage > Methods._PageCount Then
                        Methods._FromPage = Methods._PageCount
                    End If
                    If Methods._ToPage < Methods._FromPage Then
                        Methods._ToPage = Methods._FromPage 'at least 1 page
                    ElseIf Methods._ToPage > Methods._PageCount Then
                        Methods._ToPage = Methods._PageCount
                    End If
                End With
            End With
        End Sub

#End Region

#Region " PRINT DOCUMENT EVENT PROCEDURES "

        ''' <summary> Begins a print. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Print event information. </param>
        Private Sub BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            '   prepare to start printing   
            Methods._CurrentPage = Methods._FromPage
        End Sub

        ''' <summary> Print page. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Print page event information. </param>
        Private Sub PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
            '   print current page
            Dim firstCharOnNextPage As Integer = SafeNativeMethods.FormatRange(Methods._RichTextBox.Handle, e.Graphics, e.PageSettings,
                                                                               Methods._PageIndexes(Methods._CurrentPage - 1),
                                                                               Methods._TextLength, True)
            '   prepare for next page; is it already the last page?
            Methods._CurrentPage += 1
            e.HasMorePages = Methods._CurrentPage <= Methods._ToPage AndAlso firstCharOnNextPage < Methods._TextLength
        End Sub

        ''' <summary> Ends a print. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Print event information. </param>
        Private Sub EndPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            '   finish printing
            SafeNativeMethods.FormatRangeDone(Methods._RichTextBox.Handle)
        End Sub

#End Region

#Region " PRINT FUNCTIONS "

        ''' <summary>
        ''' Width of left-side "selection margin" for highlighting whole lines
        ''' when a RichTextBox's ShowSelection property is True
        ''' </summary>
        Public Const SelectionMargin As Single = 8.0F

        ''' <summary>
        ''' Print RichTextBox contents or a range of pages thereof
        ''' </summary>
        ''' <param name="printDocument">Instance of PrintDocument</param>
        ''' <remarks></remarks>
        <Extension()>
        Public Sub Print(ByVal richTextBox As RichTextBox, ByVal printDocument As PrintDocument)
            '   print document
            Methods.SetUp(richTextBox, printDocument, False)
            printDocument.Print()
        End Sub

        ''' <summary>
        ''' Preview RichTextBox contents or a range of pages thereof to be printed
        ''' </summary>
        ''' <param name="printPreviewDialog">Instance of PrintPreviewDialog</param>
        ''' <returns>Result of Print Preview dialog</returns>
        ''' <remarks></remarks>
        <Extension()>
        Public Function PrintPreview(ByVal richTextBox As RichTextBox, ByVal printPreviewDialog As PrintPreviewDialog) As DialogResult
            Methods.SetUp(richTextBox, printPreviewDialog.Document, False)
            Return printPreviewDialog.ShowDialog()
        End Function

        ''' <summary>
        ''' Get array of indexes for beginnings of pages
        ''' </summary>
        ''' <param name="printDocument">Instance of PrintDocument</param>
        ''' <returns></returns>
        ''' <remarks>Pages are measured according to PrintDocument.DefaultPageSettings;
        ''' no print job is performed. There is always at least one index (array element)
        ''' returned, and the first index is always 0, representing the beginning of all text.</remarks>
        <Extension()>
        Public Function PageIndexes(ByVal richTextBox As RichTextBox, ByVal printDocument As PrintDocument) As Integer()
            Methods.SetUp(richTextBox, printDocument, True)
            Return Methods._PageIndexes
        End Function

        ''' <summary>
        ''' Set RightMargin property of RichTextBox to width of printer page (within horizontal margins)
        ''' so that text wraps at the same position in the text box as on the printer
        ''' </summary>
        ''' <param name="pageSettings">Instance of PageSettings</param>
        ''' <remarks></remarks>
        <Extension()>
        Public Sub SetRightMarginToPrinterWidth(ByVal richTextBox As RichTextBox, ByVal pageSettings As PageSettings)
            Dim pageWidth As Integer
            '   get page width in 1/100's of inches 
            With pageSettings
                pageWidth = .Bounds.Width - .Margins.Left - .Margins.Right
            End With
            '   set rich-text-boxes .RightMargin property
            With richTextBox
                .RightMargin = CType(pageWidth * .CreateGraphics.DpiX / 100.0R, Integer)
            End With
        End Sub

        ''' <summary>
        ''' Set PageSettings right margin to RichTextBox's RightMargin value
        ''' so that text wraps at the same position on the printer as in the text box
        ''' </summary>
        ''' <param name="pageSettings">Instance of PageSettings</param>
        ''' <remarks></remarks>
        <Extension()>
        Public Sub SetPrinterWidthToRightMargin(ByVal richTextBox As RichTextBox, ByVal pageSettings As PageSettings)
            Dim pageWidth As Integer
            '   get text box's.RightMargin property in 1/100's of inches 
            With richTextBox
                pageWidth = CType(.RightMargin * 100.0R / .CreateGraphics.DpiX, Integer)
            End With
            '   set left- and right-hand margin of printer page
            With pageSettings
                Dim difference As Integer = .Bounds.Width - pageWidth
                .Margins.Left = difference \ 2
                .Margins.Right = difference - .Margins.Left
            End With
        End Sub

#End Region

    End Module

End Namespace

