Imports System.Runtime.CompilerServices
Namespace TextBoxExtensions

    ''' <summary> Text box extensions. </summary>
    ''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2019-10-17, 1.0. </para><para>
    ''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox </para></remarks>
    Partial Public Module Methods

        ''' <summary> Insert special characters based on the provided keystrokes. </summary>
        ''' <param name="textBox"> The control. </param>
        ''' <param name="e">  Key event information. </param>
        <Extension>
        Public Sub InsertSpecialCharacters(ByVal textBox As TextBox, ByVal e As KeyEventArgs)
            Dim specialText As String = Methods.ObtainSpecialCharacters(e)
            '   insert special character if one was given
            If Not String.IsNullOrEmpty(specialText) Then
                textBox.SelectedText = specialText
                e.SuppressKeyPress = True
            End If
        End Sub

        ''' <summary> Obtain special characters based on the provided keystrokes. </summary>
        ''' <param name="e">       Key event information. </param>
        ''' <returns> A String. </returns>
        ''' <remakrs> 
        ''' <list type="bullet">Special Character codes<item>
        ''' [Ctrl] + [Alt] + [-]: em dash ("—") </item><item>
        ''' [Ctrl] + [-]: ChrW(173) ¡ optional hyphen (display only when breaking line)</item><item>
        ''' [Ctrl] + [Shift] + [~]: ChrW(8220) left double-quote</item><item>
        ''' [Ctrl] + [Shift] + [~]: ChrW(8216) left single-quote</item><item>
        ''' [Ctrl] + [Shift] + ["]: ChrW(8221) right double-quote</item><item>
        ''' [Ctrl] + [']: : ChrW(8217) right single-quote</item><item>
        ''' [Ctrl] + [Alt] + [R]: registered trademark ("®")</item><item>
        ''' [Ctrl] + [Alt] + [C]: copyright ("©")</item><item>
        ''' [Ctrl] + [Alt] + [T]: trademark ("™")</item></list>
        ''' </remakrs>
        <Extension>
        Public Function ObtainSpecialCharacters(ByVal e As KeyEventArgs) As String
            Dim specialText As String = String.Empty
            ' [Ctrl] not pressed
            If e Is Nothing OrElse Not e.Control Then Return specialText

            Select Case e.KeyCode

                Case Keys.OemMinus, Keys.Subtract
                    '   hyphens/dashes
                    If e.Alt Then
                        '   em dash ("—")
                        '      -- [Ctrl] + [Alt] + [-]
                        specialText = "—"
                    Else
                        '   optional hyphen (display only when breaking line)
                        '      -- [Ctrl] + [-]
                        specialText = ChrW(173)
                    End If

                Case Keys.Oemtilde
                    '   left quotes
                    If e.Shift Then
                        '   left double-quote
                        '      -- [Ctrl] + [Shift] + [~]
                        specialText = ChrW(8220)
                    Else
                        '   left single-quote
                        '      -- [Ctrl] + [`]
                        specialText = ChrW(8216)
                    End If

                Case Keys.OemQuotes
                    '   right quotes
                    If e.Shift Then
                        '    right double-quote
                        '      -- [Ctrl] + [Shift] + ["]
                        specialText = ChrW(8221)
                    Else
                        '   right single-quote
                        '      -- [Ctrl] + [']
                        specialText = ChrW(8217)
                    End If

                Case Keys.C
                    '   copyright ("©")
                    '      -- [Ctrl] + [Alt] + [C]
                    If e.Alt Then
                        specialText = "©"
                    End If

                Case Keys.R
                    '   registered trademark ("®")
                    '      -- [Ctrl] + [Alt] + [R]
                    If e.Alt Then
                        specialText = "®"
                    End If

                Case Keys.T
                    '   trademark ("™")
                    '      -- [Ctrl] + [Alt] + [T]
                    If e.Alt Then
                        specialText = "™"
                    End If
            End Select
            Return specialText
        End Function

    End Module
End Namespace
