Imports System.Runtime.InteropServices
Imports System.Drawing.Printing
Imports System.Runtime.CompilerServices
Imports System.Text

Namespace RichTextBoxExtensions

    Partial Public Module Methods

#Region " UNSAFE NATIVE METHODS  "

        ''' <summary> An unsafe native methods. </summary>
        Private NotInheritable Class UnsafeNativeMethods
            Private Sub New()
                MyBase.New
            End Sub

            '      constants
            '      
            Public Const WM_USER As Int32 = &H400&
            Public Const EM_FORMATRANGE As Int32 = WM_USER + 57
            Public Const EM_GETSCROLLPOS As Integer = WM_USER + 221
            Public Const EM_SETSCROLLPOS As Integer = WM_USER + 222
            Public Const EM_GETCHARFORMAT As Integer = WM_USER + 58
            Public Const EM_SETCHARFORMAT As Integer = WM_USER + 68
            Public Const WM_SETREDRAW As Integer = &HB

            '      structures

#Disable Warning IDE1006 ' Naming Styles
            <StructLayout(LayoutKind.Sequential)>
            Public Structure Rect
                Public left As Int32
                Public top As Int32
                Public right As Int32
                Public bottom As Int32
            End Structure
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
            <StructLayout(LayoutKind.Sequential)>
            Public Structure CharRange
                Public cpMin As Int32
                Public cpMax As Int32
            End Structure
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
            <StructLayout(LayoutKind.Sequential)>
            Public Structure FormatRangeStructure
                Public hdc As IntPtr
                Public hdcTarget As IntPtr
                Public rc As Rect
                Public rcPage As Rect
                Public chrg As CharRange
            End Structure
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
            <StructLayout(LayoutKind.Sequential)>
            Public Structure CHARFORMAT2_STRUCT
                Public cbSize As UInt32
                Public dwMask As UInt32
                Public dwEffects As UInt32
                Public yHeight As Int32
                Public yOffset As Int32
                Public crTextColor As Int32
                Public bCharSet As Byte
                Public bPitchAndFamily As Byte
                <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
                Public szFaceName() As Char
                Public wWeight As UInt16
                Public sSpacing As UInt16
                Public crBackColor As Integer ' Color.ToArgb() -> int
                Public lcid As Integer
                Public dwReserved As Integer
                Public sStyle As Int16
                Public wKerning As Int16
                Public bUnderlineType As Byte
                Public bAnimation As Byte
                Public bRevAuthor As Byte
                Public bReserved1 As Byte
            End Structure
#Enable Warning IDE1006 ' Naming Styles

            '      DLL calls for printing

#If False Then
            <DllImport("user32.dll")>
            Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Int32, ByVal wParam As Int32, ByVal lParam As IntPtr) As Int32
            End Function

            <DllImport("user32.dll")>
            Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Int32, ByVal wParam As Int32, ByRef lParam As Point) As Int32
            End Function
#End If

            ''' <summary> Sends a message. </summary>
            ''' <param name="hWnd">   The window. </param>
            ''' <param name="msg">    The message. </param>
            ''' <param name="wParam"> The parameter. </param>
            ''' <param name="lParam"> The parameter. </param>
            ''' <returns> An Int32. </returns>
            <DllImport("user32.dll", CharSet:=CharSet.Auto)>
            Friend Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As UInt32, ByVal wParam As UIntPtr, ByVal lParam As IntPtr) As IntPtr
            End Function

            '         for scrolling and auto-redraw setting

            <DllImport("user32.dll", CharSet:=CharSet.Auto)>
            Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As UInt32, ByVal wParam As UIntPtr, ByRef lParam As Point) As IntPtr
            End Function

            <DllImport("user32.dll", EntryPoint:="GetScrollInfo")>
            Public Shared Function GetScrollInfo(ByVal hwnd As IntPtr, ByVal nBar As Integer, ByRef lpsi As ScrollInfo) As <MarshalAs(UnmanagedType.Bool)> Boolean
            End Function

            '      enums for GetScrollInfo call (used by GetScrollBarInfo)
            <Flags()>
            Public Enum ScrollBarMask
                Range = 1
                PageSize = 2
                Position = 4
                TrackPosition = 16
                Everything = Range Or PageSize Or Position Or TrackPosition
            End Enum

            <Flags>
            Public Enum ScrollBarType
                Horizontal = 0
                Vertical = 1
                Control = 2 ' ScrollInfo's nBar parameter when hwnd is a handle to a ScrollBar 
            End Enum

#Disable Warning IDE1006 ' Naming Styles
            ''' <summary> structure for GetScrollInfo call (used by GetScrollBarInfo). </summary>
            ''' <remarks> David, 2020-09-24. </remarks>
            <StructLayout(LayoutKind.Sequential)>
            Public Structure ScrollInfo
                '   INPUT: What information to get
                Public SizeOfStructure As Integer 'infrastructure
                Public ScrollBarMask As Integer 'tells which fields below to retrieve information for
                '   OUTPUT: Scrollbar information
                Public MinimumScrollPosition As Integer
                Public MaximumScrollPosition As Integer
                Public PageSize As Integer
                Public Position As Integer
                Public TrackPosition As Integer
            End Structure
#Enable Warning IDE1006 ' Naming Styles

        End Class

#End Region

#Region " SAFE NATIVE METHODS "

        ''' <summary> An unsafe native methods. </summary>
        Private NotInheritable Class SafeNativeMethods
            Private Sub New()
                MyBase.New
            End Sub

            ''' <summary> Format range. </summary>
            ''' <param name="handle">       The handle. </param>
            ''' <param name="graphics">     The graphics. </param>
            ''' <param name="pageSettings"> The page settings. </param>
            ''' <param name="charFrom">     The character from. </param>
            ''' <param name="charTo">       The character to. </param>
            ''' <param name="renderText">   True to render text. </param>
            ''' <returns> The formatted range. </returns>
            Public Shared Function FormatRange(ByVal handle As IntPtr, ByVal graphics As Graphics, ByVal pageSettings As PageSettings,
                                         ByVal charFrom As Integer, ByVal charTo As Integer, ByVal renderText As Boolean) As Integer
                With pageSettings
                    '   define character range   
                    Dim cr As UnsafeNativeMethods.CharRange
                    cr.cpMin = charFrom
                    cr.cpMax = charTo
                    '   define margins
                    Dim rc As UnsafeNativeMethods.Rect
                    rc.top = Methods.HundredthInchToTwips(.Bounds.Top + .Margins.Top)
                    rc.bottom = Methods.HundredthInchToTwips(.Bounds.Bottom - .Margins.Bottom)
                    rc.left = Methods.HundredthInchToTwips(.Bounds.Left + .Margins.Left)
                    rc.right = Methods.HundredthInchToTwips(.Bounds.Right - .Margins.Right)
                    '   define page size
                    Dim rcPage As UnsafeNativeMethods.Rect
                    rcPage.top = Methods.HundredthInchToTwips(.Bounds.Top)
                    rcPage.bottom = Methods.HundredthInchToTwips(.Bounds.Bottom)
                    rcPage.left = Methods.HundredthInchToTwips(.Bounds.Left)
                    rcPage.right = Methods.HundredthInchToTwips(.Bounds.Right)
                    '   handle device context
                    Dim hdc As IntPtr = graphics.GetHdc
                    '   handle full format info
                    Dim fr As UnsafeNativeMethods.FormatRangeStructure
                    fr.chrg = cr
                    fr.hdc = hdc
                    fr.hdcTarget = hdc
                    fr.rc = rc
                    fr.rcPage = rcPage
                    '   prepare to render/measure page   
                    Dim wParam As Int32
                    If renderText Then
                        wParam = 1 'render text
                    Else
                        wParam = 0 'measure only
                    End If
                    Dim lParam As IntPtr
                    lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(fr))
                    Marshal.StructureToPtr(fr, lParam, False)
                    '   render/measure page and return first char of next page
                    Dim res As Integer = UnsafeNativeMethods.SendMessage(handle, UnsafeNativeMethods.EM_FORMATRANGE, wParam, lParam)
                    Marshal.FreeCoTaskMem(lParam)
                    graphics.ReleaseHdc(hdc)
                    Return res
                End With
            End Function

            ''' <summary> Format range done. </summary>
            ''' <param name="handle"> The handle. </param>
            Public Shared Sub FormatRangeDone(ByVal handle As IntPtr)
                '   flag printing done
                Dim lParam As New IntPtr(0)
                UnsafeNativeMethods.SendMessage(handle, UnsafeNativeMethods.EM_FORMATRANGE, 0, lParam)
            End Sub

#Region " SCROLLING/TEXT-WIDTH METHODS "

            ''' <summary>
            ''' Get scroll position
            ''' </summary>
            ''' <param name="handle">      A handler to control containing the scroll bars. </param>
            ''' <returns>Point structure containing current horizontal (.x)
            ''' and vertical (.y) scroll positions in pixels</returns>
            ''' <remarks></remarks>
            Public Shared Function GetScrollPosition(ByVal handle As IntPtr) As Point
                Dim rtbScrollPoint As Point = Nothing
                UnsafeNativeMethods.SendMessage(handle, UnsafeNativeMethods.EM_GETSCROLLPOS, 0, rtbScrollPoint)
                Return rtbScrollPoint
            End Function

            ''' <summary>
            ''' Set scroll position of RichTextBox
            ''' </summary>
            ''' <param name="handle">      A handler to control containing the scroll bars. </param>
            ''' <param name="scrollPoint"> Point structure containing new horizontal (.x)
            ''' and vertical (.y) scroll positions in pixels</param>
            Public Shared Sub SetScrollPosition(ByVal handle As IntPtr, ByVal scrollPoint As Point)
                UnsafeNativeMethods.SendMessage(handle, UnsafeNativeMethods.EM_SETSCROLLPOS, 0, scrollPoint)
            End Sub

            ''' <summary>
            ''' Get information about a RichTextBox scroll bar
            ''' </summary>
            ''' <param name="handle">      A handler to control containing the scroll bars. </param>
            ''' <param name="scrollBarType">ScrollBarType value (.Horizontal or .Vertical)</param>
            ''' <param name="scrollBarMask">ScrollBarMask flags indicating what to get
            ''' (range, page size, position, track position; defaults to everything)</param>
            ''' <returns>ScrollInfo structure with requested info</returns>
            ''' <remarks></remarks>
            Public Shared Function GetScrollBarInfo(ByVal handle As IntPtr, ByVal scrollBarType As UnsafeNativeMethods.ScrollBarType,
                    Optional scrollBarMask As UnsafeNativeMethods.ScrollBarMask = UnsafeNativeMethods.ScrollBarMask.Everything) As UnsafeNativeMethods.ScrollInfo
                Dim si As New UnsafeNativeMethods.ScrollInfo
                si.SizeOfStructure = Marshal.SizeOf(si) 'must alway be set to the size of a ScrollInfo structure
                si.ScrollBarMask = scrollBarMask 'tells the GetScrollInfo what to get
                UnsafeNativeMethods.GetScrollInfo(handle, scrollBarType, si) 'horizontal or vertical?
                Return si
            End Function

#End Region

#Region " DRAWING METHODS "

            ''' <summary>
            ''' Turns on or off redrawing of a control with the specified handle.
            ''' This can be used to make multiple changes to the control while preventing
            ''' the intermediate rendering that would cause flicker
            ''' </summary>
            ''' <param name="handle">      A handler to the control. </param>
            ''' <param name="isOn">True to activate auto-redraw, False to deactivate it</param>
            ''' <remarks>Specifying True forces the cumulative results
            ''' of previous operations to be rendered</remarks>
            Public Shared Sub SetRedrawMode(ByVal handle As IntPtr, ByVal isOn As Boolean)
                UnsafeNativeMethods.SendMessage(handle, UnsafeNativeMethods.WM_SETREDRAW, CInt(isOn) And 1, 1)
            End Sub

#End Region


        End Class

#End Region

    End Module

End Namespace

