﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class InsertSymbolForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InsertSymbolForm))
        Me._SymbolsGrid = New System.Windows.Forms.DataGridView()
        Me._InsertButton = New System.Windows.Forms.Button()
        Me._FontButton = New System.Windows.Forms.Button()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me.lblSymbol = New System.Windows.Forms.Label()
        Me.lblSymbolFont = New System.Windows.Forms.Label()
        Me._InsertFontDialog = New System.Windows.Forms.FontDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me._SymbolRangeComboBox = New System.Windows.Forms.ComboBox()
        CType(Me._SymbolsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_SymbolsGrid
        '
        Me._SymbolsGrid.AllowUserToAddRows = False
        Me._SymbolsGrid.AllowUserToDeleteRows = False
        Me._SymbolsGrid.AllowUserToResizeColumns = False
        Me._SymbolsGrid.AllowUserToResizeRows = False
        Me._SymbolsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me._SymbolsGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me._SymbolsGrid.BackgroundColor = System.Drawing.Color.White
        Me._SymbolsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._SymbolsGrid.ColumnHeadersVisible = False
        Me._SymbolsGrid.Dock = System.Windows.Forms.DockStyle.Top
        Me._SymbolsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me._SymbolsGrid.Location = New System.Drawing.Point(0, 0)
        Me._SymbolsGrid.MultiSelect = False
        Me._SymbolsGrid.Name = "_SymbolsGrid"
        Me._SymbolsGrid.RowHeadersVisible = False
        Me._SymbolsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me._SymbolsGrid.Size = New System.Drawing.Size(841, 173)
        Me._SymbolsGrid.TabIndex = 0
        '
        '_InsertButton
        '
        Me._InsertButton.Location = New System.Drawing.Point(234, 283)
        Me._InsertButton.Name = "_InsertButton"
        Me._InsertButton.Size = New System.Drawing.Size(87, 27)
        Me._InsertButton.TabIndex = 6
        Me._InsertButton.Text = "Insert"
        Me._InsertButton.UseVisualStyleBackColor = True
        '
        '_FontButton
        '
        Me._FontButton.Location = New System.Drawing.Point(524, 189)
        Me._FontButton.Name = "_FontButton"
        Me._FontButton.Size = New System.Drawing.Size(87, 27)
        Me._FontButton.TabIndex = 3
        Me._FontButton.Text = "Font..."
        Me._FontButton.UseVisualStyleBackColor = True
        '
        '_CancelButton
        '
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.Location = New System.Drawing.Point(519, 283)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(87, 27)
        Me._CancelButton.TabIndex = 7
        Me._CancelButton.Text = "Cencel"
        Me._CancelButton.UseVisualStyleBackColor = True
        '
        'lblSymbol
        '
        Me.lblSymbol.AutoSize = True
        Me.lblSymbol.Location = New System.Drawing.Point(14, 232)
        Me.lblSymbol.Name = "lblSymbol"
        Me.lblSymbol.Size = New System.Drawing.Size(50, 15)
        Me.lblSymbol.TabIndex = 4
        Me.lblSymbol.Text = "Symbol:"
        '
        'lblSymbolFont
        '
        Me.lblSymbolFont.AutoSize = True
        Me.lblSymbolFont.Location = New System.Drawing.Point(14, 249)
        Me.lblSymbolFont.Name = "lblSymbolFont"
        Me.lblSymbolFont.Size = New System.Drawing.Size(34, 15)
        Me.lblSymbolFont.TabIndex = 5
        Me.lblSymbolFont.Text = "Font:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(245, 195)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Character Code Range:"
        '
        '_SymbolRangeComboBox
        '
        Me._SymbolRangeComboBox.FormattingEnabled = True
        Me._SymbolRangeComboBox.Location = New System.Drawing.Point(376, 190)
        Me._SymbolRangeComboBox.Name = "_SymbolRangeComboBox"
        Me._SymbolRangeComboBox.Size = New System.Drawing.Size(123, 23)
        Me._SymbolRangeComboBox.TabIndex = 2
        '
        'InsertSymbolForm
        '
        Me.AcceptButton = Me._InsertButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me._CancelButton
        Me.ClientSize = New System.Drawing.Size(841, 321)
        Me.Controls.Add(Me._SymbolRangeComboBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblSymbolFont)
        Me.Controls.Add(Me.lblSymbol)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._FontButton)
        Me.Controls.Add(Me._InsertButton)
        Me.Controls.Add(Me._SymbolsGrid)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "InsertSymbolForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Insert Symbol"
        CType(Me._SymbolsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _FontButton As Button
    Friend WithEvents lblSymbol As Label
    Friend WithEvents lblSymbolFont As Label
    Friend WithEvents _InsertFontDialog As FontDialog
    Friend WithEvents Label1 As Label
    Friend WithEvents _SymbolRangeComboBox As ComboBox
    Private WithEvents _CancelButton As Button
    Private WithEvents _SymbolsGrid As DataGridView
    Private WithEvents _InsertButton As Button
End Class
