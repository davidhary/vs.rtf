﻿Imports System.ComponentModel
Imports System.Drawing.Drawing2D

''' <summary> A text ruler control </summary>
''' <remarks> (c) 2019 Robert Gustafson. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-18, ">
''' https://www.codeproject.com/Tips/1226034/General-purpose-RULER-Control-for-Use-with-RICH-TE. 
''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex </para></remarks>
Partial Public Class TextRuler
    Inherits UserControl

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.InitializeComponent()
        '   set up marker rectangles and styles
        With Me.markers
            For i As RulerMarkerType = RulerMarkerType.LeftIndentBottom To RulerMarkerType.LeftIndents
                .Add(New RectangleF())
            Next i
        End With
        Me.SetStyle(ControlStyles.UserPaint, True)
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        Me.SetStyle(ControlStyles.ResizeRedraw, True)
        Me.SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        Me._Tabs.Clear()
        '   get unit-to-pixel conversion values
        Using g As Graphics = Graphics.FromHwnd(Me.Handle)
            Me.PixelsPerInch = g.DpiX
            Me.PixelsPerCentimeter = g.DpiX / 2.54F
        End Using
        With Me.RulerToolTip
            .ShowAlways = True
            .SetToolTip(Me, Me._ToolTipString)
        End With
    End Sub

    ''' <summary> Initializes the component. </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TextRuler))
        Me._ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.SuspendLayout()
        '
        '_ImageList
        '
        Me._ImageList.ImageStream = CType(resources.GetObject("_ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me._ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me._ImageList.Images.SetKeyName(0, "LeftIndentBottom")
        Me._ImageList.Images.SetKeyName(1, "FirstLineIndent")
        Me._ImageList.Images.SetKeyName(2, "RightIndent")
        Me._ImageList.Images.SetKeyName(3, "LeftTab")
        '
        'TextRuler
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "TextRuler"
        Me.Size = New System.Drawing.Size(100, 20)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "  PRIVATE COMPONENTS "

    '      private constants
    Private MinimumDistance As Single = 11.0F 'tabs and left-vs.-right indents must be this far apart

    '      private variables

    Private Myself As RectangleF = New RectangleF()
    Private drawZone As RectangleF = New RectangleF()
    Private workArea As RectangleF = New RectangleF()
    Private markers As List(Of RectangleF) = New List(Of RectangleF)()
    Private _Tabs As List(Of RectangleF) = New List(Of RectangleF)()
    Private _Pen As Pen = New Pen(Color.Transparent)
    Private pos As Single = -1.0F
    Private mCaptured As Boolean = False
    Private RulerToolTip As New ToolTip()
    Private WithEvents _ImageList As ImageList
    Private components As IContainer

#End Region

#Region "  PRIVATE PROCEDURES "

    ''' <summary> Zoom value. </summary>
    ''' <param name="value"> Value in units specified by Units property. </param>
    ''' <returns> A Single. </returns>
    Private Function ZoomValue(value As Single) As Single
        '   zoom position
        Return value * Me._ZoomFactor
    End Function

    ''' <summary> Un zoom value. </summary>
    ''' <param name="value"> Value in units specified by Units property. </param>
    ''' <returns> A Single. </returns>
    Private Function UnZoomValue(value As Single) As Single
        '   un-zoom position
        Return value / Me._ZoomFactor
    End Function

    ''' <summary> Right most left indent. </summary>
    ''' <returns> A Single. </returns>
    Private Function RightMostLeftIndent() As Single
        '   get upper or lower left indent, whichever is larger
        Return Math.Max(Me.luIndent, Me.llIndent)
    End Function

    ''' <summary> Left most left indent. </summary>
    ''' <returns> A Single. </returns>
    Private Function LeftMostLeftIndent() As Single
        '   get upper or lower left indent, whichever is smaller
        Return Math.Max(Me.luIndent, Me.llIndent)
    End Function

    ''' <summary> Left most tab. </summary>
    ''' <returns> A Single. </returns>
    Private Function LeftMostTab() As Single
        '   get leftmost tab position
        If Me._Tabs.Count = 0 Then
            Return Me.rMargin
        Else
            Return Me._Tabs(0).X + Me._ScrollOffset
        End If
    End Function

    ''' <summary> Right most tab. </summary>
    ''' <returns> A Single. </returns>
    Private Function RightMostTab() As Single
        '   get rightmost tab position
        If Me._Tabs.Count = 0 Then
            Return Me.lMargin
        Else
            Return Me._Tabs(Me._Tabs.Count - 1).X + Me._ScrollOffset
        End If
    End Function

    ''' <summary> Query if 'tabNumber' is tab in order. </summary>
    ''' <param name="tabNumber">   The tab number. </param>
    ''' <param name="tabPosition"> The tab position. </param>
    ''' <returns> <c>true</c> if tab in order; otherwise <c>false</c> </returns>
    Private Function IsTabInOrder(ByVal tabNumber As Integer, ByVal tabPosition As Single) As Boolean
        '   make sure tab isn't moved past another tab
        If Me._Tabs.Count < 2 Then
            Return True 'single tab is always valid
        End If
        '   compare this tab to previous and next tabs
        Select Case tabNumber
            Case 0
                '   first tab must be left of second
                Return tabPosition <= Me._Tabs(1).X - Me.MinimumDistance
            Case Me._Tabs.Count - 1
                '   last tab must be right of second-to-last
                Return tabPosition >= Me._Tabs(Me._Tabs.Count - 2).X + Me.MinimumDistance
            Case Else
                '   current tab must be between previous and next
                Return tabPosition >= Me._Tabs(tabNumber - 1).X + Me.MinimumDistance _
              AndAlso tabPosition <= Me._Tabs(tabNumber + 1).X - Me.MinimumDistance
        End Select
    End Function

    ''' <summary> Query if 'tabPosition' is tab new. </summary>
    ''' <param name="tabPosition"> The tab position. </param>
    ''' <returns> <c>true</c> if tab new; otherwise <c>false</c> </returns>
    Private Function IsTabNew(ByVal tabPosition As Single) As Boolean
        '   make sure this proposed tab isn't already present
        For i As Integer = 0 To Me._Tabs.Count - 1
            If Math.Abs(Me._Tabs(i).X - tabPosition) <= Me.MinimumDistance Then
                Return False 'tab already here
            End If
        Next i
        '   report tab is new
        Return True
    End Function

    ''' <summary> Adjust indents. Adjusts indents for new margins </summary>
    ''' <param name="newLeftMargin">  The new left margin. </param>
    ''' <param name="newRightMargin"> The new right margin. </param>
    Private Sub AdjustIndents(ByVal newLeftMargin As Single, newRightMargin As Single)
        Dim leftChange As Single = newLeftMargin - Me.lMargin
        Dim rightChange As Single = newRightMargin - Me.rMargin
        Me.luIndent += leftChange
        Me.llIndent += leftChange
        Me.rIndent += rightChange
    End Sub

    ''' <summary> Query if 'value' is valid position. </summary>
    ''' <param name="value">      Value in units specified by Units property. </param>
    ''' <param name="markerType"> Type of the marker. </param>
    ''' <returns> <c>true</c> if valid position; otherwise <c>false</c> </returns>
    Private Function IsValidPosition(ByVal value As Single, markerType As RulerMarkerType) As Boolean
        '   make sure marker value is in range
        Select Case markerType
            Case RulerMarkerType.LeftMargin
                '   left margin
                Return value >= 1.0F AndAlso Me.RightMostLeftIndent() + (value - Me.lMargin) + Me.MinimumDistance <= Me._MaxWidth - Me.rIndent AndAlso value <= Me.LeftMostTab()
            Case RulerMarkerType.RightMargin
                '   right margin
                Return Me.RightMostLeftIndent() + Me.MinimumDistance <= value - Me.rIndent AndAlso value <= Me._MaxWidth - 1.0F AndAlso value >= Me.RightMostTab()
            Case RulerMarkerType.FirstLineIndent, RulerMarkerType.HangingIndent
                '   upper or lower left indent
                Return value >= Me.lMargin AndAlso value <= Me._MaxWidth - Me.rIndent - Me.MinimumDistance
            Case RulerMarkerType.LeftIndents
                '   both indents
                Dim hi As Single = Me.llIndent - Me.luIndent
                If hi < 0 Then
                    '    lower indent is leftmost
                    Return value >= Me.lMargin AndAlso value - hi <= Me._MaxWidth - (Me.rIndent + Me.MinimumDistance)
                Else
                    '   upper indent is leftmost
                    Return value - hi >= Me.lMargin AndAlso value <= Me._MaxWidth - (Me.rIndent + Me.MinimumDistance)
                End If
            Case RulerMarkerType.RightIndent
                '   right indent
                Return value >= Me.RightMostLeftIndent() + Me.MinimumDistance AndAlso value <= Me._MaxWidth - Me.rMargin
            Case RulerMarkerType.Tab
                '   tab
                Return value >= Me.lMargin AndAlso value <= Me._MaxWidth - Me.rMargin
        End Select
        Return True
    End Function

    ''' <summary> Creates tab arguments. </summary>
    ''' <param name="newPos"> The new position. </param>
    ''' <returns> The new tab arguments. </returns>
    Private Function CreateTabArgs(ByVal newPos As Single) As TabEventArgs
        '   set up tab info for event
        Dim tae As TabEventArgs =
        New TabEventArgs(
            CInt(Me.UnZoomValue(newPos - 1.0F) + Me._ScrollOffset),
            CInt(Me.UnZoomValue(Me.pos - 1.0F) + Me._ScrollOffset))
        Return tae
    End Function

    ''' <summary> Check for marker or tab. </summary>
    ''' <param name="e"> Mouse event information. </param>
    Private Sub CheckForMarkerOrTab(ByVal e As MouseEventArgs)
        '   check to see if mouse is hovering over an indent/margin marker or a tab
        Me._CurrentMarker = RulerMarkerType.NoMarker
        Me._CurrentTab = RulerMarkerType.NoMarker
        '   check for indent or margin marker 
        For i As RulerMarkerType = RulerMarkerType.LeftMargin To RulerMarkerType.LeftIndents
            '   check this marker
            If Me.markers(i).Contains(e.Location) Then
                If Me.NoMargins AndAlso
                    (i = RulerMarkerType.LeftMargin OrElse i = RulerMarkerType.RightMargin) Then
                    Exit For 'margins not allowed
                End If
                '   mouse is over one--see if left mouse button is down
                Me._CurrentMarker = i
                Exit Sub
            End If
        Next i
        '   see if mouse is over a tab
        If Me._TabsEnabled AndAlso Me._Tabs.Count > 0 Then
            For i As Integer = 0 To Me._Tabs.Count - 1
                '   check this tab
                If Me._Tabs(i).Contains(e.Location) Then
                    '   mouse is over one--see if left mouse button is down
                    Me._CurrentMarker = RulerMarkerType.Tab
                    Me._CurrentTab = i
                    Exit Sub
                End If
            Next i
        End If
    End Sub

    ''' <summary> Sets tool tip. Configures tooltip text to mouse situation. </summary>
    ''' <param name="markerType"> Type of the marker. </param>
    Private Sub SetToolTip(ByVal markerType As RulerMarkerType)
        With Me.RulerToolTip
            Dim ts As String = Me._ToolTipString
            If Me.UsingSmartToolTips AndAlso markerType <> RulerMarkerType.NoMarker AndAlso markerType <> RulerMarkerType.LeftIndentBottom Then
                '   display tooltip for marker name
                Try
                    ts = My.Resources.Resources.ResourceManager.GetString(markerType.ToString)
                Catch ex As Exception
                    '   invalid tooltip value (shouldn't happen)--ignore
                End Try
            End If
            '   change tooltip text if needed
            If ts <> .GetToolTip(Me) Then
                .SetToolTip(Me, ts)
            End If
        End With
    End Sub

    ''' <summary> Draw back ground. </summary>
    ''' <param name="g"> The Graphics to process. </param>
    Private Sub DrawBackGround(ByVal g As Graphics)
        Me._Pen.Color = Color.Transparent
        g.FillRectangle(Me._Pen.Brush, Me.Myself)
        Me._Pen.Color = Me.BaseColor
        g.FillRectangle(Me._Pen.Brush, Me.drawZone)
    End Sub

    ''' <summary> Draw margins. </summary>
    ''' <param name="g"> The Graphics to process. </param>
    Private Sub DrawMargins(ByVal g As Graphics)
        If Not Me._NoMargins Then
            Me.markers(RulerMarkerType.LeftMargin) = New RectangleF(-Me._ScrollOffset, 3.0F, Me.lMargin, 14.0F)
            Me.markers(RulerMarkerType.RightMargin) = New RectangleF(Me._MaxWidth - Me._ScrollOffset - Me.rMargin + 1.0F, 3.0F, CSng(Me.rMargin), 14.0F)
            Me._Pen.Color = Color.DarkGray
            g.FillRectangle(Me._Pen.Brush, Me.markers(RulerMarkerType.LeftMargin))
            g.FillRectangle(Me._Pen.Brush, Me.markers(RulerMarkerType.RightMargin))
        End If
        '   draw outer rectangle
        g.PixelOffsetMode = PixelOffsetMode.None
        Me._Pen.Color = Me._strokeColor
        g.DrawRectangle(Me._Pen, -Me._ScrollOffset, 3.0F, Me._MaxWidth - 1, 14.0F)
        '   if there's any space after outer rectangle, mark it off
        Dim afterMargin As Single = Me._MaxWidth - Me._ScrollOffset
        If afterMargin < Me.Myself.Width Then
            '   this space is inaccessible to user, so mark it off
            Me._Pen.Color = Me.ForeColor
            g.FillRectangle(Me._Pen.Brush, afterMargin, 3, Me.Myself.Width - afterMargin, 14)
        End If
    End Sub

    ''' <summary> Draw text and marks. </summary>
    ''' <param name="g"> The Graphics to process. </param>
    Private Sub DrawTextAndMarks(ByVal g As Graphics)
        Me._Pen.Color = Me.ForeColor
        Dim sz As SizeF
        Dim points, position, maximum, subdiv As Integer
        Dim range As Single = 0.0F, yLine() As Single = Nothing
        '   get subdivisions
        Select Case Me._Units
            Case RulerUnitType.Centimeters
                '   mark off centimeters (divided in 1/2ths)
                points = CInt(Me.UnZoomValue(Me._MaxWidth) / Me.PixelsPerCentimeter)
                subdiv = 2
                range = Me.PixelsPerCentimeter / CSng(subdiv)
                maximum = points * subdiv
                ReDim yLine(maximum)
                '   determine subdivision marker lengths
                For i As Integer = 0 To maximum
                    position = i + 1
                    If (position Mod subdiv) = 1 Then
                        yLine(i) = 8.0F  ' 1/2 cm (5 mm)
                    Else
                        yLine(i) = -1.0F ' whole cm (no marker)
                    End If
                Next i
            Case RulerUnitType.Inches
                '   mark off inches (divided in 1/8ths)
                points = CInt(Me.UnZoomValue(Me._MaxWidth) / Me.PixelsPerInch)
                subdiv = 8
                range = Me.PixelsPerInch / CSng(subdiv)
                maximum = points * subdiv
                ReDim yLine(maximum)
                '   determine subdivision marker lengths
                For i As Integer = 0 To maximum
                    position = i + 1
                    Select Case position Mod subdiv
                        Case 1, 3, 5, 7
                            yLine(i) = 4.0F  ' 1/8, 3/8, 5/8, 7/8 inch
                        Case 2, 6
                            yLine(i) = 6.0F  ' 1/4, 3/4 inch
                        Case 4
                            yLine(i) = 8.0F  ' 1/2 inch
                        Case Else
                            yLine(i) = -1.0F ' whole inch (no marker)
                    End Select
                Next i
        End Select
        '   draw #'s and markers
        Dim numeral As String = String.Empty,
        XPos As Single = 0F
        For i As Integer = 0 To maximum
            position = i + 1
            XPos = Me.ZoomValue(position * range) - Me._ScrollOffset
            If (position Mod subdiv) > 0 AndAlso yLine(i) > 0.0F Then
                '   draw subdivision marker
                g.DrawLine(Me._Pen, XPos, 9.0F - yLine(i) / 2.0F, XPos, 9.0F + yLine(i) / 2.0F)
            Else
                '   draw #
                numeral = Convert.ToInt32(position / subdiv).ToString
                sz = g.MeasureString(numeral, Me.Font)
                g.DrawString(numeral, Me.Font, Me._Pen.Brush,
                New PointF(XPos - sz.Width / 2,
                    (Me.Myself.Height - sz.Height) / 2))
            End If
        Next i
        '   set pixel offset
        g.PixelOffsetMode = PixelOffsetMode.Half
    End Sub

    ''' <summary> Draw indent markers. </summary>
    ''' <param name="g"> The Graphics to process. </param>
    Private Sub DrawIndents(ByVal g As Graphics)
        Me.markers(RulerMarkerType.FirstLineIndent) = New RectangleF(Me.luIndent - Me._ScrollOffset - 4.5F, 0F, 9.0F, 8.0F)
        Me.markers(RulerMarkerType.LeftIndentBottom) = New RectangleF(Me.llIndent - Me._ScrollOffset - 4.5F, 8.2F, 9.0F, 11.8F)
        Me.markers(RulerMarkerType.RightIndent) = New RectangleF(Me._MaxWidth - Me._ScrollOffset - Me.rIndent - 4.5F, 11.0F, 9.0F, 8.0F)
        Me.markers(RulerMarkerType.HangingIndent) = New RectangleF(Me.llIndent - Me._ScrollOffset - 4.5F, 8.2F, 9.0F, 5.9F)
        Me.markers(RulerMarkerType.LeftIndents) = New RectangleF(Me.llIndent - Me._ScrollOffset - 4.5F, 14.1F, 9.0F, 5.9F)
#If True Then
        g.DrawImage(Me._ImageList.Images.Item(RulerMarkerType.FirstLineIndent.ToString), Me.markers(RulerMarkerType.FirstLineIndent))
        g.DrawImage(Me._ImageList.Images.Item(RulerMarkerType.LeftIndentBottom.ToString), Me.markers(RulerMarkerType.LeftIndentBottom))
        g.DrawImage(Me._ImageList.Images.Item(RulerMarkerType.RightIndent.ToString), Me.markers(RulerMarkerType.RightIndent))
#Else
        g.DrawImage(My.Resources.l_indet_pos_upper, markers(RulerMarkerType.FirstLineIndent))
        g.DrawImage(My.Resources.l_indent_pos_lower, markers(RulerMarkerType.LeftIndentBottom))
        g.DrawImage(My.Resources.r_indent_pos, markers(RulerMarkerType.RightIndent))
#End If

#If False Then
	g.DrawImage(My.Resources.l_indet_pos_upper, markers(MarkerType.First_Line_Indent))
	g.DrawImage(My.Resources.l_indent_pos_lower, markers(MarkerType.Left_Indent_Bottom))
	g.DrawImage(My.Resources.r_indent_pos, markers(MarkerType.Right_Indent))
#End If
    End Sub

    ''' <summary> Draw tab markers. </summary>
    ''' <param name="g"> The Graphics to process. </param>
    Private Sub DrawTabs(ByVal g As Graphics)
        If Not Me._TabsEnabled OrElse Me._Tabs.Count = 0 Then
            Return
        End If
        For i As Integer = 0 To Me._Tabs.Count - 1
            g.DrawImage(Me._ImageList.Images.Item($"Left{RulerMarkerType.Tab}"), Me._Tabs(i))
        Next i
    End Sub

    ''' <summary> Creates a new tab and raise event. </summary>
    ''' <param name="pos"> The position. </param>
    Private Sub AddTab(ByVal pos As Single)
        Dim rect As RectangleF = New RectangleF(pos, 10.0F, 8.0F, 8.0F)
        '   determine where to insert
        Dim Index As Integer = -1
        Do
            Index += 1
        Loop While Index < Me._Tabs.Count AndAlso pos > Me._Tabs(Index).X
        Me._Tabs.Insert(Index, rect)
        Me._CurrentMarker = RulerMarkerType.Tab
        Me._CurrentTab = Index
        '   fire event
        RaiseEvent TabAdded(Me, Me.CreateTabArgs(pos))
    End Sub

#End Region

#Region "  PUBLIC COMPONENTS: EVENTS "

    ''' <summary>
    ''' Event for tracking a change in indent(s) by user
    ''' </summary>
    ''' <param name="sender">TextRuler instance</param>
    ''' <param name="e">INPUT: e.MarkerType = type of indent(s) changed</param>
    Public Event IndentsChanged(ByVal sender As Object, ByVal e As MarginOrIndentEventArgs)

    ''' <summary>
    ''' Event for tracking a change in margin by user
    ''' </summary>
    ''' <param name="sender">TextRuler instance</param>
    ''' <param name="e">INPUT: e.MarkerType = type of margin changed</param>
    Public Event MarginsChanged(ByVal sender As Object, ByVal e As MarginOrIndentEventArgs)

    ''' <summary>
    ''' Event for tracking a new tab being added by user
    ''' </summary>
    ''' <param name="sender">TextRuler instance</param>
    ''' <param name="e">INPUT: e.NewPosition = position of new tab</param>
    Public Event TabAdded(ByVal sender As Object, ByVal e As TabEventArgs)

    ''' <summary>
    ''' Event for tracking an existing tab being removed by user
    ''' </summary>
    ''' <param name="sender">TextRuler instance</param>
    ''' <param name="e">INPUT: e.OldPosition = position of deleted tab</param>
    Public Event TabRemoved(ByVal sender As Object, ByVal e As TabEventArgs)

    ''' <summary>
    ''' Event for tracking an existing tab being moved by user
    ''' </summary>
    ''' <param name="sender">TextRuler instance</param>
    ''' <param name="e">INPUT:
    ''' e.NewPosition = mew position of tab, and e.OldPosition = previous position of tab</param>
    Public Event TabChanged(ByVal sender As Object, ByVal e As TabEventArgs)

#End Region

#Region "  PUBLIC COMPONENTS: properties "

    Private _CurrentMarker As RulerMarkerType = RulerMarkerType.NoMarker
    ''' <summary>
    ''' Which margin or indent(s) is/are the mouse hovering over or dragging?
    ''' </summary>
    ''' <returns>Type of marker (MarkerType.No_Marker if none)</returns>
    <Browsable(False)>
    Public ReadOnly Property MarkerUnderMouse() As RulerMarkerType
        Get
            Return Me._CurrentMarker
        End Get
    End Property

    Private _CurrentTab As Integer = RulerMarkerType.NoMarker
    ''' <summary>
    ''' Which tab is the mouse hovering over or dragging?
    ''' </summary>
    ''' <returns>Tab position number (MarkerType.No_Marker if none)</returns>
    <Browsable(False)>
    Public ReadOnly Property TabUnderMouse() As Integer
        Get
            If Me._CurrentMarker = RulerMarkerType.NoMarker _
                OrElse Me._CurrentTab = RulerMarkerType.NoMarker Then
                '   not over a tab
                Return RulerMarkerType.NoMarker
            Else
                '   else get position
                Return CInt(Me.UnZoomValue(Me._Tabs(Me._CurrentTab).X - 1.0F) + Me._ScrollOffset)
            End If
        End Get
    End Property

    Private _TabsEnabled As Boolean = True
    ''' <summary>
    ''' Gets or sets whether ruler can be used to handle tabs
    ''' </summary>
    ''' <value>True to enable, False to disable</value>
    ''' <returns>True (default) if enabled, False if disabled</returns>
    <Category("Tabulation")>
    <Description("Gets or sets whether tabs are to be displayed.")>
    <DefaultValue(False)>
    Public Property TabsEnabled As Boolean
        Get
            Return Me._TabsEnabled
        End Get
        Set(ByVal value As Boolean)
            Me._TabsEnabled = value
            Me.Refresh()
        End Set
    End Property

    Private _MaxTabs As Integer = 32
    ''' <summary>
    ''' Gets or sets maximum number of tabs allowed when tabs are enabled
    ''' </summary>
    ''' <vale>Number of tabs allowed (must be non-negative)</vale>
    ''' <returns>Number of tabs allowed (defaults to 32)</returns>
    ''' <remarks>If number of existing tabs is greater than value given,
    ''' then this property is not changed</remarks>
    <Category("Tabulation")>
    <Description("Gets or sets how many tabs are allowed.")>
    <DefaultValue(GetType(Integer), "32")>
    Public Property MaximumTabs As Integer
        Get
            Return Me._MaxTabs
        End Get
        Set(value As Integer)
            If value < 0 Then
                value = 0 'make negative value 0
            End If
            If value <= Me._Tabs.Count Then
                Me._MaxTabs = value 'can set new ceiling
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets positions of tabs in inches/centimeters,
    ''' depending on value of Units property
    ''' </summary>
    ''' <value>Array of new tab positions</value>
    ''' <returns>Array of existing tab positions</returns>
    <Browsable(False)>
    Public Property TabPositionsInUnits() As Single()
        Get
            '   get positions using a certain measurement
            Dim positions() As Integer = Me.TabPositions()
            Dim Units() As Single = Nothing
            If positions IsNot Nothing Then
                ReDim Units(positions.Length - 1)
                For i As Integer = 0 To positions.Length - 1
                    Units(i) = Me.PixelsToUnits(positions(i))
                Next i
            End If
            Return Units
        End Get
        Set(Units() As Single)
            '   set positions using a certain measurement
            Dim positions() As Integer = Nothing
            If Units IsNot Nothing Then
                ReDim positions(Units.Length - 1)
                For i As Integer = 0 To Math.Min(positions.Length, Me._MaxTabs) - 1
                    positions(i) = Me.UnitsToPixels(Units(i))
                Next i
            End If
            Me.TabPositions() = positions
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets positions of tabs in pixels
    ''' </summary>
    ''' <value>Array of new tab positions</value>
    ''' <returns>Array of existing tab positions</returns>
    <Browsable(False)>
    Public Property TabPositions() As Integer()
        Get
            '   get positions
            Dim positions(Me._Tabs.Count - 1) As Integer
            For i As Integer = 0 To Me._Tabs.Count - 1
                positions(i) = CInt(Me.UnZoomValue(Me._Tabs(i).X - 1.0F) + Me._ScrollOffset)
            Next i
            Array.Sort(positions)
            Return positions
        End Get
        Set(positions() As Integer)
            '   set positions
            Array.Sort(positions)
            Me._Tabs.Clear()
            If positions IsNot Nothing Then
                For i As Integer = 0 To Math.Min(positions.Length, Me._MaxTabs) - 1
                    Dim TabValue As Single = Me.ZoomValue(positions(i)) + 1.0F
                    If Me.IsValidPosition(TabValue, RulerMarkerType.Tab) Then
                        '   tab stop can't be outside margins or redundant
                        If i = 0 OrElse positions(i) > positions(i - 1) Then
                            Dim rect As RectangleF =
                            New RectangleF(TabValue - Me._ScrollOffset, 10.0F, 8.0F, 8.0F)
                            Me._Tabs.Add(rect)
                        End If
                    End If
                Next i
            End If
            Me.Refresh()
        End Set
    End Property
    ''' <summary>
    ''' Gets number of pixels per centimeter
    ''' </summary>
    ''' <returns>Number of pixels/cm</returns>
    <Category("Measurements")>
    <Description("Gets number of pixels per centimeter.")>
    Public ReadOnly Property PixelsPerCentimeter() As Single
    ''' <summary>
    ''' Gets number of pixels per inch
    ''' </summary>
    ''' <returns>Number of pixels/inch</returns>
    <Category("Measurements")>
    <Description("Gets number of pixels per inch.")>
    Public ReadOnly Property PixelsPerInch() As Single

    Private _Units As RulerUnitType = RulerUnitType.Centimeters
    ''' <summary>
    ''' Gets or sets whether units are inches or centimeters
    ''' </summary>
    ''' <value>New type of unit</value>
    ''' <returns>Existing type of unit (default is inches)</returns>
    <Category("Measurements")>
    <DefaultValue(GetType(RulerUnitType), "Centimeters")>
    <Description("Gets or sets whether units are measured in millimeters, 1/16th inch, or pixels.")>
    Public Property Units() As RulerUnitType
        Get
            Return Me._Units
        End Get
        Set(value As RulerUnitType)
            Me._Units = value
            Me.Refresh()
        End Set
    End Property

    Private _ToolTipString As String = String.Empty
    ''' <summary>
    ''' Gets or sets normal tooltip for ruler
    ''' </summary>
    ''' <value>New tooltip text</value>
    ''' <returns>Existing tooltip text (defaults to null)</returns>
    <Category("ToolTips")>
    <Description("Gets or sets TooTip text for ruler.")>
    Public Property ToolTipText() As String
        Get
            Return Me._ToolTipString
        End Get
        Set(value As String)
            Me._ToolTipString = value
            Me.SetToolTip(RulerMarkerType.NoMarker)
        End Set
    End Property
    ''' <summary>
    ''' Gets or sets whether tooltip becomes name of marker when mouse is over one
    ''' </summary>
    ''' <value>True for yes (use marker names when over markers),
    ''' False for no (always stick with standard ToolTipText)</value>
    ''' <returns>True (default) or yes, False for no</returns>
    <Category("ToolTips")>
    <Description("Gets or sets whther TooltTips change when over markers and tabs.")>
    Public Property UsingSmartToolTips() As Boolean = True

    Private _strokeColor As Color = Color.Black
    ''' <summary>
    ''' Gets or sets color of ruler's border
    ''' </summary>
    ''' <value>New border color</value>
    ''' <returns>Existing border color (defaults to black)</returns>
    <Category("Appearance")>
    <DefaultValue(GetType(Color), "Black")>
    <Description("Color of the border drawn on the bounds of control.")>
    Public Property BorderColor As Color
        Get
            Return Me._strokeColor
        End Get
        Set(ByVal value As Color)
            Me._strokeColor = value
            Me.Refresh()
        End Set
    End Property

    Private _ZoomFactor As Single = 1.0F
    ''' <summary>
    ''' Gets or sets zoom factor for ruler
    ''' </summary>
    ''' <value>Factor to multiply "true" margin/indent/tab positions by (must be positive!)</value>
    ''' <returns>Factor by which "true" positions are multiplied</returns>
    ''' <remarks>Changing the zoom factor only changes the visual scaling of the ruler and of
    ''' the positions of markers; if DOES NOT effect the values gotten/set for the margin,
    ''' indent, and tab properties (that is, their pixel/unit values are always treated as if
    ''' ZoomFactor were 1.0).</remarks>
    <Category("Appearance")>
    <DefaultValue(GetType(Single), "1.0")>
    <Description("Zoom factor for control.")>
    Public Property ZoomFactor As Single
        Get
            Return Me._ZoomFactor
        End Get
        Set(value As Single)
            If value <= 0F Then
                Throw New ArgumentException("Invalid zoom factor!")
            ElseIf value <> Me._ZoomFactor Then
                '   re-position information
                Dim Adjustment As Single = value / Me._ZoomFactor
                '   margins
                Me.lMargin = Adjustment * (Me.lMargin - 1.0F) + 1.0F
                Me.rMargin = Adjustment * (Me.rMargin - 1.0F) + 1.0F
                '   indents
                Me.llIndent = Adjustment * (Me.llIndent - 1.0F) + 1.0F
                Me.luIndent = Adjustment * (Me.luIndent - 1.0F) + 1.0F
                Me.rIndent = Adjustment * (Me.rIndent - 1.0F) + 1.0F
                '   tabs
                For i = 0 To Me._Tabs.Count - 1
                    Me._Tabs(i) =
                    New RectangleF(
                        Adjustment * (Me._Tabs(i).X + Me._ScrollOffset - 1.0F) + 1.0F - Me._ScrollOffset,
                        Me._Tabs(i).Y, Me._Tabs(i).Width, Me._Tabs(i).Height)
                Next i
                '   re-draw
                Me._ZoomFactor = value
                Me.Refresh()
            End If
        End Set
    End Property
    ''' <summary>
    ''' Gets or sets base color for ruler
    ''' </summary>
    ''' <value>New base color</value>
    ''' <returns>Existing base color (defaults to white)</returns>
    <Category("Appearance")>
    <DefaultValue(GetType(Color), "White")>
    <Description("Base color for the control.")>
    Public Property BaseColor As Color = Color.White

    Private lMargin As Single = 1.0F, rMargin As Single = 1.0F,
        llIndent As Single = 1.0F, luIndent As Single = 1.0F, rIndent As Single = 1.0F

    Private _MaxWidth As Single = 0F
    ''' <summary>
    ''' Get or sets the maximum width of ruler, including margins, in pixels
    ''' </summary>
    ''' <value>Maximum width for ruler area in pixels, including margins</value>
    ''' <returns>Maximum width of ruler area in pixels, including margins</returns>
    ''' <remarks>NOTES:
    ''' 1. Value cannot be too narrow to allow space between the rightmost left-indent
    '''    and the right indent
    ''' 2. This is PrintableWidth plus any margin widths</remarks>
    <Category("Ruler Dimensions")>
    Public Property RulerWidth() As Integer
        Get
            Return CInt(Me._MaxWidth - 2.0F)
        End Get
        Set(value As Integer)
            Dim ActualValue As Single = CSng(value) + 2.0F
            If ActualValue > Me.RightMostLeftIndent() + Me.rIndent Then
                Me._MaxWidth = CInt(ActualValue)
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets width of area within margins of ruler, in pixels
    ''' </summary>
    ''' <value>The maximum printable area within margins in pixels</value>
    ''' <returns>The maximum printable area with margins in pixels</returns>
    ''' <remarks>NOTES:
    ''' 1. Value cannot be too narrow to allow space between the rightmost left-indent
    '''    and the right indent
    ''' 2. This is MaximumWidth minus any margin widths</remarks>
    <Category("Ruler Dimensions")>
    Public Property PrintableWidth() As Integer
        Get
            Return Me.RulerWidth - CInt(Me.lMargin + Me.rMargin)
        End Get
        Set(value As Integer)
            Me.RulerWidth = value + CInt(Me.lMargin + Me.rMargin)
        End Set
    End Property

    Private _ScrollOffset As Single = 0F
    ''' <summary>
    ''' Get or sets the scrolling offset of ruler
    ''' </summary>
    ''' <value>Scrolling offset for ruler area in pixels</value>
    ''' <returns>Scrolling offset of ruler area in pixles</returns>
    ''' <remarks>Value cannot be larger than maximum ruler width</remarks>
    <Category("Ruler Dimensions")>
    Public Property ScrollingOffset() As Integer
        Get
            Return CInt(Me._ScrollOffset)
        End Get
        Set(value As Integer)
            If Me._ScrollOffset <= Me._MaxWidth Then
                '   move tabs to sync with new value
                For i As Integer = 0 To Me._Tabs.Count - 1
                    Me._Tabs(i) = New RectangleF(
                    Me._Tabs(i).X - (CSng(value) - Me._ScrollOffset), Me._Tabs(i).Y,
                    Me._Tabs(i).Width, Me._Tabs(i).Height)
                Next i
                Me._ScrollOffset = CSng(value)
                Me.Refresh()
            End If
        End Set
    End Property

    Private _NoMargins As Boolean = True
    ''' <summary>
    ''' Gets or sets whether margins are disabled
    ''' </summary>
    ''' <value>True to disable (no margins), False to enable (margins allowed)</value>
    ''' <returns>True if disabled (default), False if enabled</returns>
    ''' <remarks>NOTE: If margins pre-exist, then the left and right indent properties
    ''' are adjusted to point to the same locations as before</remarks>
    <Category("Margins")>
    <Description("Gets or sets whether margins are disabled."), DefaultValue(True)>
    Public Property NoMargins() As Boolean
        Get
            Return Me._NoMargins
        End Get
        Set(ByVal value As Boolean)
            Me._NoMargins = value
            If value Then
                '   erase margins
                Me.AdjustIndents(1.0F, 1.0F)
                Me.lMargin = 1.0F
                Me.rMargin = 1.0F
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets left margin, in pixels whenever ZoomFactor = 1
    ''' </summary>
    ''' <value>New margin (NoMargins property must be False)</value>
    ''' <returns>Existing margin (defaults to 0)</returns>
    ''' <remarks>NOTES:
    ''' 1. If NoMargins property is True, margin will not be set
    ''' 2. Left margin cannot be set to move rightmost left indent
    '''    too close to right indent</remarks>
    <Category("Margins")>
    <Description("Gets or sets left margin.")>
    <DefaultValue(0)>
    Public Property LeftMargin As Integer
        Get
            Return CInt(Me.UnZoomValue(Me.lMargin - 1.0F))
        End Get
        Set(ByVal value As Integer)
            If Not Me.NoMargins Then
                '   set left margin
                Dim ActualValue As Single = Me.ZoomValue(value) + 1.0F
                If Me.IsValidPosition(ActualValue, RulerMarkerType.LeftMargin) Then
                    Me.AdjustIndents(ActualValue, Me.rMargin)
                    Me.lMargin = ActualValue
                    Me.Refresh()
                End If
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets right margin, in pixels whenever ZoomFactor = 1
    ''' </summary>
    ''' <value>New margin</value>
    ''' <returns>Existing margin (defaults to 0)</returns>
    ''' <remarks>NOTES:
    ''' 1. If NoMargins property is True, margin will not be set
    ''' 2. Right margin cannot be set to move right indent
    '''    too close to rightmost left indent</remarks>
    <Category("Margins")>
    <Description("Gets or sets right margin.")>
    <DefaultValue(0)>
    Public Property RightMargin As Integer
        Get
            Return CInt(Me.UnZoomValue(Me.rMargin - 1.0F))
        End Get
        Set(ByVal value As Integer)
            If Not Me.NoMargins Then
                '   set right margin
                Dim ActualValue As Single = Me.ZoomValue(value) + 1.0F
                If Me.IsValidPosition(Me._MaxWidth - ActualValue, RulerMarkerType.RightMargin) Then
                    Me.AdjustIndents(Me.lMargin, ActualValue)
                    Me.rMargin = ActualValue
                    Me.Refresh()
                End If
            End If
        End Set
    End Property

    ''' <summary>
    ''' 'Gets or sets left hanging indent--offset, in pixels whenever ZoomFactor = 1,
    ''' from first-line left indent for all lines in a paragraph after the first
    ''' (negative for "first-line" indent, 0 for no indent offset, positive for true "hanging" indent)
    ''' </summary>
    ''' <value>New offset from left indent</value>
    ''' <returns>Existing offset from left indent (defaults to 0)</returns>
    ''' <remarks>NOTE: Hanging indent may not be set to get left of left margin</remarks>
    <Category("Indents")>
    <Description("Gets or sets left hanging indent.")>
    <DefaultValue(0)>
    Public Property HangingIndent As Integer
        Get
            Return CInt(Me.UnZoomValue(Me.llIndent - Me.luIndent))
        End Get
        Set(ByVal value As Integer)
            Dim ActualValue = Me.ZoomValue(value) + Me.luIndent
            If Me.IsValidPosition(ActualValue, RulerMarkerType.HangingIndent) Then
                '   set left indent for all lines but first
                Me.llIndent = ActualValue
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets first-line indent--offset, in pixels whenever ZoomFactor = 1,
    ''' from any left margin of the first line in a paragraph without changing indent
    ''' of subsequent lines
    ''' </summary>
    ''' <value>New first-line left-indent value</value>
    ''' <returns>Existing first-line left-indent value
    ''' (the same as LeftIndent property when reading; defaults to 0)</returns>
    ''' <remarks>NOTES:
    ''' 1. First-line indent cannot be set to move left of left margin or right of right indent
    ''' 2. This moves only the indent of the first-line in each paragraph; the indent of subsequent
    '''    lines remains unchanged, causing HangingIndent property to be counter-offset</remarks>
    <Category("Indents")>
    <Description("Gets or sets left indent.")>
    <DefaultValue(0)>
    Public Property FirstLineIndent As Integer
        Get
            Return CInt(Me.UnZoomValue(Me.luIndent - Me.lMargin))
        End Get
        Set(ByVal value As Integer)
            Dim ActualValue As Single = Me.ZoomValue(value) + Me.lMargin
            If Me.IsValidPosition(ActualValue, RulerMarkerType.FirstLineIndent) Then
                '   set first-line left indent
                Me.luIndent = ActualValue
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets left indent--offset, in pixels whenever ZoomFactor = 1, from any left margin
    ''' of first line in paragraph, moving indentation of subsequent lines along with it,
    ''' </summary>
    ''' <value>New first-line left-indent value
    ''' (cannot be make other lines go left of left MARGIN!)</value>
    ''' <returns>Existing first-line left-indent value
    ''' (the same as FirstLineIndent property when reading; defaults to 0)</returns>
    ''' <remarks>NOTES:
    ''' 1. Left indent cannot be set to move left of left margin or right of right indent
    ''' 2. All lines in paragraph are moved the same distance left or right,
    '''    causing HangingIndent property to remain unchanged</remarks>
    <Category("Indents")>
    <Description("Gets or sets left indent.")>
    <DefaultValue(0)>
    Public Property LeftIndent As Integer
        Get
            Return CInt(Me.UnZoomValue(Me.luIndent - Me.lMargin))
        End Get
        Set(ByVal value As Integer)
            Dim hi As Single = Me.ZoomValue(Me.llIndent - Me.luIndent)
            Dim ActualValue As Single = Me.ZoomValue(value) + Me.lMargin
            If Me.IsValidPosition(ActualValue + hi, RulerMarkerType.LeftIndents) Then
                '   set both left indents
                Me.luIndent = ActualValue
                Me.llIndent = Me.luIndent + hi
                Me.Refresh()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets right indent--offset, in pixels whenever ZoomFactor = 1,
    ''' from any right margin
    ''' </summary>
    ''' <value>New right-indent value</value>
    ''' <returns>Existing right-indent value (defaults to 0)</returns>
    ''' <remarks>NOTE: Right indent cannot be set to move
    ''' right of right margin or left of leftmost left indent</remarks>
    <Category("Indents")>
    <Description("Gets or sets right indent.")>
    <DefaultValue(0)>
    Public Property RightIndent() As Integer
        Get
            Return CInt(Me.UnZoomValue(Me.rIndent - Me.rMargin))
        End Get
        Set(ByVal value As Integer)
            Dim ActualValue As Single = Me.ZoomValue(value)
            If Me.IsValidPosition(Me._MaxWidth - ActualValue - Me.rMargin,
                RulerMarkerType.RightIndent) Then
                '   set right indent
                Me.rIndent = ActualValue + Me.rMargin
                Me.Refresh()
            End If
        End Set
    End Property

#End Region

#Region "  PUBLIC COMPONENTS: METHODS "

    ''' <summary>
    ''' Convert value from current unit type (inches or centimeters) to pixels
    ''' </summary>
    ''' <param name="value">Value in units specified by Units property</param>
    ''' <returns>Equivalent value in pixels</returns>
    Public Function UnitsToPixels(ByVal value As Single) As Integer
        If Me.Units = RulerUnitType.Centimeters Then
            '   unit is in centimeters
            Return CInt(value * Me.PixelsPerCentimeter)
        Else
            '   unit is in inches
            Return CInt(value * Me.PixelsPerInch)
        End If
    End Function

    ''' <summary>
    ''' Convert value from pixels to current unit type (inches or centimeters)
    ''' </summary>
    ''' <param name="value">Value in pixels</param>
    ''' <returns>Equivalent value in units specified by Units property</returns>
    <Browsable(False)>
    Public Function PixelsToUnits(ByVal value As Integer) As Single
        If Me._Units = RulerUnitType.Centimeters Then
            '   unit will be in centimeters
            Return CSng(value) / Me.PixelsPerCentimeter
        Else
            '   unit will be in inches
            Return CSng(value) / Me.PixelsPerInch
        End If
    End Function


#End Region

#Region "  PROTECTED COMPONENTS: METHODS "

    ''' <summary> handle redraw. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)
        With e.Graphics
            .CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality
            .InterpolationMode = InterpolationMode.HighQualityBicubic
            .SmoothingMode = SmoothingMode.AntiAlias
            .PixelOffsetMode = PixelOffsetMode.Half
            .TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit
        End With
        Me.Myself = New RectangleF(0F, 0F, CSng(Me.Width), CSng(Me.Height))
        Me.drawZone = New RectangleF(1.0F, 3.0F, CSng(Me.Width - 2), 14.0F)
        Me.workArea = New RectangleF(Math.Max(Me.lMargin - Me._ScrollOffset, 0), 3.0F, Me.drawZone.Width - Me.rMargin - Me.drawZone.X * 2, 14.0F)
        Me.DrawBackGround(e.Graphics)
        Me.DrawMargins(e.Graphics)
        Me.DrawTextAndMarks(e.Graphics)
        Me.DrawIndents(e.Graphics)
        Me.DrawTabs(e.Graphics)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        MyBase.OnResize(e)
        '   fix height at 20
        If Me.Height <> 20 Then
            Me.Height = 20
        End If
    End Sub

    ''' <summary> Gets the default font size. </summary>
    ''' <value> The default font size. </value>
    Public Property DefaultFontSize As Single = 9.0

    ''' <summary> Gets the default font size range. </summary>
    ''' <value> The default font size range. </value>
    Public Property DefaultFontSizeRange As Single = 0.5
    ''' <summary> Keeps font size withing range.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnFontChanged(ByVal e As EventArgs)
        MyBase.OnFontChanged(e)
        If Math.Abs(Me.DefaultFontSize - Me.Font.SizeInPoints) > 0.5 * Me.DefaultFontSizeRange Then
            Me.Font = New Font(Me.Font.FontFamily, Me.DefaultFontSize, Me.Font.Style, GraphicsUnit.Point)
        End If
    End Sub

#End Region

#Region "  PROTECTED COMPONENTS: MOUSE ACTIONS "

    ''' <summary> see if mouse is over an indent/margin marker or a tab. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        MyBase.OnMouseDown(e)
        Me.mCaptured = False
        Me.CheckForMarkerOrTab(e)
        If (e.Button And MouseButtons.Left) = MouseButtons.Left Then
            '   button pressed--set capture info
            Select Case Me._CurrentMarker
                Case RulerMarkerType.NoMarker
                    '   not over anything
                    Me.mCaptured = False
                Case RulerMarkerType.Tab
                    '   over a tab
                    Me.mCaptured = True
                    Me.pos = CInt(Me._Tabs(Me._CurrentTab).X)
                Case Else
                    '   over an indent/margin marker
                    Me.mCaptured = True
            End Select
        End If
        '   set up tooltip
        Me.SetToolTip(Me._CurrentMarker)
    End Sub

    ''' <summary> see if we are to add, remove, or move a tab. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)

        MyBase.OnMouseUp(e)
        '   mouse is down--what was tracked?
        If Not Me.workArea.Contains(e.Location) Then
            '   removing a tab?
            If Me.mCaptured AndAlso Me._TabsEnabled AndAlso Me._CurrentTab <> RulerMarkerType.NoMarker Then
                '   get tab position and remove it from list
                Try
                    Dim pos As Single = Me._Tabs(Me._CurrentTab).X
                    Me._Tabs.RemoveAt(Me._CurrentTab)
                    Me.Refresh()
                    RaiseEvent TabRemoved(Me, Me.CreateTabArgs(pos))
                Catch __unusedException1__ As Exception
                    '   ignore any error
                End Try
                Me._CurrentMarker = RulerMarkerType.NoMarker
                Me._CurrentTab = RulerMarkerType.NoMarker
            End If
        ElseIf Me.IsValidPosition(e.Location.X + Me._ScrollOffset, RulerMarkerType.Tab) Then
            '   adding or moving a tab?		
            If Not Me.mCaptured AndAlso Me._TabsEnabled Then
                '   new tab
                Me._CurrentMarker = RulerMarkerType.Tab
                Me.Refresh()
                If Me._Tabs.Count < Me._MaxTabs AndAlso Me.IsTabNew(e.Location.X) Then
                    Me.AddTab(CSng(e.Location.X))
                End If
            ElseIf Me.mCaptured AndAlso Me._CurrentTab <> RulerMarkerType.NoMarker _
                AndAlso Me.IsTabInOrder(Me._CurrentTab, e.Location.X) Then
                '   existing tab
                Me._CurrentMarker = RulerMarkerType.Tab
                Me.Refresh()
                RaiseEvent TabChanged(Me, Me.CreateTabArgs(e.Location.X))
            End If
        End If
        'capObject = MarkerType.No_Marker
        '   set up tool tip
        Me.SetToolTip(Me._CurrentMarker)
        Me.Refresh()
        '   no marker being tracked
        Me.mCaptured = False
        Me._CurrentMarker = RulerMarkerType.NoMarker
        Me._CurrentTab = RulerMarkerType.NoMarker
    End Sub

    ''' <summary> see if we are to drag an indent/margin marker or a tab. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)

        MyBase.OnMouseMove(e)
        Dim XPos As Single = e.Location.X + Me._ScrollOffset 'account for unseen region
        If (e.Button And MouseButtons.Left) = 0 Then
            '   mouse not down--check where it is for tool tip
            Me.CheckForMarkerOrTab(e)
            Me.SetToolTip(Me._CurrentMarker)
            Exit Sub
        End If
        '   button down--update marker or tab
        If Me.mCaptured AndAlso Me.IsValidPosition(XPos, Me._CurrentMarker) Then
            '   marker was captured--which one?
            Select Case Me._CurrentMarker
                Case RulerMarkerType.LeftMargin
                    '   moving left margin?
                    If Not Me.NoMargins Then
                        '   update left margin
                        Me.AdjustIndents(XPos, Me.rMargin)
                        Me.lMargin = XPos
                        Me.Refresh()
                        RaiseEvent MarginsChanged(Me,
                        New MarginOrIndentEventArgs(Me._CurrentMarker))
                    End If
                Case RulerMarkerType.RightMargin
                    '   moving right margin?
                    If Not Me.NoMargins Then
                        '   update right margin
                        Me.rMargin = Me._MaxWidth - XPos
                        Me.AdjustIndents(Me.lMargin, Me.rMargin)
                        Me.Refresh()
                        RaiseEvent MarginsChanged(Me,
                        New MarginOrIndentEventArgs(Me._CurrentMarker))
                    End If
                Case RulerMarkerType.FirstLineIndent
                    '   moving main left indent?
                    Me.luIndent = XPos
                    Me.Refresh()
                    RaiseEvent IndentsChanged(Me,
                    New MarginOrIndentEventArgs(Me._CurrentMarker))
                Case RulerMarkerType.RightIndent
                    '   moving right indent?
                    Me.rIndent = Me._MaxWidth - XPos
                    Me.Refresh()
                    RaiseEvent IndentsChanged(Me,
                    New MarginOrIndentEventArgs(Me._CurrentMarker))
                Case RulerMarkerType.HangingIndent
                    '   moving left hanging indent?
                    Me.llIndent = XPos
                    Me.Refresh()
                    RaiseEvent IndentsChanged(Me,
                    New MarginOrIndentEventArgs(Me._CurrentMarker))
                Case RulerMarkerType.LeftIndents
                    '    moving BOTH left indents?
                    Me.luIndent = XPos - (Me.llIndent - Me.luIndent)
                    Me.llIndent = XPos
                    Me.Refresh()
                    RaiseEvent IndentsChanged(Me,
                    New MarginOrIndentEventArgs(Me._CurrentMarker))
                Case RulerMarkerType.Tab
                    '   moving tab?
                    If Me._CurrentTab <> RulerMarkerType.NoMarker _
                        AndAlso Me.workArea.Contains(e.Location) _
                        AndAlso Me.IsTabInOrder(Me._CurrentTab, e.Location.X) Then
                        Me._Tabs(Me._CurrentTab) =
                        New RectangleF(CSng(e.Location.X), Me._Tabs(Me._CurrentTab).Y,
                            Me._Tabs(Me._CurrentTab).Width, Me._Tabs(Me._CurrentTab).Height)
                        Me.Refresh()
                    End If
            End Select
        End If
        '   set cursor
        If Me.NoMargins AndAlso
            (Me.markers(RulerMarkerType.LeftMargin).Contains(e.Location) _
                OrElse Me.markers(RulerMarkerType.RightMargin).Contains(e.Location)) Then
            '   inside margin space--use west-east cursor
            Me.Cursor = Cursors.SizeWE
        Else
            ' not inside margin--normal cursor
            Me.Cursor = Cursors.[Default]
        End If
        Me.SetToolTip(Me._CurrentMarker)
    End Sub

#End Region

End Class

''' <summary>
''' Event class for dealing with when a tab is added, moved, or removed
''' </summary>
Public Class TabEventArgs
    Inherits EventArgs

    ''' <summary> Constructor. </summary>
    ''' <param name="newPosition">      Get new position of tab. </param>
    ''' <param name="previousPosition"> Get original position of tab. </param>
    Public Sub New(ByVal newPosition As Integer, ByVal previousPosition As Integer)
        Me.NewPosition = newPosition
        Me.PreviousPosition = Me.PreviousPosition
    End Sub

    ''' <summary>
    ''' Get new position of tab
    ''' </summary>
    ''' <returns>New position of tab</returns>
    Public ReadOnly Property NewPosition As Integer

    ''' <summary>
    ''' Get original position of tab
    ''' </summary>
    ''' <returns>Original position of tab</returns>
    Public ReadOnly Property PreviousPosition As Integer

End Class

''' <summary>
''' Event class for dealing with when 1 or more 2 margins or indents are changed by the user
''' </summary>
Public Class MarginOrIndentEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' Set up margin/indent type
    ''' </summary>
    ''' <param name="markerType">Type of margin or indent</param>
    Public Sub New(ByVal markerType As RulerMarkerType)
        Me.MarkerType = markerType
    End Sub
    ''' <summary>
    ''' Get type of margin or indent(s)
    ''' </summary>
    ''' <returns>Margin/indent(s) type</returns>
    Public ReadOnly Property MarkerType As RulerMarkerType
End Class

''' <summary>
''' Enumerations for ruler's units (inches or centimeters)
''' </summary>
Public Enum RulerUnitType
    Centimeters
    Inches
End Enum

''' <summary>
''' Enumerations for margin, indent, and tab markers
''' </summary>
Public Enum RulerMarkerType
    NoMarker = -1
    LeftIndentBottom = 0
    LeftMargin = 1
    RightMargin = 2
    FirstLineIndent = 3
    RightIndent = 4
    HangingIndent = 5
    LeftIndents = 6
    Tab = 7
End Enum

