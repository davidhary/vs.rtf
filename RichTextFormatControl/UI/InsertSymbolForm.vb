﻿''' <summary> Form for viewing the insert symbol. </summary>
''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17, 1.0.*">
''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex. 
''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox </para></remarks>
Public Class InsertSymbolForm

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <param name="symbolFont"> The symbol font. </param>
    Public Sub New(ByVal symbolFont As Font)
        '   set parameter
        Me.SymbolFont = symbolFont
        Me.SymbolCharacter = " "
        '   this call is required by the designer.
        Me.InitializeComponent()
    End Sub

    ''' <summary> Input/Output: Gets the symbol font. </summary>
    ''' <value> The symbol font. </value>
    Public Property SymbolFont As Font

    ''' <summary> Output: Gets the symbol character. </summary>
    ''' <value> The symbol character. </value>
    Public Property SymbolCharacter As String

    Private Const CharactersAtATime As Integer = 1024
    Private Const TotalCharacters As Integer = 65536
    Private Const CharactersPerRow As Integer = 32

#End Region

#Region " PRIVATE VARIABLES "

    Private Property _Populating As Boolean = False
    Private Property _IsValidChar As Boolean = False 'status flags
    Private _SymbolList() As Char 'printable characters

    ''' <summary> Inserts a symbol load. get list of printable characters. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InsertSymbol_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me._SymbolList = Me.GetPrintableCharacters()
        With Me._SymbolRangeComboBox.Items
            .Clear()

            For StartCode As Integer = 0 To Me._SymbolList.Length - 1 _
                Step CharactersAtATime
                '    get character code range for this page
                Dim EndCode As Integer =
                Math.Min(Me._SymbolList.Length, StartCode + CharactersAtATime) - 1
                .Add(AscW(Me._SymbolList(StartCode)).ToString _
                  & "-" & AscW(Me._SymbolList(EndCode)).ToString)
            Next StartCode
        End With
        '   fill grid with characters and set font
        Me._SymbolRangeComboBox.SelectedIndex = 0
        Me.NewFont(Me.SymbolFont)
    End Sub

    ''' <summary> Symbols grid current cell Changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SymbolsGrid_CurrentCellChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _SymbolsGrid.CurrentCellChanged
        '   cell clicked
        With Me._SymbolsGrid
            If Not (Me._Populating OrElse .CurrentCell.Value Is Nothing) Then
                Dim character As String = .CurrentCell.Value.ToString
                Me._IsValidChar = Not String.IsNullOrEmpty(character)
                If Me._IsValidChar Then
                    '   character chosen
                    Me.SymbolCharacter = character
                    Me.IndicateSymbol(Me.SymbolCharacter)
                End If

            End If
        End With
    End Sub

    ''' <summary> Symbols grid cell double click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Data grid view cell event information. </param>
    Private Sub SymbolsGrid_CellDoubleClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles _SymbolsGrid.CellDoubleClick
        '   cell double-clicked
        If Me._IsValidChar Then
            Me.DialogResult = DialogResult.OK 'accept character
        End If
    End Sub

    ''' <summary> Inserts a button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InsertButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _InsertButton.Click
        Me.DialogResult = DialogResult.OK 'pick selected symbol
    End Sub

    ''' <summary> Cancel button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _CancelButton.Click
        Me.DialogResult = DialogResult.Cancel 'ignore selected symbol

    End Sub

    ''' <summary> Font button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _FontButton.Click
        '   change font
        With Me._InsertFontDialog
            .Font = Me.SymbolFont
            If .ShowDialog = DialogResult.OK Then
                '   redraw grid
                Me.SymbolFont = .Font
                Me.NewFont(Me.SymbolFont)
                Me._SymbolsGrid.Select()

            End If
        End With
    End Sub

    ''' <summary> Symbol range combo box selected item changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SymbolRangeComboBox_SelectedItemChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _SymbolRangeComboBox.SelectedIndexChanged
        If Me._SymbolRangeComboBox.SelectedIndex > -1 Then
            Me.NewGridRange(Me._SymbolRangeComboBox.SelectedIndex)
        End If
    End Sub

    '         change grid font and character range
    Private Sub NewGridRange(ByVal page As Integer)

        '   change character range
        With Me._SymbolsGrid
            '   flag that we are updating the grid
            .SuspendLayout() : Me._Populating = True
            '   get printable characters in this range
            '   fill grid
            Dim startPos As Integer = page * CharactersAtATime
            Dim endPos As Integer = Math.Min(Me._SymbolList.Length, startPos + CharactersAtATime)
            Dim symbolIndex As Integer = startPos
            Dim numberOfRows As Integer = Math.Ceiling((endPos - startPos) / CharactersPerRow)
            Dim rowInfo(CharactersPerRow - 1) As String
            .ColumnCount = CharactersPerRow
            With .Rows
                .Clear()

                For row As Integer = 0 To numberOfRows - 1
                    '   assign column values for row
                    For column As Integer = 0 To CharactersPerRow - 1
                        If symbolIndex >= endPos Then
                            '   past range of printable characters--use null string
                            rowInfo(column) = String.Empty
                        Else
                            '   printable character--use it
                            rowInfo(column) = Me._SymbolList(symbolIndex).ToString
                            symbolIndex += 1
                        End If
                    Next column
                    '   add row
                    .Add(rowInfo)
                Next row
            End With
            '   flag that we are done updating grid
            Me._Populating = False : .Invalidate() : .ResumeLayout()
            '   indicate that first character of grid is selected
            Me._IsValidChar = True
            Me.SymbolCharacter = Me._SymbolList(startPos) : Me.IndicateSymbol(Me.SymbolCharacter)
        End With
    End Sub

    ''' <summary> Creates a new font. configure grid for new font. </summary>
    ''' <param name="newFont"> The new font. </param>
    Private Sub NewFont(ByVal newFont As Font)
        '   change font
        With Me._SymbolsGrid
            .SuspendLayout()
            '   set new font
            .Font = New Font(newFont.Name, .DefaultCellStyle.Font.SizeInPoints, newFont.Style)
            .ResumeLayout()
            '   display font info
            Me.lblSymbolFont.Text = "Font: " & newFont.Name & "; " _
            & newFont.SizeInPoints & " points; " & CType(newFont.Style, FontStyle).ToString
        End With
    End Sub

    ''' <summary> Indicate symbol. display current symbol info </summary>
    ''' <param name="newSymbol"> The new symbol. </param>
    Private Sub IndicateSymbol(ByVal newSymbol As String)
        Me.lblSymbol.Text = $"Symbol: {newSymbol}  Shortcut: Alt+{AscW(newSymbol(0))}"
    End Sub

    ''' <summary> Gets printable characters. get string of printable characters. </summary>
    ''' <returns> The printable characters. </returns>
    Private Function GetPrintableCharacters() As Char()
        '   go through Unicode character list
        Dim symbolList As String = String.Empty
        For symbolIndex As Integer = 0 To TotalCharacters - 1
            '   check this character
            Dim Character As Char = ChrW(symbolIndex)
            Select Case Char.GetUnicodeCategory(Character)
                Case Globalization.UnicodeCategory.PrivateUse,
                  Globalization.UnicodeCategory.Format,
                  Globalization.UnicodeCategory.OtherNotAssigned
                    '   character unavailable
                    Continue For
            End Select
            Select Case True
                Case Character = " "c
                    '   space--printable
                    symbolList &= Character
                Case Char.IsControl(Character), Char.IsSeparator(Character),
                   Char.IsWhiteSpace(Character), Char.IsSurrogate(Character)
                    '   character not renderable
                    Continue For
                Case Else
                    '   printable character
                    symbolList &= Character
            End Select
        Next symbolIndex
        '   return with list of characters
        Return symbolList.ToCharArray
    End Function

#End Region

End Class