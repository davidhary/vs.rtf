Imports System.ComponentModel
Imports System.Linq
Imports System.Runtime.InteropServices
Imports System.Text.RegularExpressions
Imports isr.Controls.RichTextFormat.RichTextBoxExtensions

''' <summary> Rich Text Format control. </summary>
''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17, 1.0.*">
''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex. 
''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox
''' Spell checking thanks to: http://www.codeproject.com/KB/string/netspell.aspx </para></remarks>
Public Class RichTextFormatControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION and CLEANUP "

    Private ReadOnly Property InitializingComponenets As Boolean
    ''' <summary>
    ''' initialize components and search information
    ''' </summary>
    Public Sub New()
        MyBase.New
        Me._InitializingComponenets = True
        Me.InitializeComponent()
        Me.RichTextFormatHeader = Me.RichTextBox.Rtf
        Me._MaxTextWidth = Me.RichTextBox.GetMaximumWidth()
        Me._SearchInfo = New SearchCriteria("", "", RichTextBoxFinds.None, True)
        Me._LinkTooltip = New ToolTip With {.ShowAlways = True}
        Me._LinkTooltip.SetToolTip(Me.RichTextBox, "")
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        Me.PageSettings = New Printing.PageSettings
        Me.PrinterSettings = New Printing.PrinterSettings
        Me._InitializingComponenets = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PRIVATE (USERCONTROL) COMPONENTS: PRIVATE CONSTANTS "

    '      editing constants
    Private Const _HyphenChar As String = ChrW(173)
    Private Const _MaxFontSize As Single = 1638.35
    Private Const _NoBeginning As Integer = -1
    Private Const _NoEnd As Integer = -1
    Private Const _NotFound As Integer = -1
    Private Const _NotReplacing As Integer = -1
    Private Const _NoSpecificFileFormat As RichTextBoxStreamType = -1
    Private Const _ShowErrors As Boolean = True
    Private Const _DontShowErrors As Boolean = False
    Private Const _SelectionMargin As Single = 8.0F
    Private Const _LeftButtonPressed As Integer = 1
    Private Const _RightButtonPressed As Integer = 2
    Private Const _MiddleButtonPressed As Integer = 16
    Private Const _AnyButtonsPressed As Integer = _LeftButtonPressed Or _RightButtonPressed Or _MiddleButtonPressed
    Private Const _CustomLinkSyntax As String = "(?n)\{(?<Text>[^\{\|\}\r\n\s]+(\ +[^\{\|\}\r\n\s]+)*)\|(?<Hyperlink>[^\{\|\}\r\n\s]+(\ +[^\{\|\}\r\n\s]+)*)\}"

#End Region

#Region " PRIVATE (USERCONTROL) COMPONENTS: PRIVATE VARIABLES "

    '      standard editing variables

    Private _MouseButtonsPressed As MouseButtons = MouseButtons.None ' which mouse buttons are pressed?
    Private _UpdatingIndentsOrTabs As Boolean = False '                ruler being read? then don't update it
    Private _SettingFont As Boolean '     avoids event cascades during font-setting operations
    Private _SettingLinkMode As Boolean ' avoids event cascades during changes of link mode
    Private _DoingAll As Boolean '        handling whole text or just selection?
    Private _StartPosition As Integer = _NoBeginning
    Private _EndPosition As Integer = _NoEnd ' range for edit
    Private _MaxTextWidth As Integer = 0
    Private _SelectionOffset As Integer = 0 ' text-line width span
    Private _AreRedrawing As Boolean = True ' can we redraw?

    '      custom-link variables

    Private _ChangingText As Boolean = False 'are we in the middle of a text modification?
    Private _SettingProtection As Boolean = False 'are we setting protection status (upon change)?
    Private _CurrentCustomLink As CustomLinkInfo = Nothing 'any custom link mouse is over
    Private _PreviousHyperlink As String = String.Empty 'most recent hyperlink
    Private _UpdatingCustomLinks As Boolean = False 'avoids event cascades during custom-link updates
    Private ReadOnly _CustomLinks As SortedList(Of Integer, CustomLinkInfo) = New SortedList(Of Integer, CustomLinkInfo)() 'list of links in document
    Private ReadOnly _LinkTooltip As ToolTip = New ToolTip() 'custom-link tooltip (displays hyperlink)

    '      change-tracking variable:
    '         The following variable allows one to suppress firing of ChangesMade event
    '         when temporary, reversing changes are made, or to prevent multiple firings
    '         of it when a series of edits are basically one compound change; always
    '         set it to False when changes are meant to be ignored, then set it to True
    '         when changes are meant to be counted again
    Private _TrackingChanges As Boolean = True 'are current changes being tracked?

    '      protection-watching variable:
    '         The following variable is used to see if an edit operation succeeded,
    '         or failed due to text-protection; always set it to True right before
    '         performing an individual edit inside this code, and check it immediately
    '         after before doing any success-dependent action
    Private _IsNotProtected As Boolean = True 'trying to change possibly protected text

#End Region

#Region " UNSAFE METHODS:  WIN32 STUFF FOR CUSTOM LINKS "

    ''' <summary> An unsafe native methods. </summary>
    ''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2019-10-17, 1.0.*">
    ''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex. 
    ''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox </para></remarks>
    Private NotInheritable Class UnsafeNativeMethods
        Private Sub New()
            MyBase.New
        End Sub

        Public Const WM_USER As Int32 = &H400&
        Public Const EM_GETCHARFORMAT As Integer = WM_USER + 58, EM_SETCHARFORMAT As Integer = WM_USER + 68
        Public Const SCF_SELECTION As Integer = &H1
        Public Const CFE_LINK As UInt32 = &H20
        Public Const CFM_LINK As UInt32 = &H20 ' Exchange hyperlink extension

#Disable Warning IDE1006 ' Naming Styles
        ''' <summary> A char format 2 structure. </summary>
        <StructLayout(LayoutKind.Sequential)>
        Public Structure CHARFORMAT2_STRUCT
            Public cbSize As UInt32
            Public dwMask As UInt32
            Public dwEffects As UInt32
            Public yHeight As Int32
            Public yOffset As Int32
            Public crTextColor As Int32
            Public bCharSet As Byte
            Public bPitchAndFamily As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
            Public szFaceName() As Char
            Public wWeight As UInt16
            Public sSpacing As UInt16
            Public crBackColor As Integer ' Color.ToArgb() -> int
            Public lcid As Integer
            Public dwReserved As Integer
            Public sStyle As Int16
            Public wKerning As Int16
            Public bUnderlineType As Byte
            Public bAnimation As Byte
            Public bRevAuthor As Byte
            Public bReserved1 As Byte
        End Structure
#Enable Warning IDE1006 ' Naming Styles

        ''' <summary> Sends a message. </summary>
        ''' <param name="hWnd">   The window. </param>
        ''' <param name="msg">    The message. </param>
        ''' <param name="wParam"> The parameter. </param>
        ''' <param name="lParam"> The parameter. </param>
        ''' <returns> An IntPtr. </returns>
        <DllImport("user32.dll", CharSet:=CharSet.Auto)>
        Friend Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As UInt32, ByVal wParam As UIntPtr, ByVal lParam As IntPtr) As IntPtr
        End Function

#If False Then
        <DllImport("user32.dll", CharSet:=CharSet.Auto)>
        Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
        End Function
#End If
    End Class

    ''' <summary> A safe native methods. </summary>
    ''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2019-10-17, 1.0.*">
    ''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex. 
    ''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox </para></remarks>
    Private Class SafeNativeMethods

        '   Win32 link procedures

        ''' <summary> Enables the link. </summary>
        ''' <param name="handle"> The handle. </param>
        Public Shared Sub EnableLink(ByVal handle As IntPtr)
            SafeNativeMethods.SetSelectionStyle(handle, UnsafeNativeMethods.CFM_LINK, UnsafeNativeMethods.CFE_LINK)
        End Sub

        ''' <summary> Disables the link. </summary>
        ''' <param name="handle"> The handle. </param>
        Public Shared Sub DisableLink(ByVal handle As IntPtr)
            SafeNativeMethods.SetSelectionStyle(handle, UnsafeNativeMethods.CFM_LINK, 0)
        End Sub

        ''' <summary> Sets selection style. </summary>
        ''' <param name="handle"> The handle. </param>
        ''' <param name="mask">   The mask. </param>
        ''' <param name="effect"> The effect. </param>
        Private Shared Sub SetSelectionStyle(ByVal handle As IntPtr, ByVal mask As UInt32, ByVal effect As UInt32)
            '   modify current selection style
            Dim cf As New UnsafeNativeMethods.CHARFORMAT2_STRUCT()
            cf.cbSize = CType(Marshal.SizeOf(cf), System.UInt32)
            cf.dwMask = mask
            cf.dwEffects = effect
            Dim wpar As New UIntPtr(UnsafeNativeMethods.SCF_SELECTION)
            Dim lpar As IntPtr = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
            Marshal.StructureToPtr(cf, lpar, False)
            Dim res As IntPtr = UnsafeNativeMethods.SendMessage(handle, UnsafeNativeMethods.EM_SETCHARFORMAT, wpar, lpar)
            Marshal.FreeCoTaskMem(lpar)
        End Sub

        ''' <summary> Queries if a link is enabled. </summary>
        ''' <param name="handle"> The handle. </param>
        ''' <returns> <c>true</c> if a link is enabled; otherwise <c>false</c> </returns>
        Public Shared Function IsLinkEnabled(ByVal handle As IntPtr) As Boolean
            Return SafeNativeMethods.IsSelectionStyleInEffect(handle, UnsafeNativeMethods.CFM_LINK, UnsafeNativeMethods.CFE_LINK)
        End Function

        Private Shared Function IsSelectionStyleInEffect(ByVal handle As IntPtr, ByVal mask As UInt32, ByVal effect As UInt32) As Boolean
            '   test for given selection style
            Dim cf As New UnsafeNativeMethods.CHARFORMAT2_STRUCT()
            cf.cbSize = CType(Marshal.SizeOf(cf), System.UInt32)
            cf.szFaceName = New Char(31) {}
            Dim wpar As New UIntPtr(UnsafeNativeMethods.SCF_SELECTION)
            Dim lpar As IntPtr = Marshal.AllocCoTaskMem(Marshal.SizeOf(cf))
            Marshal.StructureToPtr(cf, lpar, False)
            Dim res As IntPtr = UnsafeNativeMethods.SendMessage(handle, UnsafeNativeMethods.EM_GETCHARFORMAT, wpar, lpar)
            cf = DirectCast(Marshal.PtrToStructure(lpar, GetType(UnsafeNativeMethods.CHARFORMAT2_STRUCT)), UnsafeNativeMethods.CHARFORMAT2_STRUCT)
            Dim state = (cf.dwMask And mask) = mask AndAlso (cf.dwEffects And effect) = effect
            '   dwMask holds the information for which
            '   properties are consistent throughout the selection:
            Marshal.FreeCoTaskMem(lpar)
            Return state
        End Function

    End Class

#End Region

#Region " PRIVATE USER CONTROL EVENT PROCEDURES "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)
        MyBase.OnLoad(e)
        '   initialize the file path
        Me.NewDocument()
        If String.IsNullOrWhiteSpace(Me.FilePath) Then Me.FilePath = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        Me._SaveButton.Visible = Me.IsModified AndAlso Me.HasText
        '   set focus
        Me.OnEnter(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.VisibleChanged" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> object that contains the event data. </param>
    Protected Overrides Sub OnVisibleChanged(ByVal e As EventArgs)
        MyBase.OnVisibleChanged(e)
        If Me.Visible Then Me.OnEnter(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. 
    '''           Sizes ToolStrip, TextRuler, and RichTextBox to fill UserControl's space</summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        MyBase.OnResize(e)
#If False Then
        Dim yPosition As Integer = 0
        Dim rtbHeight As Integer = Me.Height
        Me.GetHorizontalInfo()
        If Me._ShowToolStrip Then
            With _TopToolStrip
                .SetBounds(0, 0, Me.Width, .Height)
                yPosition += .Height
                rtbHeight -= .Height
            End With
        End If
        If Me._ShowRuler Then
            '   ruler
            With Me._TopRuler
                .SetBounds(_SelectionOffset, yPosition, Me.Width - _SelectionOffset, .Height)
                Me.GetHorizontalInfo()
                yPosition += .Height
                rtbHeight -= .Height
            End With
        End If
        '   rich-text box
        Me.RichTextBox.SetBounds(0, yPosition, Me.Width, rtbHeight)
#Else
        Me.GetHorizontalInfo()
#End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Enter" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnEnter(ByVal e As EventArgs)
        MyBase.OnEnter(e)
        '   initialize tool-bar and give text box the focus
        Me.RichTextBox.Focus()
        Me.AutoValidate = Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.UpdateCustomLinks(True, True)
        Me.UpdateToolbarNoChangesTracked()
    End Sub

    ''' <summary> Tool strip visible changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TopToolStrip_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TopToolStrip.VisibleChanged
        '   adjust for whether ToolStrip is visible now
        Me.OnResize(e)
    End Sub

#End Region

#Region "  RICH TEXT BOX EVENT PROCEDURES "

    ''' <summary> Gets the rich text box. </summary>
    ''' <value> The rich text box. </value>
    Public ReadOnly Property RichTextBox As RichTextBox
        Get
            Return Me._Rtb
        End Get
    End Property

    ''' <summary> Event handler. Called by rich text box for key down events. Handle special characters. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub HandleKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _Rtb.KeyDown
        '   make sure any custom-links are protected when a non-modifier key is pressed
        If Me._MouseButtonsPressed <> MouseButtons.None Then
            e.SuppressKeyPress = True
            Exit Sub 'no key if mouse button pressed
        End If
        Select Case e.KeyCode
            Case Keys.None, Keys.ControlKey, Keys.LControlKey, Keys.RControlKey, Keys.ShiftKey, Keys.LShiftKey, Keys.RShiftKey
                Exit Sub 'no key or modifier key only
        End Select
        Me.UpdateCustomLinks(Me.RichTextBox.SelectionLength = 0, True)
        '   raise event to see if user wants to make a custom insertion
        Dim customInsert As InsertRtfTextEventArgs = New InsertRtfTextEventArgs(e)
        Me.OnInsertRtfText(customInsert)
        e.SuppressKeyPress = customInsert.KeyEventArgs.SuppressKeyPress
        Dim insertionText As String = customInsert.RtfText
        If String.IsNullOrEmpty(insertionText) AndAlso Me.AllowDefaultInsertText AndAlso Not e.SuppressKeyPress Then
            If e.Control Then
                '   check for default keyboard shortcuts
                Select Case e.KeyCode
                    Case Keys.OemMinus, Keys.Subtract
                        '   hyphens/dashes
                        If e.Alt Then
                            '   em dash ("—")
                            '      -- [Ctrl] + [Alt] + [-]
                            insertionText = "—"
                        Else
                            '   optional hyphen (display only when breaking line)
                            '      -- [Ctrl] + [-]
                            insertionText = "\-"
                        End If
                    Case Keys.Oemtilde
                        '   left quotes
                        If e.Shift Then
                            '   left double-quote
                            '      -- [Ctrl] + [Shift] + [~]
                            insertionText = "\ldblquote"
                        Else
                            '   left single-quote
                            '      -- [Ctrl] + [`]
                            insertionText = "\lquote"
                        End If
                    Case Keys.OemQuotes
                        '   right quotes
                        If e.Shift Then
                            '    right double-quote
                            '      -- [Ctrl] + [Shift] + ["]
                            insertionText = "\rdblquote"
                        Else
                            '   right single-quote
                            '      -- [Ctrl] + [']
                            insertionText = "\rquote"
                        End If
                    Case Keys.C
                        '   copyright ("©")
                        '      -- [Ctrl] + [Alt] + [C]
                        If e.Alt Then
                            insertionText = "©"
                        End If
                    Case Keys.R
                        '   registered trademark ("®")
                        '      -- [Ctrl] + [Alt] + [RC]
                        If e.Alt Then
                            insertionText = "®"
                        End If
                    Case Keys.T
                        '   trademark ("™")
                        '      -- [Ctrl] + [Alt] + [T]
                        If e.Alt Then
                            insertionText = "™"
                        End If
                    Case Keys.L
                        '   needed to make sure shortcut [Ctrl] + [Shift] + [L]
                        '   only makes lists when listing is actually enabled
                        If e.Shift Then
                            '   make sure bullets are enabled
                            e.SuppressKeyPress = Not Me._AllowLists
                            Exit Sub
                        End If
                End Select
            Else
                '   see if user wants to remove a link
                Select Case e.KeyCode
                    Case Keys.Back, Keys.Delete
                        If Me._DoCustomLinks Then
                            If Me.RichTextBox.SelectionLength > 0 Then
                                '   deleting a selection
                                Me.UpdateCustomLinks(False)
                                Me.RichTextBox.SelectedText = String.Empty
                                Me.UpdateCustomLinks(True)
                                e.SuppressKeyPress = True
                                Exit Sub
                            End If
                            '   see if we're deleting a link
                            Dim currentPos As Integer
                            Dim customLinkInfo As CustomLinkInfo = Nothing
                            If e.KeyCode = Keys.Back Then
                                '   are we backing over a link?
                                currentPos = Me.RichTextBox.SelectionStart + Me.RichTextBox.SelectionLength
                                If currentPos > 0 Then
                                    customLinkInfo = Me.GetCustomLink(-currentPos, True)
                                    If customLinkInfo Is Nothing AndAlso Me.AreWeBeforeACustomLink(currentPos - 1) Then
                                        customLinkInfo = Me._CustomLinks(currentPos)
                                    End If
                                End If
                            Else
                                '   are we deleting over a link?
                                currentPos = Me.RichTextBox.SelectionStart
                                If currentPos < Me.RichTextBox.TextLength Then
                                    customLinkInfo = Me.GetCustomLink(currentPos, True)
                                    If customLinkInfo Is Nothing AndAlso Me.AreWeBeforeACustomLink(currentPos) Then
                                        customLinkInfo = Me._CustomLinks(currentPos + 1)
                                    End If
                                End If
                            End If
                            '   see if we can remove any link here
                            If customLinkInfo IsNot Nothing Then
                                ' does user to remove it?
                                Beep()
                                Dim defaultButton As MessageBoxDefaultButton = If(Me._KeepHypertextOnRemove, MessageBoxDefaultButton.Button1, MessageBoxDefaultButton.Button2)
                                Dim details As String = $"About to delete a link to {ControlChars.CrLf}'{customLinkInfo.Hyperlink}'{ControlChars.CrLf}{ControlChars.CrLf}Keep aforementioned hyperlink in text?"
                                Dim result As DialogResult = MessageBox.Show(details, "Link Present", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, defaultButton)
                                If result <> DialogResult.Cancel Then
                                    '   yes--remove it
                                    Me._KeepHypertextOnRemove = result = DialogResult.Yes
                                    Me.DeleteCustomLink(customLinkInfo.Position)
                                End If
                                e.SuppressKeyPress = True
                            End If
                        End If
                End Select
            End If
        End If
        '   insert custom text if necessary
        If Not String.IsNullOrEmpty(insertionText) AndAlso Not e.SuppressKeyPress Then
            Me.SuppressRedraw()
            Me.ReplaceRtfText(insertionText) 'make insertion
            Me.ResumeRedraw()
            e.SuppressKeyPress = True 'absorb keystroke
        End If
    End Sub

    ''' <summary> Event handler. Called by _Rtb for key press events. 
    '''           Handles smart character conversions. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key press event information. </param>
    Private Sub HandleKeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles _Rtb.KeyPress
        '   make sure any custom-links are protected when a non-modifier key is pressed
        If Me._MouseButtonsPressed <> MouseButtons.None Then
            e.Handled = True
            Exit Sub 'no key if mouse button pressed
        End If
        Select Case AscW(e.KeyChar)
            Case Keys.ControlKey, Keys.LControlKey, Keys.RControlKey,
              Keys.ShiftKey, Keys.LShiftKey, Keys.RShiftKey
                e.Handled = True
                Exit Sub 'modifier key only
        End Select
        Me.UpdateCustomLinks(Me.RichTextBox.SelectionLength = 0, True)
        '   raise event to see if user wants to specify any "smart text" conversions
        Dim start As Integer = Me.RichTextBox.SelectionStart
        Dim length As Integer = Me.RichTextBox.SelectionLength
        Dim customInsert As SmartRtfTextEventArgs = New SmartRtfTextEventArgs(e)
        Me.OnSmartRtfText(customInsert)
        Dim smartText As String = customInsert.RtfText
        Dim precedingCharLength As Integer = customInsert.PrecedingCharacterCount
        If String.IsNullOrEmpty(smartText) AndAlso Me.AllowDefaultSmartText Then
            '   get current and preceding char
            Dim currentChar As String = e.KeyChar.ToString
            Dim previousChar As String = String.Empty
            Dim positionInLine As Integer = Me.GetPositionInLine(start)
            If positionInLine > 0 Then
                '   not at beginning of line--get previous char
                previousChar = Me.RichTextBox.Text.Substring(start - 1, 1)(0)
            End If
            '   check incoming char		
            Select Case currentChar
                Case "-"
                    '   dash--doubled for em dash?
                    If previousChar = "-" Then
                        smartText = "—"
                        precedingCharLength = 1 'em dash
                    End If
                Case """"
                    '   double-quote char
                    Select Case previousChar
                        Case "", " ", "—", "-", ControlChars.Tab, ChrW(8216)
                            smartText = "\ldblquote" 'use left double-quote
                        Case Else
                            smartText = "\rdblquote" 'use right double-quote
                    End Select
                Case "'"
                    '   single-quote char
                    Select Case previousChar
                        Case "", " ", "—", "-", ControlChars.Tab, ChrW(8220)
                            smartText = "\lquote" 'use left single-quote
                        Case Else
                            smartText = "\rquote" 'use right single-quote
                    End Select
            End Select
        End If
        '   replace with smart text if necessary
        If Not String.IsNullOrEmpty(smartText) Then
            Me.SuppressRedraw()
            Me.RichTextBox.Select(start - precedingCharLength, length + precedingCharLength) 'remove preceding chars?
            Me.ReplaceRtfText(smartText) 'make replacement
            Me.ResumeRedraw()
            e.Handled = True 'suppress keystroke
        End If
    End Sub

    ''' <summary> Event handler. Called by Me.RichTextBox for key up events. 
    '''           make sure are custom links are protected when key is released</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub HandleKeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _Rtb.KeyUp
        Me.UpdateCustomLinks(Me._MouseButtonsPressed = MouseButtons.None, True)
    End Sub

    ''' <summary>
    ''' Event handler. Called by Me.RichTextBox for [ protected] events. Handle attempts to modify protected
    ''' text and/or custom links.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleProtected(ByVal sender As Object, ByVal e As EventArgs) Handles _Rtb.[Protected]
        '   can't protect what isn't there
        If Me.RichTextBox.TextLength = 0 Then
            Me.RichTextBox.SelectionProtected = False
            Exit Sub
        End If
        '   flag that edit failed
        Me._IsNotProtected = False
        '   give developer-user ability for custom action
        Dim canceling As CancelEventArgs = New CancelEventArgs() With {.Cancel = False}
        Me.OnTextProtected(canceling)
        If canceling.Cancel Then
            '   developer wants to preempt automatic warning
            Exit Sub
        End If
        '   warn end-user--are custom links potentially involved?
        Dim s As String = String.Empty
        If Me._DoCustomLinks Then
            s = " and/or active link(s)"
        End If
        Beep()
        MessageBox.Show($"Region of text contains protected text {s}!{ControlChars.CrLf}{ControlChars.CrLf}To perform this edit, text protection(s){s}{ControlChars.CrLf}must be removed from affected text first.",
        "Invalid Edit", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    End Sub

#End Region

#Region "  RICH TEXT BOX EVENT PROCEDURES: MOUSE "

    ''' <summary> Event handler. Called by _Rtb for mouse up events. 
    ''' Enables custom-link protection when mouse button is up. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub HandleMouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _Rtb.MouseUp
        Me._MouseButtonsPressed = e.Button
        Me.UpdateCustomLinks(True, True)
    End Sub

    ''' <summary> Event handler. Called by _Rtb for mouse down events. 
    ''' Disables custom-link protection when mouse button is down</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub HandleMouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _Rtb.MouseDown
        Me._MouseButtonsPressed = e.Button
        Me.UpdateCustomLinks(False, True)
    End Sub

    ''' <summary> Event handler. Called by _Rtb for mouse move events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub HandleMouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _Rtb.MouseMove
        Dim hyperlink As String = String.Empty
        Me._MouseButtonsPressed = e.Button
        If Me._DoCustomLinks Then
            '   handle custom-link protection in case of drag-and-drop
            If Me._ProtectingLinks Xor Me._MouseButtonsPressed = MouseButtons.None Then
                '   toggle custom-link protection
                Me.UpdateCustomLinks(Not Me._ProtectingLinks, True)
            End If
            '   tooltip logic
            Dim currentPlace As Point = New Point(e.X, e.Y)
            Dim currentCharacter As Integer = Me.RichTextBox.GetCharIndexFromPosition(currentPlace)
            If currentCharacter >= 0 AndAlso currentCharacter < Me.RichTextBox.TextLength Then
                '   get current link, if any
                Me._CurrentCustomLink = Me.GetCustomLink(currentCharacter)
                If Me._CurrentCustomLink IsNot Nothing Then
                    '   make hyperlink text box's tooltip
                    hyperlink = Me._CurrentCustomLink.Hyperlink
                End If
            Else
                '   not over anything
                Me._CurrentCustomLink = Nothing
            End If
        End If
        '   display any hyperlink
        If hyperlink <> Me._PreviousHyperlink Then
            Me._LinkTooltip.SetToolTip(Me.RichTextBox, hyperlink)
            Me._PreviousHyperlink = hyperlink
        End If
    End Sub

#End Region

#Region "  RICH TEXT BOX EVENT PROCEDURES: DRAG and DROP "

    ''' <summary> Event handler. Called by _Rtb for drag drop events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>
    Private Sub HandleDragDrop(ByVal sender As Object, ByVal e As DragEventArgs) Handles _Rtb.DragDrop
        '   raise event for whole class
        Try
            Me._AutoDragDropInProgress = True 'set auto-drop flag
            Me.OnDragDrop(e) 'protect any custom links and raise class-level event
        Catch ex As Exception
            '   cancel drop on error
            e.Effect = DragDropEffects.None
            Throw
        Finally
            '   make sure auto-drop flag is reset even if error occurs
            Me._AutoDragDropInProgress = False
        End Try
        '   see if we can do default drop
        If e.Effect = DragDropEffects.None Then
            Exit Sub 'preempt default drop
        End If
        If Me.AreCustomLinksInSelection() Then
            '   don't do drop if custom links are affected
            e.Effect = DragDropEffects.None
            Beep()
            MessageBox.Show(
            $"Region of text contains link(s)!{ControlChars.CrLf}{ControlChars.CrLf}To drop, links(s) must be removed from affected text first.",
            "Invalid Edit", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End If
    End Sub

    ''' <summary> Event handler. Called by _Rtb for drag over events. </summary>
    ''' <remarks> Makes sure custom-link protection is disabled when mouse buttons are pressed
    ''' and preparations are made in case drop occurs over text containing links
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>
    Private Sub HandleDragOver(ByVal sender As Object, ByVal e As DragEventArgs) Handles _Rtb.DragOver
        If (e.KeyState And _AnyButtonsPressed) = 0 Xor Me._ProtectingLinks Then
            Me.UpdateCustomLinks(Not Me._ProtectingLinks, True)
        End If
        Me.OnDragOver(e) 'raise event for whole class
    End Sub

    ''' <summary> Event handler. Called by _Rtb for drag enter events. </summary>
    ''' <remarks> Makes sure custom-link protection Is disabled when mouse buttons are pressed
    ''' and preparations are made in case drop occurs over text containing links
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>
    Private Sub HandleDragEnter(ByVal sender As Object, ByVal e As DragEventArgs) Handles _Rtb.DragEnter
        If (e.KeyState And _AnyButtonsPressed) = 0 Xor Me._ProtectingLinks Then
            Me.UpdateCustomLinks(Not Me._ProtectingLinks, True)
        End If
        Me.OnDragEnter(e) 'raise event for whole class
    End Sub

    ''' <summary> Event handler. Called by _Rtb for drag leave events. </summary>
    ''' <remarks>Makes sure custom-link protection Is disabled when mouse buttons are pressed
    ''' and preparations are made in case drop occurs over text containing links
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Drag event information. </param>
    Private Sub HandleDragLeave(ByVal sender As Object, ByVal e As DragEventArgs) Handles _Rtb.DragLeave
        If (e.KeyState And _AnyButtonsPressed) = 0 Xor Me._ProtectingLinks Then
            Me.UpdateCustomLinks(Not Me._ProtectingLinks, True)
        End If
        Me.OnDragLeave(e) 'raise event for whole class
    End Sub

#End Region

#Region "  RICH TEXT BOX EVENT PROCEDURES: LINKS "

    ''' <summary> Event handler. Called by _Rtb for link clicked events. 
    '''           Handles clicked links. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Link clicked event information. </param>
    Private Sub HandleLinkClicked(ByVal sender As Object, ByVal e As LinkClickedEventArgs) Handles _Rtb.LinkClicked
        Dim customLinkInfoEA As CustomLinkInfoEventArgs = New CustomLinkInfoEventArgs()
        With customLinkInfoEA
            '   get link information
            If Me._DoCustomLinks AndAlso Me._CurrentCustomLink IsNot Nothing Then
                '   custom link--get one under mouse (there could be multiple adjacent ones)
                .CustomLinkInfo = Me._CurrentCustomLink
            Else
                '   standard (non-custom) link--rely on link text
                .CustomLinkInfo = New CustomLinkInfo With {.Position = -1, .Text = e.LinkText, .Hyperlink = e.LinkText}
            End If
            '   raise event
            Me.OnHyperlinkClicked(customLinkInfoEA)
        End With
    End Sub

#End Region

#Region "  RICH TEXT BOX EVENT PROCEDURES: TEXT AND SELECTIONS "

    ''' <summary> Event handler. Called by _Rtb for selection changed events. 
    '''           Updates buttons when text is selected. 
    '''           are turning custom links on or off? </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Rtb.SelectionChanged
        If Me._SettingLinkMode Then Exit Sub
        '   see if we need to normalize selection
        If Me._DoCustomLinks Then
            Me.RichTextBox.DetectUrls = False
            If Me._UpdatingCustomLinks Then
                Exit Sub
            Else
                Me.NormalizeSelectionForCustomLinks()
            End If
        End If
        '   handle ruler
        Me.GetHorizontalInfo()
        '   see which toolbar items should be updated
        Dim currentFont As Font = Me.RichTextBox.SelectionFont
        '   update font-attribute items
        If currentFont IsNot Nothing Then
            With currentFont
                '   update font name and size
                If Not Me._SettingFont Then
                    Me._SettingFont = True 'prevent font from being changed as it's reported
                    If Me._FontNameComboBox.Text <> .Name Then
                        Me._FontNameComboBox.Text = .Name
                    End If
                    If Me._FontSizeComboBox.Text <> CStr(.SizeInPoints) Then
                        Me._FontSizeComboBox.Text = CStr(.SizeInPoints)
                    End If
                    Me._SettingFont = False
                End If
                '   update font styles
                Me._BoldButton.Checked = .Bold
                Me._BoldContextMenuItem.Checked = .Bold
                Me._ItalicButton.Checked = .Italic
                Me._ItalicContextMenuItem.Checked = .Italic
                Me._UnderlineButton.Checked = .Underline
                Me._UnderlineContextMenuItem.Checked = .Underline
                Me._StrikeThroughContextMenuItem.Checked = .Strikeout
            End With
        End If
        '   update superscript/subscript status items
        Me.HandleSuperOrSubScript(Me.RichTextBox.SelectionCharOffset)
        '   update alignment-attribute items
        Me._LeftAlignButton.Checked = (Me.RichTextBox.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Left)
        Me._CenterAlignButton.Checked = (Me.RichTextBox.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center)
        Me._RightAlignButton.Checked = (Me.RichTextBox.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Right)
        Me._LeftAlignContextMenuItem.Checked = Me._LeftAlignButton.Checked
        Me._CenterAlignContextMenuItem.Checked = Me._CenterAlignButton.Checked
        Me._RightAlignContextMenuItem.Checked = Me._RightAlignButton.Checked
        Me._HyperlinkButton.Checked = Me._DoCustomLinks AndAlso Me.AreCustomLinksInSelection()
        '   allow insertion between adjacent links
        If Me._DoCustomLinks AndAlso Me.RichTextBox.SelectionLength = 0 AndAlso Me.AreWeBeforeACustomLink(Me.RichTextBox.SelectionStart) Then
            Me.RichTextBox.SelectionProtected = False
        End If
    End Sub

    ''' <summary> Event handler. Called by _Rtb for text changed events. 
    '''           Enable/Disable options based on text changes. 
    '''           see if we need to alter custom-link info. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleTextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Rtb.TextChanged
        If Me._DoCustomLinks Then
            Me.RichTextBox.DetectUrls = False
            If Me._UpdatingCustomLinks Then
                Exit Sub
            Else
                Me.UpdateCustomLinks()
            End If
        End If
        '   Spell-Check, Find, and Hyphenate require text to be present
        '   set context-menu/toolbar options
        Dim isTextPresent As Boolean = Me.RichTextBox.TextLength > 0
        Dim canSpellCheck As Boolean = isTextPresent AndAlso Me._AllowSpellCheck
        Me._SpellCheckButton.Enabled = canSpellCheck
        Me._SpellCheckContextMenuItem.Enabled = canSpellCheck
        Me._SearchContextMenuItem.Enabled = isTextPresent
        Me._FindButton.Enabled = isTextPresent
        Me._FindContextMenuItem.Enabled = isTextPresent
        Me._ReplaceContextMenuItem.Enabled = isTextPresent
        Me._HyphenateButton.Enabled = isTextPresent
        Me._HyphenationContextMenuItem.Enabled = isTextPresent
        Me._RemoveHyphensContextMenuItem.Enabled = isTextPresent
        Me._RemoveHiddenHyphensContextMenuItem.Enabled = isTextPresent
        '   Find Next also requires a search string to have been given
        Dim isSearchTextPresent As Boolean = (isTextPresent AndAlso Me._SearchInfo.SearchText.Length > 0)
        Me._FindNextButton.Enabled = isSearchTextPresent
        Me._FindNextContextMenuItem.Enabled = isSearchTextPresent
        Me._FindPreviousContextMenuItem.Enabled = isSearchTextPresent
        '   refresh toolbar display
        Me.HandleSelectionChanged(sender, e)
        Me.HandleTrackedChange()
    End Sub

#End Region

#Region "  RICH TEXT BOX EVENT PROCEDURES: SCROLLING "

    ''' <summary> Event handler. Called by _Rtb for h scroll events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleHScroll(ByVal sender As Object, ByVal e As EventArgs) Handles _Rtb.HScroll
        If Not Me._UpdatingIndentsOrTabs Then
            '   check against text being over-scrolled
            Dim scrollPosition As Point = Me.RichTextBox.GetScrollPosition()
            With scrollPosition
                If .X > Me._MaxTextWidth - Me.RichTextBox.ClientRectangle.Width + Me._SelectionOffset Then
                    .X = Me._MaxTextWidth - Me.RichTextBox.ClientRectangle.Width + Me._SelectionOffset
                    Me.RichTextBox.SetScrollPosition(scrollPosition)
                End If
                Me._TopRuler.ScrollingOffset = .X
            End With
        End If
    End Sub

#End Region

#Region "  RULER EVENT PROCEDURES "

    ''' <summary> Text ruler visible changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TopRuler_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TopRuler.VisibleChanged
        '   adjust for whether TextRuler is visible now
        Me.OnResize(e)
    End Sub

    ''' <summary> Top ruler tab added. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tab event information. </param>
    Private Sub TopRuler_TabAdded(ByVal sender As Object, ByVal e As TabEventArgs) Handles _TopRuler.TabAdded, _TopRuler.TabRemoved, _TopRuler.TabChanged
        Me._UpdatingIndentsOrTabs = True 'suspend updating rule
        Me.UpdateCustomLinks(False, True)
        Me._SettingProtection = False
        Me.RichTextBox.SelectionTabs = Me._TopRuler.TabPositions 'set new tabs
        Me._SettingProtection = True
        Me.UpdateCustomLinks(True, True)
        Me._UpdatingIndentsOrTabs = False 'resume updating ruler
        Me.GetHorizontalInfo()
        Me.RichTextBox.Focus()
    End Sub

    ''' <summary> Top ruler indents changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Margin or indent event information. </param>
    Private Sub TopRuler_IndentsChanged(ByVal sender As Object, ByVal e As MarginOrIndentEventArgs) Handles _TopRuler.IndentsChanged
        With Me._TopRuler
            '   defer updating ruler
            Me._UpdatingIndentsOrTabs = True
            Me.ToggleRedrawMode(False)
            Me.UpdateCustomLinks(False, True)
            Me._SettingProtection = False
            '   set new indents
            Select Case e.MarkerType
                Case RulerMarkerType.FirstLineIndent
                    '   moving only first-line indent
                    Me.RichTextBox.SelectionIndent = .FirstLineIndent
                    Me.RichTextBox.SelectionHangingIndent = .HangingIndent
                Case RulerMarkerType.HangingIndent
                    '   moving only hanging indent
                    Me.RichTextBox.SelectionHangingIndent = .HangingIndent
                Case RulerMarkerType.LeftIndents
                    '   moving both left indents
                    Me.RichTextBox.SelectionIndent = .LeftIndent
                Case RulerMarkerType.RightIndent
                    '   moving right indent
                    Me.RichTextBox.SelectionRightIndent = .RightIndent
            End Select
            '   update ruler now
            Me.ToggleRedrawMode(Me._AreRedrawing)
            Me._SettingProtection = True
            Me.UpdateCustomLinks(True, True)
            Me._UpdatingIndentsOrTabs = False
            Me.GetHorizontalInfo()
            Me.RichTextBox.Focus()
        End With
    End Sub

#End Region

#Region "  SPELL CHECKER EVENT PROCEDURES "

    ''' <summary> Spell checker deleted word. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Spelling event information. </param>
    Private Sub SpellChecker_DeletedWord(ByVal sender As Object, ByVal e As NetSpell.SpellChecker.SpellingEventArgs) Handles _SpellChecker.DeletedWord
        Me.SuppressRedraw()
        '   select word for this event
        Me.RichTextBox.Select(Me._StartPosition + e.TextIndex, e.Word.Length)
        If Me._DoCustomLinks AndAlso Me.AreCustomLinksInSelection() Then
            '   make sure we avoid custom links
            Beep()
            MessageBox.Show("Text in active hyperlink(s) cannot be deleted.",
            "Link Present", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            '   delete word
            Me.RichTextBox.SelectedText = String.Empty
            If Me._IsNotProtected Then
                '   succeeded
                If Me._EndPosition > Me._StartPosition Then
                    Me._EndPosition -= e.Word.Length
                End If
                If Me._EndPosition > Me.RichTextBox.TextLength Then
                    Me._EndPosition = Me.RichTextBox.TextLength
                End If
            End If
        End If
        Me._IsNotProtected = True
        Me.ResumeRedraw()
    End Sub

    ''' <summary> Spell checker end of text. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SpellChecker_EndOfText(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SpellChecker.EndOfText
        Me.DoneWithRange()
    End Sub

    ''' <summary> Spell checker replaced word. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Replace word event information. </param>
    Private Sub SpellChecker_ReplacedWord(ByVal sender As Object, ByVal e As NetSpell.SpellChecker.ReplaceWordEventArgs) Handles _SpellChecker.ReplacedWord
        Me.SuppressRedraw()
        '   select word for this event
        Me.RichTextBox.Select(Me._StartPosition + e.TextIndex, e.Word.Length)
        If Me._DoCustomLinks AndAlso Me.AreCustomLinksInSelection() Then
            '   make sure we avoid custom links
            Beep()
            MessageBox.Show("Text in active hyperlink(s) cannot be replaced.", "Link Present", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            '   replace word
            Me.RichTextBox.SelectedText = e.ReplacementWord
            If Me._IsNotProtected Then
                '   succeeded
                If Me._EndPosition > Me._StartPosition Then
                    Me._EndPosition += e.ReplacementWord.Length - e.Word.Length
                End If
                If Me._EndPosition > Me.RichTextBox.TextLength Then
                    Me._EndPosition = Me.RichTextBox.TextLength
                End If
            End If
        End If
        Me._IsNotProtected = True
        Me.ResumeRedraw()
    End Sub

#End Region

#Region " EDIT SHORTCUT MENU EVENT PROCEDURES "

    ''' <summary> Copies the tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CopyContextMenuItem.Click
        Me.RichTextBox.Copy()
    End Sub

    ''' <summary> Select all tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SelectAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectAllContextMenuItem.Click
        Me.RichTextBox.SelectAll()
    End Sub

    ''' <summary> Cut tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CutContextMenuItem.Click
        Me.UpdateCustomLinks(False)
        Me.RichTextBox.Cut()
        Me._IsNotProtected = True
        Me.UpdateCustomLinks(True)
    End Sub

    ''' <summary> Pastes the tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PasteContextMenuItem.Click
        Me.UpdateCustomLinks(Me.RichTextBox.SelectionLength = 0)
        Me.RichTextBox.Paste()
        Me._IsNotProtected = True
        Me.UpdateCustomLinks(True)
    End Sub

    ''' <summary> Undo tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UndoToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _UndoContextMenuItem.Click
        Me.UpdateCustomLinks(False)
        Me.RichTextBox.Undo()
        Me._IsNotProtected = True
        Me.UpdateCustomLinks(True)
    End Sub

    ''' <summary> Redo tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RedoToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RedoContextMenuItem.Click
        Me.UpdateCustomLinks(False)
        Me.RichTextBox.Redo()
        Me._IsNotProtected = True
        Me.UpdateCustomLinks(True)
    End Sub

#End Region

#Region " FONT-SELECTION EVENT PROCEDURES "

    ''' <summary> Change font. </summary>
    ''' <returns> A DialogResult. </returns>
    Public Function ChangeFont() As DialogResult
        Dim result As DialogResult = DialogResult.OK
        Using fontDialog As New FontDialog
            fontDialog.Font = Me.GetCurrentFont()
            fontDialog.ShowColor = Me.SetColorWithFont
            If Me.SetColorWithFont Then
                '   setting color with font
                fontDialog.Color = Me.GetCurrentColor()
            End If
            result = fontDialog.ShowDialog
            If Windows.Forms.DialogResult.Cancel <> result Then
                '   new font (and maybe color)
                Me.UpdateCustomLinks(False, True)
                Me._SettingProtection = False
                Try
                    Me.RichTextBox.SelectionFont = fontDialog.Font
                    If Me.SetColorWithFont Then
                        '   setting color with font
                        Me.RichTextBox.SelectionColor = fontDialog.Color
                    End If
                Catch
                    Throw
                Finally
                    Me._SettingProtection = True
                End Try
                Me.UpdateCustomLinks(True, True)
                Me.UpdateToolbarNoChangesTracked()
            End If
        End Using
        Return result
    End Function

    ''' <summary> Font tool strip button click.
    '''           Shows Font dialog </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub FontToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FontButton.Click, _GeneralFontSettingsContextMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = "changing font"
            Me.ChangeFont()
        Catch ex As Exception
            MessageBox.Show($"Invalid picture format!{ControlChars.CrLf}{ex}", $"Exception {activity}",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Change font color. </summary>
    ''' <returns> A DialogResult. </returns>
    Public Function ChangeFontColor() As DialogResult
        Dim result As DialogResult = DialogResult.OK
        Using colorDialog As New ColorDialog
            colorDialog.Color = Me.GetCurrentColor()
            result = colorDialog.ShowDialog
            If Windows.Forms.DialogResult.Cancel <> result Then
                Me.UpdateCustomLinks(False, True)
                Me.RichTextBox.SelectionColor = colorDialog.Color
                Me.UpdateCustomLinks(True, True)
                Me.UpdateToolbarNoChangesTracked()
            End If
        End Using
        Return result
    End Function

    ''' <summary> Font color tool strip button click. 
    '''           Show Foreground Color dialog. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub FontColorToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FontColorButton.Click
        Dim activity As String = String.Empty
        Try
            activity = "changing font color"
            Me.ChangeFontColor()
        Catch ex As Exception
            MessageBox.Show($"Invalid picture format!{ControlChars.CrLf}{ex}", $"Exception {activity}",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Change back color. </summary>
    ''' <returns> A DialogResult. </returns>
    Public Function ChangeBackColor() As DialogResult
        Dim result As DialogResult = DialogResult.OK
        Using colorDialog As New ColorDialog
            colorDialog.Color = Me.GetCurrentBackgroundColor()
            result = colorDialog.ShowDialog
            If Windows.Forms.DialogResult.Cancel <> result Then
                Me.UpdateCustomLinks(False, True)
                Me.RichTextBox.SelectionBackColor = colorDialog.Color
                Me.UpdateCustomLinks(True, True)
                Me.UpdateToolbarNoChangesTracked()
            End If
        End Using
        Return result
    End Function

    ''' <summary> Back color tool strip button click. 
    '''           Show Background Color dialog. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub BackColorToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _BackColorButton.Click
        Dim activity As String = String.Empty
        Try
            activity = "changing background color"
            Me.ChangeBackColor()
        Catch ex As Exception
            MessageBox.Show($"Invalid picture format!{ControlChars.CrLf}{ex}", $"Exception {activity}",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Font name tool strip combo box drop down. 
    '''           Populate list of font names</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontNameToolStripComboBox_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FontNameComboBox.DropDown
        With Me._FontNameComboBox.Items
            '    set list of names
            .Clear()
            For Each FontFamily As FontFamily In FontFamily.Families
                .Add(FontFamily.Name)
            Next FontFamily
        End With
    End Sub

    ''' <summary> Font name tool strip combo box selected index changed.
    '''           Handles edit-box caret when an item is selected from the drop-down box </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontNameToolStripComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FontNameComboBox.SelectedIndexChanged
        If Not Me._SettingFont Then
            Me._SettingFont = True 'prevent recursive event cascade
            Try
                Dim result As Boolean = Me.AreSettingValidFontName(_DontShowErrors)
                If result Then
                    Me._FontNameComboBox.Select(Me._FontNameComboBox.Text.Length, 0)
                End If
            Catch
                Throw
            Finally
                Me._SettingFont = False
            End Try
        End If
    End Sub

    ''' <summary> Font name tool strip combo box text changed. 
    '''           New font name is being entered--show it in text box</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontNameToolStripComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FontNameComboBox.TextChanged
        If Not Me._SettingFont Then
            With Me._FontNameComboBox
                '   validate font name
                Me._SettingFont = True 'prevent recursive event cascade
                Try
                    Dim result As Boolean = Me.AreSettingValidFontName(_DontShowErrors)
                    If result Then
                        .Select(.Text.Length, 0)
                    End If
                Catch
                    Throw
                Finally
                    Me._SettingFont = False
                End Try
            End With
        End If
    End Sub

    ''' <summary> Font name tool strip combo box validating.
    '''           Validate new font name </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub FontNameToolStripComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _FontNameComboBox.Validating
        '   set font name
        If Me.AreSettingValidFontName(_ShowErrors) Then
            '   send focus to text box if OK
            Me.ResetFocus()
        Else
            '   restore focus back if error
            e.Cancel = True
        End If
    End Sub

    ''' <summary> Font size tool strip combo box leave.
    '''           Set new font size </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontSizeToolStripComboBox_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FontSizeComboBox.Leave
        '   set font size
        If Me.AreSettingValidFontSize(_ShowErrors) Then
            '   send focus to text box if OK
            Me.ResetFocus()
        Else
            '   restore focus back if error
            Me._FontSizeComboBox.Focus()
        End If
    End Sub

    ''' <summary> Font size tool strip combo box selected index changed.
    '''           Handles edit-box caret when an item is selected from the drop-down box </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontSizeToolStripComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FontSizeComboBox.SelectedIndexChanged
        If Not Me._SettingFont Then
            Me._SettingFont = True 'prevent recursive event cascade
            Dim result As Boolean = Me.AreSettingValidFontSize(_DontShowErrors)
            Me._SettingFont = False
        End If
        Me._FontSizeComboBox.Select(Me._FontSizeComboBox.Text.Length, 0)
    End Sub

    ''' <summary> Font size tool strip combo box text changed.
    '''           New font size is being entered--show it in text box </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FontSizeToolStripComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FontSizeComboBox.TextChanged
        If Not Me._SettingFont Then
            With Me._FontSizeComboBox
                Me._SettingFont = True 'prevent recursive event cascade
                '   validate font size
                Dim result As Boolean = Me.AreSettingValidFontSize(_DontShowErrors)
                .Select(.Text.Length, 0)
                Me._SettingFont = False
            End With
        End If
    End Sub

    ''' <summary> Font size tool strip combo box validating. 
    '''           Validates new font size</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub FontSizeToolStripComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _FontSizeComboBox.Validating
        '   set font size
        If Me.AreSettingValidFontSize(_ShowErrors) Then
            '   send focus to text box if OK
            Me.RichTextBox.Focus()
        Else
            '   restore focus back if error
            e.Cancel = True
        End If
    End Sub

    ''' <summary> Bold tool strip button click.
    '''           Switch Bold </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BoldToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _BoldButton.Click, _BoldContextMenuItem.Click
        Me.ToggleFontStyle(Drawing.FontStyle.Bold)
        Me.ResetFocus()
    End Sub

    ''' <summary> Italic tool strip button click.
    '''            Switch Italic </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ItalicToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ItalicButton.Click, _ItalicContextMenuItem.Click
        Me.ToggleFontStyle(Drawing.FontStyle.Italic)
        Me.ResetFocus()
    End Sub

    ''' <summary> Underline tool strip button click. 
    '''           Switch Underline</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UnderlineToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _UnderlineButton.Click,
                                                                                                                   _UnderlineContextMenuItem.Click
        Me.ToggleFontStyle(Drawing.FontStyle.Underline)
        Me.ResetFocus()
    End Sub

    ''' <summary> Strike through tool strip menu item click.
    '''           Switch Strike-through </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StrikeThroughToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _StrikeThroughContextMenuItem.Click
        Me.ToggleFontStyle(Drawing.FontStyle.Strikeout)
        Me.ResetFocus()
    End Sub

#End Region

#Region " TEXT-ALIGNMENT EVENT PROCEDURES "

    ''' <summary> Left tool strip button click.
    '''           Aligns text left </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LeftToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LeftAlignButton.Click, _LeftAlignContextMenuItem.Click
        Me.UpdateCustomLinks(False, True)
        Me.RichTextBox.SelectionAlignment = HorizontalAlignment.Left
        Me.UpdateCustomLinks(True, True)
        Me.ResetFocus()
    End Sub

    ''' <summary> Center tool strip button click. 
    '''           Align text center </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CenterToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CenterAlignButton.Click, _CenterAlignContextMenuItem.Click
        Me.UpdateCustomLinks(False, True)
        Me.RichTextBox.SelectionAlignment = HorizontalAlignment.Center
        Me.UpdateCustomLinks(True, True)
        Me.ResetFocus()
    End Sub

    ''' <summary> Right tool strip button click. 
    '''           Align text right</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RightToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RightAlignButton.Click, _RightAlignContextMenuItem.Click
        Me.UpdateCustomLinks(False, True)
        Me.RichTextBox.SelectionAlignment = HorizontalAlignment.Right
        Me.UpdateCustomLinks(True, True)
        Me.ResetFocus()
    End Sub

#End Region

#Region " LIST, SPELL-CHECK, PICTURE, AND SYMBOL EVENT PROCEDURES: "

    ''' <summary> Selects a list type. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleListSelectionMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ListNoneMenuItem.Click, _ListBulletMenuItem.Click,
                                                                                      _ListNumbersMenuItem.Click, _ListLowerCaseAlphaMenuItem.Click,
                                                                                      _ListUpperCaseAlphaMenuItem.Click, _ListLowerCaseRomanMenuItem.Click,
                                                                                      _ListUpperCaseRomanMenuItem.Click, _ListNoneContextMenuItem.Click,
                                                                                      _ListBulletContextMenuItem.Click, _ListNumberContextMenuItem.Click,
                                                                                      _ListLowerCaseAlphaContextMenuItem.Click, _ListUpperCaseAlphaContextMenuItem.Click,
                                                                                      _ListLowerCaseRomanContextMenuItem.Click, _ListUpperCaseRomanContextMenuItem.Click
        '   set list style for selected text
        Dim ctl As ToolStripMenuItem = DirectCast(sender, ToolStripMenuItem)
        Dim listStyle As RichTextFormatListStyle = CType(ctl.Tag, RichTextFormatListStyle)
        Me.UpdateCustomLinks(False)
        Me._SettingProtection = False
        Me.SuppressRedraw()
        Me.RichTextBox.SetListStyle(listStyle)
        Me.ResumeRedraw()
        Me._SettingProtection = True
        Me.UpdateCustomLinks(True)
        '   update toolbar
        Me.UpdateToolbarNoChangesTracked()
    End Sub

    ''' <summary> Spell check tool strip button click. 
    '''           Handles spell-check</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SpellCheckToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SpellCheckButton.Click, _SpellCheckContextMenuItem.Click
        '   do spell check on selection
        Me.UpdateCustomLinks(True, True)
        Me.GetRange()
        '   are there hyphens to account for?
        Dim doSpellCheck As Boolean = True
        If Me.RichTextBox.Find(_HyphenChar, Me._StartPosition, Me._EndPosition, RichTextBoxFinds.NoHighlight) > -1 Then
            Beep()
            doSpellCheck = DialogResult.OK = MessageBox.Show(
                $"Text to be spell-checked contains syllable hyphen(s).{ControlChars.CrLf}This might affect the accuracy of the spell-check.",
                "Hyphens Present", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
        End If
        '   do spell-check if given the OK
        If doSpellCheck Then
            With Me._SpellChecker
                If Me.RichTextBox.SelectionLength = 0 Then
                    .Text = Me.RichTextBox.Text         'all text
                Else
                    .Text = Me.RichTextBox.SelectedText 'selected text
                End If
                .SpellCheck()
            End With
        End If
    End Sub

    ''' <summary> Inserts a picture into rich text-box. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub InsertPictureToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InsertPictureButton.Click, _InsertPictureContextMenuItem.Click
        Me.UpdateCustomLinks(Me.RichTextBox.SelectionLength = 0, True)
        Dim activity As String = String.Empty
        Try
            activity = "inserting picture"
            Me.InsertPicture()
        Catch ex As Exception
            MessageBox.Show($"Invalid picture format!{ControlChars.CrLf}{ex}", $"Exception {activity}",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
        Me.UpdateCustomLinks(True, True)
    End Sub

    ''' <summary> Inserts a symbol. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InsertSymbolToolStripButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _InsertSymbolButton.Click, _InsertSymbolContextMenuItem.Click
        Dim insertSymbolForm As InsertSymbolForm = New InsertSymbolForm(Me.GetCurrentFont)
        With insertSymbolForm
            If .ShowDialog() = DialogResult.OK Then
                '   symbol was selected
                Me.SuppressRedraw()
                Me.UpdateCustomLinks(Me.RichTextBox.SelectionLength = 0, True)
                Me._SettingProtection = False
                Me.RichTextBox.SelectionFont = .SymbolFont
                If Me._IsNotProtected Then
                    Me.RichTextBox.SelectedText = .SymbolCharacter
                End If
                Me._SettingProtection = True
                Me.UpdateCustomLinks(True, True)
                Me._IsNotProtected = True
                Me.ResumeRedraw()
            End If
        End With
    End Sub

#End Region

#Region " TEXT-SEARCHING AND HYPHENATION EVENT PROCEDURES "

    ''' <summary> Searches for the first tool strip button click. 
    '''           Handles searching for and/or replacing sub-text</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FindToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FindButton.Click, _FindContextMenuItem.Click
        '    find/replace
        Me.FindOrReplaceText(True)
    End Sub

    ''' <summary> Replace tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReplaceToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReplaceContextMenuItem.Click
        '   replace/find
        Me.FindOrReplaceText(True)
    End Sub

    ''' <summary> Searches for the next tool strip button click. 
    '''           Handles searching for subsequent appearances of sub-text </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FindNextToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FindNextButton.Click, _FindNextContextMenuItem.Click, _FindPreviousContextMenuItem.Click
        Me.UpdateCustomLinks(True)
        Dim direction As Boolean = sender Is Me._FindPreviousContextMenuItem
        With Me._SearchInfo
            If String.IsNullOrEmpty(.SearchText) Then
                '   no text given? (shouldn't happen)
                Beep()
                MessageBox.Show("No search string was specified!")
            Else
                '   find it
                Dim currentIndex As Integer = Me.RichTextBox.SelectionStart
                Dim searchIndex As Integer = Me.FindOccurrence(direction)
                If searchIndex = currentIndex Then
                    '   were we already on it? then search for another
                    searchIndex = Me.FindOccurrence(direction)
                End If
                '   have we exhausted our search?
                If searchIndex = _NotFound Then
                    Me.ReportDoneSearching(, direction)
                End If
            End If
            Me.UpdateToolbarNoChangesTracked()
        End With
    End Sub

    ''' <summary> Hyphenate tool strip button click.
    '''           Handle hyphenation </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HyphenateToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HyphenateButton.Click,
            _HyphenateTextContextMenuItem.Click
        '   hyphenate
        Me.HyphenateText()
    End Sub

    ''' <summary> Removes all hyphens tool strip menu item 1 click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RemoveAllHyphensToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _RemoveHyphensContextMenuItem.Click
        '  remove all hyphens
        Me.RemoveHyphens(True)
    End Sub

    Private Sub RemoveHiddenHyphensOnlyToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _RemoveHiddenHyphensContextMenuItem.Click
        '   de-hyphenate -- remove hyphens not at line-ends
        Me.RemoveHyphens(False)
    End Sub

#End Region

#Region " RULER MEASUREMENT MENU EVENT PROCEDURES "

    ''' <summary> Measure in centimeters tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeasureInCentimetersToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _CentimetersContextMenuItem.Click,
                                                                                                      _InchesContextMenuItem.Click
        With Me._TopRuler
            '   set checks
            Me._InchesContextMenuItem.Checked =
            sender Is Me._InchesContextMenuItem
            Me._CentimetersContextMenuItem.Checked =
            sender Is Me._CentimetersContextMenuItem
            '   which one is it?
            If sender Is Me._InchesContextMenuItem Then
                '   measure in inches now
                .Units = RulerUnitType.Inches
            Else
                '   measure in centimeters now
                .Units = RulerUnitType.Centimeters
            End If
        End With
    End Sub

#End Region

#Region " VERTICAL ALIGNMENT AND HYPERLINK EVENT PROCEDURES "

    ''' <summary> Raised superscript tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub RaisedSuperscriptToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _SuperscriptContextMenuItem.Click
        If Me.RichTextBox.SelectionCharOffset > 0 Then
            Me.HandleSuperOrSubScript(0, True)
        Else
            Me.HandleSuperOrSubScript(Me.GetCurrentFont().Size \ 2, True)
        End If
    End Sub

    ''' <summary> Lowered subscript tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LoweredSubscriptToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _SubscriptContextMenuItem.Click
        If Me.RichTextBox.SelectionCharOffset < 0 Then
            Me.HandleSuperOrSubScript(0, True)
        Else
            Me.HandleSuperOrSubScript(-Me.GetCurrentFont().Size \ 2, True)
        End If
    End Sub

    ''' <summary> Hyperlink tool Strip button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HyperlinkToolStripButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _HyperlinkButton.Click, _InsertEditRemoveHyperlinkContextMenuItem.Click
        '   check for link
        Me.UpdateCustomLinks(True)
        Dim start As Integer = Me.RichTextBox.SelectionStart
        Dim length As Integer = Me.RichTextBox.SelectionLength
        Dim existingLink As CustomLinkInfo = Me.GetCustomLink(start, (length > 0))
        Dim linkExists As Boolean = existingLink IsNot Nothing
        If Not linkExists Then
            '   link not pre-existing
            If Me.RichTextBox.SelectionProtected Then
                '   wholly protected but not a link
                Beep()
                MessageBox.Show("Protected non-link text is not available for a link.", "Invalid Edit", MessageBoxButtons.OK, MessageBoxIcon.Information,
                                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                Exit Sub
            ElseIf length > 0 Then
                '   validate selected text
                existingLink = Me.GetValidCustomLinkInfo(start, Me.RichTextBox.SelectedText)
                If existingLink Is Nothing Then
                    Beep()
                    MessageBox.Show("Invalid text for link.", "Invalid Text for Link", MessageBoxButtons.OK, MessageBoxIcon.Information,
                                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                    Exit Sub
                End If
            Else
                '   no selection--starting from scratch
                existingLink = New CustomLinkInfo With {.Position = start, .Text = String.Empty, .Hyperlink = String.Empty}
            End If
        End If
        '   let user set link info
        Dim hyperlinkForm As InsertLinkForm =
        New InsertLinkForm(Me.CopyCustomLinkInfo(existingLink), linkExists, Me._KeepHypertextOnRemove)
        With hyperlinkForm
            .ShowDialog()
            Me._KeepHypertextOnRemove = .KeepHypertext
            Select Case .LinkAction
                Case InsertLinkActions.Insert
                    '   insert new link
                    Me.RichTextBox.SelectedText = String.Empty
                    Me.InsertCustomLink(.CustomLinkInfo)
                Case InsertLinkActions.Remove
                    '   remove existing link
                    Me.DeleteCustomLink(existingLink.Position)
                Case InsertLinkActions.Update
                    '   update--replace existing link with new info
                    Me.DeleteCustomLink(existingLink.Position)
                    With .CustomLinkInfo
                        .Position = existingLink.Position - 1
                        Me.RichTextBox.Select(.Position, Me.RichTextBox.SelectionStart - .Position)
                    End With
                    Me.InsertCustomLink(.CustomLinkInfo)
            End Select
            '   clean up and exit
            Me._IsNotProtected = True
        End With
    End Sub

    ''' <summary> Removes all hyper-links tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RemoveAllHyperLinksToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _RemoveHyperLinksContextMenuItem.Click
        Me.UpdateCustomLinks(True)
        Me.RemoveCustomLinksInSelection(False)
    End Sub

    ''' <summary> Keep hyper-text when removing links tool strip menu item click. 
    '''           handle turning on/off hyperlink-text retention when removing custom links</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub KeepHyperTextWhenRemovingLinksToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _KeepHyperTextWhenRemovingLinksContextMenuItem.Click
        Me._KeepHypertextOnRemove = Not Me._KeepHypertextOnRemove
        Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Checked = Me._KeepHypertextOnRemove
    End Sub

#End Region

#Region " FILE CONTEXT MENU EVENTS "

    ''' <summary> Handles creating a new document. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleNewFile(sender As Object, e As EventArgs) Handles _NewFileContextMenuItem.Click, _NewFileButton.Click
        '   [Ctrl] + [O] = load text
        Dim args As CancelEventArgs = New CancelEventArgs()
        Me.TryCloseFile(args) ' see if we must save existing text
        Dim activity As String = String.Empty
        Try
            If Not args.Cancel Then
                activity = "clearing document for a new file"
                Me.NewDocument()
            End If
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Handles opening a file. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleFileOpen(ByVal sender As Object, ByVal e As EventArgs) Handles _FileOpenContextMenuItem.Click
        '   [Ctrl] + [O] = load text
        Dim args As CancelEventArgs = New CancelEventArgs()
        Me.TryCloseFile(args) 'see if we must save existing text
        Dim activity As String = String.Empty
        Try
            If Not args.Cancel Then
                activity = "loading file"
                Me.LoadFile()
            End If
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Try close file. </summary>
    ''' <param name="e"> Cancel event information. </param>
    Public Sub TryCloseFile(ByVal e As CancelEventArgs)
        If Me.IsModified AndAlso Me.HasText Then
            '   if recent changes were made, should we save them?
            Dim result As DialogResult = MessageBox.Show("The text has been modified. Save it?", "Text Changed!", MessageBoxButtons.YesNoCancel)
            Select Case result
                Case DialogResult.Cancel
                    '   don't close file
                    e.Cancel = True : Exit Sub
                Case DialogResult.Yes
                    '   save text
                    If Me.SaveFile() Then
                        Me.NewDocument()
                        MessageBox.Show("Changes saved. New document created", "File Closed")
                    Else
                        MessageBox.Show("Saving failed; file not closed.", "File not closed")
                        e.Cancel = True
                    End If
                Case DialogResult.No
                    '   don't save text
                    Me.NewDocument()
                    MessageBox.Show("Changes discarded. New document created", "File Closed")
            End Select
        End If
    End Sub

    ''' <summary> Handles Saving file. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleFileSave(ByVal sender As Object, ByVal e As EventArgs) Handles _FileOpenContextMenuItem.Click, _SaveButton.Click
        '   [Ctrl] + [S] = save
        Dim activity As String = String.Empty
        Try
            activity = "saving file"
            Me.SaveFile(Me.FileName)
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Handles Saving file as. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleFileSaveAs(ByVal sender As Object, ByVal e As EventArgs) Handles _FileOpenContextMenuItem.Click
        '   [Ctrl] + [Shift] + [S] = save as
        Dim activity As String = String.Empty
        Try
            activity = "saving file as"
            Me.SaveFile(String.Empty)
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> handles printing. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandlePrint(ByVal sender As Object, ByVal e As EventArgs) Handles _PrintContextMenuItem.Click
        '   [Ctrl] + [P] = print
        Dim activity As String = String.Empty
        Try
            activity = "printing"
            Me.Print(False)
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Handles quick printing using existing print settings. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleQuickPrint(ByVal sender As Object, ByVal e As EventArgs) Handles _QuickPrintContextMenuItem.Click
        '   [Ctrl] + [Shift] + [P] = quick print
        Dim activity As String = String.Empty
        Try
            activity = "quick printing"
            Me.QuickPrint()
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> handles preview printing. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandlePrintPreview(ByVal sender As Object, ByVal e As EventArgs) Handles _PrintPreviewContextMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = "print preview"
            Me.Print(True)
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Handles page setup. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandlePageSetup(ByVal sender As Object, ByVal e As EventArgs) Handles _QuickPrintContextMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = "page setup"
            Me.PageSetup()
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Handles closing the document. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleFileClose(ByVal sender As Object, ByVal e As EventArgs) Handles _FileCloseContextMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = "closing file"
            Me.TryCloseFile(New CancelEventArgs())
        Catch ex As Exception
            MessageBox.Show($"Exception occurred {activity}{ControlChars.CrLf}{ex}", $"{activity} failed",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

#End Region

#Region " NON-EVENT PROCEDURES "

    ''' <summary> Sets file format filters and default extensions. </summary>
    ''' <param name="fileDialog"> The file dialog. </param>
    ''' <param name="streamType"> Type of the <see cref="RichTextBoxStreamType"/> stream. </param>
    Private Sub SetFileFormatFilters(ByVal fileDialog As FileDialog, ByVal streamType As RichTextBoxStreamType)
        Select Case streamType
            Case _NoSpecificFileFormat
                '   rich or plain text
                fileDialog.DefaultExt = "rtf"
                fileDialog.Filter = "Rich text files (*.rtf)|*.rtf|Plain text files (*.txt)|*.txt|All files (*.*)|*.*"
            Case RichTextBoxStreamType.RichText, RichTextBoxStreamType.RichNoOleObjs
                '   rich text
                fileDialog.DefaultExt = "rtf"
                fileDialog.Filter = "Rich text files (*.rtf)|*.rtf|All files (*.*)|*.*"
            Case Else
                '   plain text
                fileDialog.DefaultExt = "txt"
                fileDialog.Filter = "Plain text files (*.txt)|*.txt|All files (*.*)|*.*"
        End Select
    End Sub

    ''' <summary> Determines file format. </summary>
    ''' <param name="fileName"> Name of RTF/plain-text file (if null or omitted, then a file dialog
    '''                         is invoked) </param>
    ''' <param name="streamType"> Type of the <see cref="RichTextBoxStreamType"/> stream. </param>
    ''' <returns> The file format. </returns>
    Private Function GetFileFormat(ByVal fileName As String, ByVal streamType As RichTextBoxStreamType) As RichTextBoxStreamType
        If streamType = _NoSpecificFileFormat Then
            '   if any format is allowed, check filename extension
            Return If(FileIO.FileSystem.GetName(fileName).ToLower.EndsWith(".rtf"),
                RichTextBoxStreamType.RichText,
                RichTextBoxStreamType.PlainText)
        Else
            '   return specified format
            Return streamType
        End If
    End Function

    ''' <summary> Gets horizontal information. </summary>
    Private Sub GetHorizontalInfo()
        With Me.RichTextBox
            '   sync ruler's magnification to text-box's
            If .ZoomFactor <> Me._TopRuler.ZoomFactor Then
                Me._TopRuler.ZoomFactor = .ZoomFactor
                Me.RichTextBox.Focus()
            End If
            '   account for any selection margin
            Me._SelectionOffset = .Padding.Left + 1
            If .ShowSelectionMargin Then
                Me._SelectionOffset += CInt(_SelectionMargin * Me.RichTextBox.ZoomFactor)
            End If
            '   determine maximum line width
            Me._MaxTextWidth = .GetMaximumWidth()
            If Me._MaxTextWidth > .ClientRectangle.Width - Me._SelectionOffset Then
                '   if larger than text-box client area, enable horizontal scroll bars
                .WordWrap = False
                .ScrollBars = .ScrollBars Or RichTextBoxScrollBars.Horizontal
            End If
            If Me.RichTextBox.RightMargin <> Me._MaxTextWidth Then
                Me.RichTextBox.RightMargin = Me._MaxTextWidth
            End If
        End With
#If False Then
        '   set up ruler if not updating indents or tabs
        With Me._TopRuler
            If Not Me._UpdatingIndentsOrTabs Then
                .SetBounds(_SelectionOffset, .Top, _MaxTextWidth, .Height)
                .PrintableWidth = _MaxTextWidth
                .ScrollingOffset = Me.RichTextBox.GetScrollPosition.X
                .FirstLineIndent = Me.RichTextBox.SelectionIndent
                .HangingIndent = Me.RichTextBox.SelectionHangingIndent
                .RightIndent = Me.RichTextBox.SelectionRightIndent
                If _AllowTabs Then
                    .TabPositions = Me.RichTextBox.SelectionTabs
                End If
            End If
        End With
#Else
        If Me.ShowRuler Then
            '   set up ruler if not updating indents or tabs
            With Me._TopRuler
                If Not Me._UpdatingIndentsOrTabs Then
                    .SetBounds(Me._SelectionOffset, .Top, Me._MaxTextWidth, .Height)
                    .PrintableWidth = Me._MaxTextWidth
                    .ScrollingOffset = Me.RichTextBox.GetScrollPosition.X
                    .FirstLineIndent = Me.RichTextBox.SelectionIndent
                    .HangingIndent = Me.RichTextBox.SelectionHangingIndent
                    .RightIndent = Me.RichTextBox.SelectionRightIndent
                    If Me._AllowTabs Then
                        .TabPositions = Me.RichTextBox.SelectionTabs
                    End If
                End If
            End With
        End If
#End If
    End Sub

    ''' <summary> Updates the toolbar no changes tracked. </summary>
    Private Sub UpdateToolbarNoChangesTracked()
        '   update toolbar without tracking changes
        Me._TrackingChanges = False
        Me.UpdateToolbar()
        Me._TrackingChanges = True
    End Sub

    ''' <summary> Updates the toolbar. </summary>
    Private Sub UpdateToolbar()
        '   update toolbar and raise ChangesMade event if text is changed
        Me.HandleTextChanged(Me.RichTextBox, New EventArgs())
        Me.ResetFocus()
    End Sub

    ''' <summary> Toggle redraw mode. </summary>
    ''' <param name="isRedrawing"> True if is redrawing, false if not. </param>
    Private Sub ToggleRedrawMode(ByVal isRedrawing As Boolean)
        Me.RichTextBox.SetRedrawMode(isRedrawing)
        Me._TopToolStrip.SetRedrawMode(isRedrawing)
        Me._TopRuler.SetRedrawMode(isRedrawing)
        If isRedrawing Then
            Me.RichTextBox.ResumeLayout()
        Else
            Me.RichTextBox.SuspendLayout()
        End If
    End Sub

    ''' <summary> Suppress redraw. </summary>
    Private Sub SuppressRedraw()
        Me._AreRedrawing = False
        Me.ToggleRedrawMode(Me._AreRedrawing)
    End Sub

    ''' <summary> Resume redraw. </summary>
    Private Sub ResumeRedraw()
        Me._AreRedrawing = True
        Me.ToggleRedrawMode(Me._AreRedrawing)
    End Sub

    ''' <summary> Query if this object is range a selection. </summary>
    ''' <returns> <c>true</c> if range a selection; otherwise <c>false</c> </returns>
    Private Function IsRangeASelection() As Boolean
        Return Me._StartPosition > _NoBeginning AndAlso Not Me._DoingAll
    End Function

    ''' <summary> Gets the range. Saves any range to text </summary>
    Private Sub GetRange()
        '   range default to all text
        Me._DoingAll = Me.RichTextBox.SelectionLength = 0
        If Me._DoingAll Then
            ' complete text
            Me._StartPosition = 0
            Me._EndPosition = Me.RichTextBox.TextLength
        Else
            ' get range of text
            Me._StartPosition = Me.RichTextBox.SelectionStart
            Me._EndPosition = Me._StartPosition + Me.RichTextBox.SelectionLength
        End If
    End Sub

    ''' <summary> Restore range. </summary>
    Private Sub RestoreRange()
        '   restore any range
        If Me.IsRangeASelection() AndAlso (Me.RichTextBox.SelectionStart <> Me._StartPosition OrElse Me.RichTextBox.SelectionLength <> Me._EndPosition - Me._StartPosition) Then
            Me.RichTextBox.Select(Me._StartPosition, Me._EndPosition - Me._StartPosition)
        End If
        Me._StartPosition = _NoBeginning
        Me._EndPosition = _NoEnd
    End Sub

    ''' <summary> Done with range.
    '''           Dispense with edit range </summary>
    ''' <param name="direction">      (Optional) The direction. </param>
    ''' <param name="areMovingCaret"> (Optional) True if are moving caret. </param>
    Private Sub DoneWithRange(Optional ByVal direction As RichTextBoxFinds = RichTextBoxFinds.None, Optional ByVal areMovingCaret As Boolean = True)
        '   finished with range--set caret
        If Me.MaintainSelection AndAlso Me.IsRangeASelection() Then
            '   restore selection
            Me.RestoreRange()
        Else
            '   set for no selection
            If areMovingCaret Then
                '   set caret
                If (direction And RichTextBoxFinds.Reverse) = 0 Then
                    '   go to end of range
                    Me.RichTextBox.Select(Me._EndPosition, 0)
                Else
                    '   go to beginning of range
                    Me.RichTextBox.Select(Me._StartPosition, 0)
                End If
            End If
            '   flag that we are no longer using a range
            Me._StartPosition = _NoBeginning
            Me._EndPosition = _NoEnd
        End If
        Me.UpdateToolbarNoChangesTracked()
    End Sub

    ''' <summary> Gets position in line.
    '''           get position in line of character index </summary>
    ''' <param name="index"> Zero-based index of the text characters. </param>
    ''' <returns> The position in line. </returns>
    Private Function GetPositionInLine(ByVal index As Integer) As Integer
        Return index - Me.RichTextBox.GetFirstCharIndexFromLine(Me.RichTextBox.GetLineFromCharIndex(index))
    End Function

    ''' <summary> Replace RTF text. </summary>
    ''' <param name="replacementText"> The replacement text. </param>
    Private Sub ReplaceRtfText(ByVal replacementText As String)
        '   remove existing text and insert RTF info
        Me.RichTextBox.InsertRtf(replacementText)
        Me._IsNotProtected = True
    End Sub

    ''' <summary> Hyphenate text.
    '''           Looks for words to hyphenate </summary>
    Private Sub HyphenateText()
        Me.UpdateCustomLinks(True)
        Me.GetRange()
        Me._ChangingText = True
        Me._TrackingChanges = False 'ignore temporary changes
        '   temporarily add line break to end of text
        Me.RichTextBox.Select(Me.RichTextBox.TextLength, 0)
        Me.RichTextBox.SelectionProtected = False
        Me.RichTextBox.SelectedText = ControlChars.Lf
        '   define variables
        Dim word As String = String.Empty
        Dim currentLine As Integer = Me.RichTextBox.GetLineFromCharIndex(Me._StartPosition) - 1
        Dim startOfLine, endOfLine, wordEnd, lineAtBreak, maxPosition As Integer
        '   parse text line by line
        Do
            '   get start of current line
            currentLine += 1
            startOfLine = Me.RichTextBox.GetFirstCharIndexFromLine(currentLine)
            '   see if first word is wrapped from last line
            If startOfLine = 0 Then
                '   beginning of text--don't break word
                Continue Do
            Else
                '   beginning of paragraph or page?
                Select Case Me.RichTextBox.Text.Substring(startOfLine - 1, 1)
                    Case ControlChars.Lf, ControlChars.FormFeed
                        '   yes--don't break word
                        Continue Do
                End Select
            End If
            '   get end of line
            If currentLine = Me.RichTextBox.Lines.GetUpperBound(0) Then
                endOfLine = Me.RichTextBox.TextLength 'already at last line
            Else
                endOfLine = Me.RichTextBox.GetFirstCharIndexFromLine(currentLine + 1)
            End If
            '    get end of first word in line
            wordEnd = startOfLine
            Do While wordEnd < endOfLine AndAlso Char.IsLetterOrDigit(Me.RichTextBox.Text.Chars(wordEnd))
                wordEnd += 1
            Loop
            '   get word (must allow for 2+ chars at end of last line
            '   and 3+ at beginning of current line)
            word = Me.RichTextBox.Text.Substring(startOfLine, wordEnd - startOfLine)
            maxPosition = word.Length - 2
            If maxPosition > 1 Then
                '   look for greatest position at which word may be broken
                Me.SuppressRedraw()
                Do
                    maxPosition -= 1
                    '   temporarily insert hyphen here and check line #
                    Me.RichTextBox.Select(startOfLine + maxPosition, 0)
                    If Me.RichTextBox.SelectionProtected Then
                        Continue Do 'don't hyphenate link
                    End If
                    Me.RichTextBox.SelectedText = _HyphenChar
                    lineAtBreak = Me.RichTextBox.GetLineFromCharIndex(startOfLine + maxPosition)
                    Me.RichTextBox.Undo() 'reverse insertion
                Loop Until maxPosition = 1 OrElse lineAtBreak < currentLine
                Me.ResumeRedraw()
                '   if word can be hyphenated, ask user if and where
                If lineAtBreak < currentLine AndAlso maxPosition > 1 Then
                    '   get user input
                    Using hyphenationForm As HyphenateForm = New HyphenateForm(word, maxPosition)
                        With hyphenationForm
                            Dim result As DialogResult = .ShowDialog()
                            '   what are we to do?
                            If result = DialogResult.Cancel Then
                                '   we are done
                                Exit Do
                            ElseIf .DesiredPosition > 0 Then
                                '   insert hyphen here
                                Me.RichTextBox.Select(startOfLine + .DesiredPosition, 0)
                                Me._TrackingChanges = True 'make sure this change is tracked
                                Me.RichTextBox.SelectedText = _HyphenChar
                                If Me._IsNotProtected Then
                                    Me._EndPosition += 1
                                End If
                                Me._IsNotProtected = True
                                Me._TrackingChanges = False
                            End If
                        End With
                    End Using
                End If
            End If
        Loop Until currentLine = Me.RichTextBox.GetLineFromCharIndex(Me._EndPosition)
        '   remove temporary line break at end
        Me.RichTextBox.Select(Me.RichTextBox.TextLength - 1, 1)
        If Me.RichTextBox.SelectedText = ControlChars.Lf Then
            Me.RichTextBox.SelectedText = String.Empty
        End If
        Me._ChangingText = False
        Me._TrackingChanges = True 'notice all changes again
        '   finished--restore selection
        Me.DoneWithRange()
    End Sub

    ''' <summary> Removes the hyphens; return ending position</summary>
    ''' <param name="removeAll"> True to remove all. </param>
    Private Sub RemoveHyphens(ByVal removeAll As Boolean)
        Me.UpdateCustomLinks(True)
        '   get selection range
        Me.GetRange()
        Me.SuppressRedraw()
        Me._ChangingText = True
        Dim hyphenPos As Integer =
        Me.RichTextBox.Find(_HyphenChar, Me._StartPosition, Me._EndPosition, RichTextBoxFinds.None) 'find first hyphen
        Do While hyphenPos > _NotFound AndAlso hyphenPos < Me._EndPosition
            '   do we remove this hyphen?
            If removeAll OrElse Me.GetPositionInLine(hyphenPos + 1) > 0 Then
                '   removing all hyphens or this hyphen is not at the end of a line
                If Not Me.RichTextBox.SelectionProtected Then
                    Me.RichTextBox.SelectedText = String.Empty
                    Me._EndPosition -= 1
                End If
            Else
                '   hyphen is at end of line--skip
                hyphenPos += 1
            End If
            hyphenPos =
            Me.RichTextBox.Find(_HyphenChar, hyphenPos, Me._EndPosition, RichTextBoxFinds.None) 'find next hyphen
        Loop
        '   finished--set caret to end of selection
        Me.DoneWithRange()
        Me.ResumeRedraw()
        Me._ChangingText = False
    End Sub

    ''' <summary> Searches for the first or replace text. </summary>
    ''' <param name="allowingReplacing"> True to allowing replacing. </param>
    Private Sub FindOrReplaceText(ByVal allowingReplacing As Boolean)
        '   initialize info
        Dim searchOption As SearchOptions = SearchOptions.None
        ' Dim searchForm As SearchForm = Nothing
        Dim isFirstTime As Boolean = True
        Me.UpdateCustomLinks(True)
        Me.GetRange()
        Me.RichTextBox.SelectionLength = 0
        Do
            Dim result As DialogResult
            Dim isDone As Boolean = False
            Dim replacementCount As Integer = _NotReplacing
            '   initialize search form
            Using searchForm As New SearchForm(Me._SearchInfo, allowingReplacing, Me._DoCustomLinks)
                With searchForm
                    '   display form
                    result = .ShowDialog()
                    If result = DialogResult.Cancel Then
                        '   finished!
                        Exit Do
                    End If
                    '   get info returned
                    searchOption = .SearchOption
                    Me._SearchInfo = .SearchInfo
                End With
            End Using
            '   what are we doing
            If searchOption = SearchOptions.None Then
                '   finished! (should be pre-empted by above logic)
                Exit Do
            ElseIf isFirstTime AndAlso Me.IsRangeASelection() Then
                If (Me._SearchInfo.SearchFinds And RichTextBoxFinds.Reverse) = 0 Then
                    Me.RichTextBox.Select(Me._StartPosition, 0)
                Else
                    Me.RichTextBox.Select(Me._EndPosition, 0)
                End If
            End If
            isFirstTime = False 'no longer an issue
            '   what should we do?
            Select Case searchOption
                Case SearchOptions.FindNext
                    '   find next occurrence
                    isDone = Me.FindNextText()
                    replacementCount = _NotReplacing
                Case SearchOptions.Replace
                    '   replace 1 occurrence
                    isDone = Me.ReplaceText()
                    replacementCount = _NotReplacing
                Case SearchOptions.ReplaceAll
                    '   replace all occurrences
                    replacementCount = Me.ReplaceAllText()
                    isDone = True
            End Select
            '   do we need to change the final selection?
            If isDone Then
                '   we've reached edge of available text
                Me.ReportDoneSearching(replacementCount)
            End If
        Loop
        '   searching finished--clean up
        If searchOption = SearchOptions.FindNext Then
            '   last did a Find--narrow selection to it
            Me._StartPosition = Me.RichTextBox.SelectionStart
            Me._EndPosition = Me._StartPosition + Me.RichTextBox.SelectionLength
        End If
        Me.DoneWithRange(Me._SearchInfo.SearchFinds, False)
    End Sub

    ''' <summary> Reports done searching. </summary>
    ''' <param name="replacementCount">   (Optional) Number of replacements. </param>
    ''' <param name="areFindingPrevious"> (Optional) True if are finding previous. </param>
    Private Sub ReportDoneSearching(Optional ByVal replacementCount As Integer = _NotReplacing, Optional ByVal areFindingPrevious As Boolean = False)
        '   redraw to show changes
        Me.ResumeRedraw()
        '   update info on the control
        Dim reportText As String = String.Empty
        With Me._SearchInfo
            '   are we search everything or just a region
            reportText = If(Me.IsRangeASelection(), "selected text", "text")
            '   finished searching
            Dim direction As RichTextBoxFinds = .SearchFinds
            If areFindingPrevious Then
                direction = (direction Xor RichTextBoxFinds.Reverse)
            End If
            If (direction And RichTextBoxFinds.Reverse) = 0 Then
                reportText = "Reached the end of the " & reportText & "!"
                '   go to end of range
                If Me.IsRangeASelection() Then
                    Me.RichTextBox.Select(Me._EndPosition, 0)         'end of selection 
                Else
                    Me.RichTextBox.Select(Me.RichTextBox.TextLength, 0) 'end of entire text
                End If
            Else
                reportText = "Reached the beginning of the " & reportText & "!"
                '   go to beginning of range
                If Me.IsRangeASelection() Then
                    Me.RichTextBox.Select(Me._StartPosition, 0) 'beginning of selection
                Else
                    Me.RichTextBox.Select(0, 0)        'beginning of entire text
                End If
            End If
            If replacementCount > _NotReplacing Then
                '   replacements were made
                reportText = $"{reportText}{ControlChars.CrLf}{ControlChars.CrLf}Number of replacements: {replacementCount}"
            End If
            '   report
            Beep()
            MessageBox.Show(reportText)
        End With
    End Sub

    ''' <summary> Searches for the next text; returns true if end of text. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function FindNextText() As Boolean
        '   find text
        With Me._SearchInfo
            '   get info
            Me.SuppressRedraw()
            Dim rearchIndex As Integer = Me.FindOccurrence()
            Me.ResumeRedraw()
            Return rearchIndex = _NotFound
        End With
    End Function

    ''' <summary> Replaces one instance of search text (if on it) -- return whether at end. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function ReplaceText() As Boolean
        With Me._SearchInfo
            '   get info
            Me.SuppressRedraw()
            .IsFirstFind = True
            Dim currentPos As Integer = Me.RichTextBox.SelectionStart
            Dim searchIndex As Integer = currentPos
            If Not Me.RichTextBox.SelectedText.Contains(.SearchText) Then
                searchIndex = Me.FindOccurrence()
            End If
            If searchIndex = currentPos Then
                '   already at an occurrence--replace it and move to next one
                searchIndex = Me.MakeReplacement(1)
            End If
            Me.ResumeRedraw()
            Return searchIndex = _NotFound
        End With
    End Function

    ''' <summary> Replaces all instances of search text -- return # of replacements. </summary>
    ''' <returns> An Integer. </returns>
    Private Function ReplaceAllText() As Integer
        With Me._SearchInfo
            '   get info
            Me.SuppressRedraw()
            .IsFirstFind = True
            Dim currentPos As Integer = Me.RichTextBox.SelectionStart
            Dim searchIndex As Integer = currentPos
            If Not Me.RichTextBox.SelectedText.Contains(.SearchText) Then
                searchIndex = Me.FindOccurrence()
            End If
            Dim replacementCount As Integer = 0
            '   make replacements
            Do While searchIndex > _NotFound
                .IsFirstFind = True
                searchIndex = Me.MakeReplacement(replacementCount)
            Loop
            Me.ResumeRedraw()
            '   return # of replacements
            Return replacementCount
        End With
    End Function

    ''' <summary> Searches for the first occurrence. </summary>
    ''' <param name="areFindingPrevious"> (Optional) True if are finding previous. </param>
    ''' <returns> The found occurrence. </returns>
    Private Function FindOccurrence(Optional ByVal areFindingPrevious As Boolean = False) As Integer
        With Me._SearchInfo
            Dim direction As RichTextBoxFinds = .SearchFinds
            If areFindingPrevious Then
                direction = (direction Xor RichTextBoxFinds.Reverse)
            End If
            '   look for item
            Dim searchPos As Integer = Me.RichTextBox.SelectionStart
            Dim searchIndex As Integer = _NotFound
            '   if we are on a custom link, move past it
            If Me._DoCustomLinks Then
                Dim link As CustomLinkInfo = Me.GetCustomLink(searchPos, True)
                If link IsNot Nothing Then
                    If (direction And RichTextBoxFinds.Reverse) = 0 Then
                        '   move to after link
                        searchPos = Me.GetEndOfCustomLink(link)
                    Else
                        searchPos = link.Position - 1
                    End If
                End If
            End If
            '   do find
            If (direction And RichTextBoxFinds.Reverse) = 0 Then
                '   searching down
                If Not .IsFirstFind Then
                    '   find subsequent occurrence
                    searchPos += 1
                End If
                If searchPos <= Me.RichTextBox.TextLength Then
                    searchIndex = Me.RichTextBox.Find(.SearchText, searchPos, _NoEnd, direction)
                End If
            Else
                '   searching up
                If Not .IsFirstFind Then
                    '   find previous occurrence
                    searchPos -= 1
                End If
                If searchPos >= 0 Then
                    searchIndex =
                    Me.RichTextBox.Find(.SearchText, 0, searchPos + .SearchText.Length, direction)
                End If
            End If
            If Me.IsRangeASelection() AndAlso (searchIndex < Me._StartPosition OrElse searchIndex > Me._EndPosition - .SearchText.Length) Then
                searchIndex = _NotFound 'out of range
            End If
            If searchIndex > _NotFound Then
                Dim link As CustomLinkInfo = Me.GetCustomLink(searchIndex, True)
                If link IsNot Nothing AndAlso Me.RichTextBox.SelectionLength = 0 Then
                    Me.RichTextBox.Select(link.Position - 1, Me.GetLengthOfCustomLink(link) + 1)
                End If
            End If
            '   return value
            .IsFirstFind = (searchIndex = _NotFound)
            Return searchIndex
        End With
    End Function

    ''' <summary> Replace an occurrence of search text -- return next occurrence position. </summary>
    ''' <param name="replacementCount"> [in,out] Number of replacements. </param>
    ''' <returns> An Integer. </returns>
    Private Function MakeReplacement(ByRef replacementCount As Integer) As Integer
        '   replacement count is adjusted here based on success or failure
        With Me._SearchInfo
            '   initialize
            Me._IsNotProtected = True
            Me._ChangingText = True
            Dim args As EventArgs = New EventArgs()
            Dim newPos As Integer = Me.RichTextBox.SelectionStart
            Dim targetTextLength As Integer = .SearchText.Length
            Dim isTooFar As Boolean = False
            '   see if we can overwrite links
            If Me._DoCustomLinks AndAlso Me.AreCustomLinksInSelection() Then
                targetTextLength = Me.RichTextBox.SelectionLength
                Me.UpdateCustomLinks(.AvoidLinks)
                Me._IsNotProtected = Not .AvoidLinks
            Else
                Me.UpdateCustomLinks(True)
            End If
            '   make change if allowed
            If Me._IsNotProtected Then
                Me.RichTextBox.SelectedText = .ReplacementText
            End If
            If Me._IsNotProtected Then
                '   replacement succeeded
                Me._EndPosition += .ReplacementText.Length - targetTextLength
                replacementCount += 1
            End If
            If Not Me._ProtectingLinks Then
                Me.UpdateCustomLinks(True)
            End If
            '   get new search start
            If (.SearchFinds And RichTextBoxFinds.Reverse) = 0 Then
                '   searching down
                If Me._IsNotProtected Then
                    '   replacement succeeded
                    newPos += .ReplacementText.Length
                    Me.RichTextBox.Select(Me.RichTextBox.SelectionStart - .ReplacementText.Length,
                    .ReplacementText.Length)
                    Me.RichTextBox.SelectionProtected = False
                Else
                    '   replacement failed
                    newPos += Me.RichTextBox.SelectionLength
                End If
                isTooFar = (newPos >= Me.RichTextBox.TextLength)
            Else
                '   searching up
                newPos -= .SearchText.Length
                isTooFar = (newPos < 0)
            End If
            Me._IsNotProtected = True
            Me._ChangingText = False
            '   get next occurrence
            If isTooFar Then
                '   new position is out of bounds
                Return _NotFound
            Else
                '   look for another occurrence
                Me.RichTextBox.Select(newPos, 0)
                Return Me.FindOccurrence()
            End If
        End With
    End Function

    ''' <summary> Determine if we are setting valid font name and sets new info</summary>
    ''' <param name="areShowingErrors"> True if are showing errors. </param>
    ''' <returns> <c>true</c> if setting valid font name; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Function AreSettingValidFontName(ByVal areShowingErrors As Boolean) As Boolean
        Dim newFontName As String = Me._FontNameComboBox.Text
        If Not String.IsNullOrEmpty(newFontName) Then
            For Each FontFamily As FontFamily In FontFamily.Families
                If FontFamily.Name = newFontName Then
                    '	new font exists--set it and leave
                    Dim currentFont As System.Drawing.Font = Me.GetCurrentFont()
                    If currentFont IsNot Nothing Then
                        Me.UpdateCustomLinks(False, True)
                        Try
                            '   set font face
                            Me.RichTextBox.SelectionFont =
                            New Drawing.Font(newFontName,
                                currentFont.Size, currentFont.Style)
                            Return True
                        Catch ex As Exception
                            '   invalid font name
                            If areShowingErrors Then
                                Beep()
                                MessageBox.Show("Invalid font name!", "Font Name")
                            End If
                            Return False
                        Finally
                            Me.UpdateCustomLinks(True, True)
                        End Try
                    End If
                End If
            Next FontFamily
        End If
        '   flag failure
        Return False
    End Function

    ''' <summary> Determine if we are setting valid font size. </summary>
    ''' <param name="areShowingErrors"> True if are showing errors. </param>
    ''' <returns> <c>true</c> if setting valid font size; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Function AreSettingValidFontSize(ByVal areShowingErrors As Boolean) As Boolean
        Dim newFontSize As Single
        If Single.TryParse(Me._FontSizeComboBox.Text, newFontSize) Then
            If newFontSize > 0 And newFontSize <= _MaxFontSize Then
                '   font size is within range--set it
                Dim currentFont As System.Drawing.Font = Me.GetCurrentFont()
                If currentFont IsNot Nothing Then
                    Me.UpdateCustomLinks(False, True)
                    Try
                        '   set font size
                        Me.RichTextBox.SelectionFont = New Drawing.Font(currentFont.FontFamily, newFontSize, currentFont.Style)
                        Return True
                    Catch ex As Exception
                        '   invalid font size
                        Beep()
                        If areShowingErrors Then
                            MessageBox.Show("Invalid font size!", "Font Size")
                        End If
                    Finally
                        Me.UpdateCustomLinks(True, True)
                    End Try
                End If
            End If
        End If
        '   flag failure
        Return False
    End Function

    ''' <summary> Gets current font. </summary>
    ''' <returns> The current font. </returns>
    Private Function GetCurrentFont() As Font
        Dim currentFont As Font = Me.RichTextBox.SelectionFont
        If currentFont Is Nothing Then
            '   if multiple fonts are in selection, then get font at beginning of selection
            Me.ToggleRedrawMode(False)
            Dim length As Integer = Me.RichTextBox.SelectionLength
            Me.RichTextBox.SelectionLength = 0
            currentFont = Me.RichTextBox.SelectionFont
            Me.RichTextBox.SelectionLength = length
            Me.ToggleRedrawMode(Me._AreRedrawing)
        End If
        '   return current font
        Return currentFont
    End Function

    ''' <summary> Gets current foreground color. </summary>
    ''' <returns> The current color. </returns>
    Private Function GetCurrentColor() As Color
        Dim currentColor As Color = Me.RichTextBox.SelectionColor
        If currentColor = Color.Empty Then
            '   if multiple colors are in selection, then get color at beginning of selection
            Me.ToggleRedrawMode(False)
            Dim length As Integer = Me.RichTextBox.SelectionLength
            Me.RichTextBox.SelectionLength = 0
            currentColor = Me.RichTextBox.SelectionColor
            Me.RichTextBox.SelectionLength = length
            Me.ToggleRedrawMode(Me._AreRedrawing)
        End If
        '   return current Color
        Return currentColor
    End Function

    ''' <summary> Gets current background color. </summary>
    ''' <returns> The current background color. </returns>
    Private Function GetCurrentBackgroundColor() As Color
        Dim currentColor As Color = Me.RichTextBox.SelectionBackColor
        If currentColor = Color.Empty Then
            '   if multiple colors are in selection, then get color at beginning of selection
            Me.ToggleRedrawMode(False)
            Dim length As Integer = Me.RichTextBox.SelectionLength
            Me.RichTextBox.SelectionLength = 0
            currentColor = Me.RichTextBox.SelectionBackColor
            Me.RichTextBox.SelectionLength = length
            Me.ToggleRedrawMode(Me._AreRedrawing)
        End If
        '   return current Color
        Return currentColor
    End Function


    ''' <summary> Resets the focus. 
    '''           Resets focus to text box after action</summary>
    Private Sub ResetFocus()
        If Me.ActiveControl IsNot Me.RichTextBox Then
            Me.RichTextBox.Focus()
        End If
    End Sub

    ''' <summary> Toggle font style. </summary>
    ''' <param name="fontStyle"> The font style. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub ToggleFontStyle(ByVal fontStyle As Drawing.FontStyle)
        Dim currentFont As Font = Me.GetCurrentFont()
        If currentFont IsNot Nothing Then
            '   get current font style
            Dim newFontStyle As System.Drawing.FontStyle
            '   toggle bit for Underline
            newFontStyle = (currentFont.Style Xor fontStyle)
            Me.UpdateCustomLinks(False, True)
            Try
                Me.RichTextBox.SelectionFont =
                New Drawing.Font(currentFont.FontFamily, currentFont.Size, newFontStyle)
            Catch ex As Exception
                Beep()
            End Try
            Me.UpdateCustomLinks(True, True)
        End If
    End Sub

    ''' <summary> Handles the super or sub script. </summary>
    ''' <param name="charOffset"> The character offset. </param>
    ''' <param name="setting">    (Optional) True to setting. </param>
    Private Sub HandleSuperOrSubScript(ByVal charOffset As Integer, Optional ByVal setting As Boolean = False)
        '   set checked status of context-menu options
        Me._SuperscriptContextMenuItem.Checked =
        charOffset > 0
        Me._SubscriptContextMenuItem.Checked =
        charOffset < 0
        If setting Then
            '   set character offset
            Me.UpdateCustomLinks(False, True)
            Me.RichTextBox.SelectionCharOffset = charOffset
            Me.UpdateCustomLinks(True, True)
        End If
    End Sub

#End Region

#Region " CUSTOM LINKS "

    ''' <summary> Copies the custom link information described by CustomLinkInfo. </summary>
    ''' <param name="customLinkInfo"> Information about insertion position, text, and hyperlink. </param>
    ''' <returns> A CustomLinkInfo. </returns>
    Private Function CopyCustomLinkInfo(ByVal customLinkInfo As CustomLinkInfo) As CustomLinkInfo
        Return New CustomLinkInfo With {.Position = customLinkInfo.Position, .Text = customLinkInfo.Text, .Hyperlink = customLinkInfo.Hyperlink}
    End Function

    ''' <summary> Determine if we have custom links in selection. </summary>
    ''' <returns> <c>true</c> if custom links in selection; otherwise <c>false</c> </returns>
    Private Function AreCustomLinksInSelection() As Boolean
        Dim startPos As Integer = Me.RichTextBox.SelectionStart
        Dim endPos As Integer = startPos + Me.RichTextBox.SelectionLength
        Dim here As Boolean = Me.GetCustomLinksAtPositions(startPos, endPos).Length > 0
        Return here
    End Function

    ''' <summary> Gets end of custom link. </summary>
    ''' <param name="customLinkInfo"> Information describing the custom link. </param>
    ''' <returns> The end of custom link. </returns>
    Private Function GetEndOfCustomLink(ByVal customLinkInfo As CustomLinkInfo) As Integer
        Return customLinkInfo.Position + Me.GetLengthOfCustomLink(customLinkInfo)
    End Function

    ''' <summary> Gets length of custom link--including special characters. </summary>
    ''' <param name="customLinkInfo"> Information describing the custom link. </param>
    ''' <returns> The length of custom link. </returns>
    Private Function GetLengthOfCustomLink(ByVal customLinkInfo As CustomLinkInfo) As Integer
        With customLinkInfo
            Return .Text.Length + .Hyperlink.Length + 3
        End With
    End Function

    ''' <summary> Select or deselects custom link. </summary>
    ''' <param name="customLinkInfo"> Information describing the custom link. </param>
    ''' <param name="areSelecting">   True if are selecting. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function SelectOrDeselectCustomLink(ByVal customLinkInfo As CustomLinkInfo, ByVal areSelecting As Boolean) As Boolean
        If areSelecting Then
            Dim length As Integer = Me.GetLengthOfCustomLink(customLinkInfo)
            Me.RichTextBox.Select(customLinkInfo.Position, length)
            Return Me.RichTextBox.SelectionLength = length
        Else
            Me.RichTextBox.Select(Me.GetEndOfCustomLink(customLinkInfo), 0)
            Me.RichTextBox.SelectionProtected = False
            Return True
        End If
    End Function

    ''' <summary> Determine if we are at the hyphen before a custom link. </summary>
    ''' <param name="position"> Starting position of link. </param>
    ''' <returns> <c>true</c> if we before a custom link; otherwise <c>false</c> </returns>
    Private Function AreWeBeforeACustomLink(ByVal position As Integer) As Boolean
        Return Me.IsHyphenHere(position) AndAlso Me._CustomLinks.ContainsKey(position + 1)
    End Function

    ''' <summary> Gets custom link. </summary>
    ''' <param name="position">      position to check for link (current caret position if omitted or
    '''                              negative) </param>
    ''' <param name="includeHyphen"> (Optional) True to include, false to exclude the hyphen. </param>
    ''' <returns> The custom link. </returns>
    ''' <remarks> 
    ''' see if a custom link is at a given position;
    ''' position is +/0 to look forward, - to look backward
    ''' IncludeHyphen is True to include a link's preceding hyphen in check, False to not
    ''' returns CustomLinkInfo instance (Nothing if no link present at location)
    '''  </remarks>
    Private Function GetCustomLink(ByVal position As Integer, Optional ByVal includeHyphen As Boolean = False) As CustomLinkInfo
        If Me._CustomLinks.Count > 0 Then
            Dim link As CustomLinkInfo = Me.GetCustomLinkAtPosition(position)
            If link IsNot Nothing Then
                '   over a link
                Return link
            ElseIf position < 0 Then
                '   link may end at -position
                If includeHyphen AndAlso Me._CustomLinks.ContainsKey(-position) AndAlso Me.IsHyphenHere((-position) - 1) Then
                    Return Me._CustomLinks(-position) 'over hyphen
                End If
            Else
                '   link may begin at position
                If includeHyphen AndAlso Me._CustomLinks.ContainsKey(position + 1) AndAlso Me.IsHyphenHere(position) Then
                    Return Me._CustomLinks(position + 1) 'over hyphen
                End If
            End If
        End If
        '   not over link
        Return Nothing
    End Function

    ''' <summary> Gets custom link at position. 
    ''' Finds last link at or before specified Position</summary>
    ''' <param name="position"> position to check for link (current caret position if omitted or
    '''                         negative) </param>
    ''' <returns> The custom link at position. </returns>
    Private Function GetCustomLinkAtPosition(ByVal position As Integer) As CustomLinkInfo
        If position < 0 Then
            '   looking backward
            Return (From Link As CustomLinkInfo In Me._CustomLinks.Values
                    Where -position > Link.Position AndAlso -position <= Me.GetEndOfCustomLink(Link)).FirstOrDefault()
        Else
            '   looking forward
            Return (From Link As CustomLinkInfo In Me._CustomLinks.Values
                    Where position >= Link.Position AndAlso position < Me.GetEndOfCustomLink(Link)).FirstOrDefault()
        End If
    End Function

    ''' <summary> Gets custom links at positions. Returns array of all links in a given region. </summary>
    ''' <param name="startPosition"> The start position. </param>
    ''' <param name="endPosition"> The end position. </param>
    ''' <returns> The custom links at positions. </returns>
    Private Function GetCustomLinksAtPositions(ByVal startPosition As Integer, ByVal endPosition As Integer) As CustomLinkInfo()
        If startPosition = endPosition Then
            '   get any link at single position
            Return (From CustomLink As CustomLinkInfo In Me._CustomLinks.Values
                    Where CustomLink.Position <= startPosition AndAlso Me.GetEndOfCustomLink(CustomLink) > endPosition).ToArray
        Else
            '   get all links at or after StartPos AND before EndPos
            Return (From CustomLink As CustomLinkInfo In Me._CustomLinks.Values
                    Where Me.GetEndOfCustomLink(CustomLink) >= startPosition AndAlso CustomLink.Position < endPosition).ToArray
        End If
    End Function

    ''' <summary> Updates the custom links. </summary>
    ''' <param name="protectionOnOrOff">  (Optional) True to disable, false to enable the protection
    '''                                   or. </param>
    ''' <param name="doingSelectionOnly"> (Optional) True to doing selection only. </param>
    Private Sub UpdateCustomLinks(Optional ByVal protectionOnOrOff As Boolean = True, Optional ByVal doingSelectionOnly As Boolean = False)
        '   update info on any custom links after text changes
        If doingSelectionOnly AndAlso Not Me.AreCustomLinksInSelection() AndAlso Not Me.RichTextBox.Modified Then
            Exit Sub 'no need to update
        End If
        Dim areUpdatingCustomLinks As Boolean = Me._UpdatingCustomLinks
        Dim areChangingText As Boolean = Me._ChangingText
        Me._UpdatingCustomLinks = True 'prevents event cascade
        Me._TrackingChanges = False 'don't count changes until afterwards
        Dim recentlyChanged As Boolean = Me.RichTextBox.Modified
        '   save selection and scroll position
        Dim startPosition As Integer = Me.RichTextBox.SelectionStart
        Dim selectionLength As Integer = Me.RichTextBox.SelectionLength
        Dim currentScrollPosition As Point = Me.RichTextBox.GetScrollPosition()
        Me.ToggleRedrawMode(False) 'suppress redraw
        '   redo all links
        Me._CustomLinks.Clear()
        Dim isTextLengthened As Boolean = False
        '   get first potential link
        Dim r As Regex = New Regex(_CustomLinkSyntax),
        m As Match = r.Match(Me.RichTextBox.Text, 0)
        Do While m.Success AndAlso m.Index < Me.RichTextBox.TextLength
            '   get information on current potential link
            Dim customLinkInfo As CustomLinkInfo = New CustomLinkInfo With {.Position = m.Index, .Text = m.Groups("Text").Value,
                                                                            .Hyperlink = m.Groups("Hyperlink").Value}
            If Me.SelectOrDeselectCustomLink(customLinkInfo, True) Then
                If SafeNativeMethods.IsLinkEnabled(Me.RichTextBox.Handle) OrElse Regex.IsMatch(Me.RichTextBox.SelectedRtf, "\\v[^0]") Then
                    '   valid potential new link--add it
                    Me.RichTextBox.SelectionProtected = False
                    SafeNativeMethods.EnableLink(Me.RichTextBox.Handle)
                    If Not Me.ExpandSelectionForHyphen() Then
                        '   add preceding hyphen if it's not present
                        Me.RichTextBox.SelectionProtected = False
                        isTextLengthened = True
                        Me._ChangingText = True
                        Me.RichTextBox.SelectionLength = 0
                        Me.RichTextBox.InsertRtf("\v0\-")
                        Me.RichTextBox.SelectionProtected = Me._ProtectingLinks
                        Me._ChangingText = areChangingText
                        customLinkInfo.Position = m.Index + 1
                        '   adjust selection information as needed
                        If startPosition >= m.Index Then
                            startPosition += 1  'start is after insertion
                        ElseIf startPosition + selectionLength >= m.Index Then
                            selectionLength += 1 'end is after insertion
                        End If
                    End If
                    '   protect/unprotect link if allowed
                    If Me._SettingProtection Then
                        Me._ProtectingLinks = protectionOnOrOff
                        Me.RichTextBox.Select(customLinkInfo.Position - 1,
                        Me.GetLengthOfCustomLink(customLinkInfo) + 1)
                        Me.RichTextBox.SelectionProtected = Me._ProtectingLinks
                    End If
                    '   add link to list
                    Me._CustomLinks.Add(customLinkInfo.Position, customLinkInfo)
                    Me.SelectOrDeselectCustomLink(customLinkInfo, False)
                    Me.RichTextBox.SelectionProtected = False 'make sure insert after link is allowed
                End If
            End If
            '   look for next potential link
            m = r.Match(Me.RichTextBox.Text, Me.GetEndOfCustomLink(customLinkInfo))
        Loop
        '   restore selection and scroll position
        If Me.RichTextBox.SelectionStart <> startPosition OrElse Me.RichTextBox.SelectionLength <> selectionLength Then
            Me.RichTextBox.Select(startPosition, selectionLength)
        End If
        Me.RichTextBox.SetScrollPosition(currentScrollPosition)
        If isTextLengthened Then
            Me.RichTextBox.ScrollToCaret()
        End If
        Me.ToggleRedrawMode(Me._AreRedrawing)
        Me._TrackingChanges = True
        Me._UpdatingCustomLinks = areUpdatingCustomLinks
        Me.RichTextBox.Modified = (recentlyChanged OrElse isTextLengthened)
    End Sub

    ''' <summary> Inserts a custom link described by customLinkInfo. </summary>
    ''' <param name="customLinkInfo"> Information describing the custom link. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function InsertCustomLink(ByVal customLinkInfo As CustomLinkInfo) As Boolean
        With customLinkInfo
            '   get RTF for text and hyperlink; get selection info
            Me.ToggleRedrawMode(False) 'suppress redraw
            Dim isSuccessful As Boolean = False
            If customLinkInfo IsNot Nothing AndAlso Not Me._CustomLinks.ContainsKey(.Position) AndAlso Not Me.RichTextBox.SelectionProtected Then
                Dim areSettingProtection As Boolean = Me._SettingProtection
                Me._SettingProtection = False
                Me._ChangingText = True
                Me._UpdatingCustomLinks = True 'prevents event cascades
                '   link not found--insert it
                Dim escText As String = Me.RichTextBox.EscapedRTFText(.Text),
                escHyperlink As String = Me.RichTextBox.EscapedRTFText(.Hyperlink)
                Dim previousLength As Integer = Me.RichTextBox.TextLength
                '   insert RTF for text and hyperlink
                Me._TrackingChanges = False
                .Position += 1
                Me.RichTextBox.InsertRtf("\v0\-{\v \{}" & escText & "{\v |" & escHyperlink & "\}}")
                If Me._IsNotProtected Then
                    '   edit succeeded--mark link, account for text-length changes,
                    '   add reference, and protect link
                    If Me.SelectOrDeselectCustomLink(customLinkInfo, True) Then
                        '   link text selected--complete insertion
                        SafeNativeMethods.EnableLink(Me.RichTextBox.Handle) 'turn on link
                        Me.SelectOrDeselectCustomLink(customLinkInfo, False)
                        isSuccessful = True
                    Else
                        '   selection failed
                        Me.RichTextBox.Undo()
                    End If
                Else
                    '   edit failed
                    Me._IsNotProtected = True
                End If
                Me._UpdatingCustomLinks = False
                Me._ChangingText = False
                Me._TrackingChanges = True
                Me.RichTextBox.Modified = isSuccessful
                Me._SettingProtection = areSettingProtection
                Me.UpdateCustomLinks(True)
                Me.ToggleRedrawMode(Me._AreRedrawing)
            End If
            '   return with success or failure
            Return isSuccessful
        End With
    End Function

    ''' <summary> Remove the custom link described by position. </summary>
    ''' <param name="position"> Starting position of link. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function DeleteCustomLink(ByVal position As Integer) As Boolean
        ' 
        Dim isSuccessful As Boolean = False
        If Me._CustomLinks.ContainsKey(position) Then
            '   link found--remove it, replace text,
            '   and account for text-length changes
            Dim cachedSettingProtection As Boolean = Me._SettingProtection
            Try
                Me._SettingProtection = False
                Me._ChangingText = True
                Me._UpdatingCustomLinks = True 'prevents event cascades
                Me.ToggleRedrawMode(False) 'suppress redraw
                Dim customLinkInfo As CustomLinkInfo = Me._CustomLinks(position)
                With customLinkInfo
                    Dim previousLength As Integer = Me.RichTextBox.TextLength
                    If Me.SelectOrDeselectCustomLink(customLinkInfo, True) Then
                        '   link selected--proceed with deletion
                        Me.ExpandSelectionForHyphen()
                        Me._TrackingChanges = False
                        Me.RichTextBox.SelectionProtected = False ' unprotect link
                        SafeNativeMethods.DisableLink(Me.RichTextBox.Handle) 'turn off link
                        Dim residualText As String
                        If Me.KeepHypertextOnRemove Then
                            '   keep whole information
                            residualText = "{" & .Text & "|" & .Hyperlink & "}"
                        Else
                            '   just visible text
                            residualText = .Text
                        End If
                        Me.RichTextBox.InsertRtf("{\v0" & Me.RichTextBox.EscapedRTFText(residualText) & "}")
                        isSuccessful = True
                    Else
                        ' selection failed
                        Me.SelectOrDeselectCustomLink(customLinkInfo, False)
                    End If
                End With
                Me._UpdatingCustomLinks = False
                Me._ChangingText = True
                Me._TrackingChanges = True
                Me.RichTextBox.Modified = isSuccessful
            Catch
                Throw
            Finally
                Me._SettingProtection = cachedSettingProtection
                Me.UpdateCustomLinks(True)
                Me.ToggleRedrawMode(Me._AreRedrawing)
            End Try
        End If
        '   return with success or failure
        Return isSuccessful
    End Function

    ''' <summary> Gets valid custom link information. 
    ''' Makes text into a valid link string if possible; returns CustomLinkInfo 
    ''' instance (Nothing if invalid text)</summary>
    ''' <param name="position"> position to check for link (current caret position if omitted or
    '''                         negative) </param>
    ''' <param name="text">     New plain Text (ChangesMade event is fired if a change in existing
    '''                         text or formatting occurs) </param>
    ''' <returns> The valid custom link information. </returns>
    Private Function GetValidCustomLinkInfo(ByVal position As Integer, ByVal text As String) As CustomLinkInfo

        Const CustomLinkExactSyntax As String = "^" & _CustomLinkSyntax & "\z"
        '   see if it's already valid
        Dim m As Match = Regex.Match(text, CustomLinkExactSyntax)
        If Not m.Success Then
            '   string not already valid--let's try to make it so
            text = text.Trim()
            If text.StartsWith("{") AndAlso text.EndsWith("}") Then
                '   strip off start and end braces
                text = text.Substring(1, text.Length - 2)
            End If
            If Not text.Contains("|") Then
                '   default hyperlink to main text
                text = text & "|" & text
            End If
            '   rebuild and try again
            text = "{" & text & "}"
            m = Regex.Match(text, CustomLinkExactSyntax)
        End If
        If m.Success Then
            '   if syntax matches, then get information
            Return New CustomLinkInfo With {.Position = position,
                                                       .Text = m.Groups("Text").Value, .Hyperlink = m.Groups("Hyperlink").Value}
        Else
            '   invalid text
            Return Nothing
        End If
    End Function

    ''' <summary> Removes the custom links in selection described by DoingWholeDocument. </summary>
    ''' <param name="doingWholeDocument"> (Optional) True to doing whole document. </param>
    Private Sub RemoveCustomLinksInSelection(Optional ByVal doingWholeDocument As Boolean = False)
        '   remove all custom links from selected text
        If Me._CustomLinks.Count = 0 Then
            Exit Sub
        End If
        Me.GetRange()
        Me.SuppressRedraw()
        Dim currentScrollPosition As Point = Me.RichTextBox.GetScrollPosition()
        If doingWholeDocument Then
            '   whole document
            Me._DoingAll = True
            Me._StartPosition = 0
            Me._EndPosition = Me.RichTextBox.TextLength
        End If
        '   remove links in range
        Dim currentLength As Integer = Me.RichTextBox.TextLength
        '   go through region
        Dim links() As CustomLinkInfo = Me.GetCustomLinksAtPositions(Me._StartPosition, Me._EndPosition)
        Dim areChanging As Boolean =
        links.Length > 0
        For LinkIndex As Integer = links.Length - 1 To 0 Step -1
            Me.DeleteCustomLink(links(LinkIndex).Position)
            '   check for change in text length
            If Me.RichTextBox.TextLength <> currentLength Then
                Me._EndPosition += (Me.RichTextBox.TextLength - currentLength)
                currentLength = Me.RichTextBox.TextLength
            End If
        Next LinkIndex
        '   restore selection and scroll position
        Me.RestoreRange()
        Me.RichTextBox.SetScrollPosition(currentScrollPosition)
        If Me._KeepHypertextOnRemove Then
            Me.RichTextBox.ScrollToCaret()
        End If
        Me.ResumeRedraw()
        Me.RichTextBox.Modified = areChanging
        Me.UpdateCustomLinks(True, True)
    End Sub

    ''' <summary> Normalize selection for custom links. </summary>
    Private Sub NormalizeSelectionForCustomLinks()
        '   make sure selection bounds don't straddle custom links
        Dim areUpdatingCustomLinks As Boolean = Me._UpdatingCustomLinks
        Me._UpdatingCustomLinks = True 'prevents event cascade
        Dim startPos As Integer = Me.RichTextBox.SelectionStart
        Dim endPos As Integer = startPos + Me.RichTextBox.SelectionLength
        Dim customLinkInfo As CustomLinkInfo
        If startPos = endPos Then
            '   no selection--are we over invisible text?
            customLinkInfo = Me.GetCustomLink(-startPos)
            If customLinkInfo IsNot Nothing AndAlso startPos > customLinkInfo.Position + customLinkInfo.Text.Length Then
                '   skip over it
                startPos = Me.GetEndOfCustomLink(customLinkInfo)
                endPos = startPos
            End If
        Else
            '   check to see if bounds are on links
            '   does the selection start on a link?
            customLinkInfo = Me.GetCustomLink(startPos) 'look forward
            If customLinkInfo IsNot Nothing AndAlso customLinkInfo.Position <= startPos Then
                '   yes--move selection start to beginning of link
                startPos = customLinkInfo.Position
                If Me.IsHyphenHere(startPos - 1) Then
                    startPos -= 1 'include preceding hyphen
                End If
            End If
            '   does the selection end on a link?
            customLinkInfo = Me.GetCustomLink(-endPos) 'look backward
            If customLinkInfo IsNot Nothing AndAlso Me.GetEndOfCustomLink(customLinkInfo) > endPos Then
                '   yes--move selection end to end of link
                endPos = Me.GetEndOfCustomLink(customLinkInfo)
            ElseIf Me.AreWeBeforeACustomLink(endPos - 1) Then
                '   we're ending on the hyphen preceding a link--include link
                endPos = Me.GetEndOfCustomLink(Me._CustomLinks(endPos))
            End If
        End If
        '   update selection
        If startPos <> Me.RichTextBox.SelectionStart OrElse endPos <> Me.RichTextBox.SelectionStart + Me.RichTextBox.SelectionLength Then
            Me.RichTextBox.Select(startPos, endPos - startPos)
            If Not Me._ChangingText Then
                Me.UpdateCustomLinks(Me.RichTextBox.SelectionLength = 0, True)
            End If
        End If
        Me._UpdatingCustomLinks = areUpdatingCustomLinks
    End Sub

#Region " HYPHENS "

    ''' <summary> Query if 'position' is hyphen here. </summary>
    ''' <param name="position"> Starting position of link. </param>
    ''' <returns> <c>true</c> if hyphen here; otherwise <c>false</c> </returns>
    Private Function IsHyphenHere(ByVal position As Integer) As Boolean
        Return position >= 0 AndAlso position < Me.RichTextBox.TextLength AndAlso Me.RichTextBox.Text.Substring(position, 1) = RichTextFormatControl._HyphenChar
    End Function

    ''' <summary> Determines if we can expand selection for hyphen. 
    ''' Expands selection to cover any immediately preceding hyphen; return whether one was found</summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function ExpandSelectionForHyphen() As Boolean
        Dim position As Integer = Me.RichTextBox.SelectionStart
        Dim length As Integer = Me.RichTextBox.SelectionLength
        If Me.IsHyphenHere(position - 1) Then
            '   hyphen precedes selection--incorporate it
            Me.RichTextBox.Select(position - 1, length + 1)
            Return True
        Else
            '   no preceding hyphen
            Return False
        End If
    End Function

#End Region

#End Region

#Region " PROTECTED METHODS: NATIVE (CUSTOM) EVENT-HANDLING METHODS  "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.DragDrop" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.DragEventArgs" /> that contains
    '''                         the event data. </param>
    ''' <remarks>
    ''' Overrides for class-level events (NOTE: Any custom links are always protected during DragDrop)
    ''' </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Protected Overrides Sub OnDragDrop(ByVal e As DragEventArgs)
        '   Handle dropping onto control
        '   get current protection status
        Dim cachedProtectingLinks As Boolean = Me._ProtectingLinks
        Try
            '   raise event with links protected
            Me.UpdateCustomLinks(True)
            Me._SettingProtection = False
            MyBase.OnDragDrop(e) 'do drop
        Finally
            '   restore previous protection status
            Me._SettingProtection = True
            Me.UpdateCustomLinks(cachedProtectingLinks)
        End Try
    End Sub

    ''' <summary> Raises the insert RTF text event. </summary>
    ''' <remarks>
    ''' <list type="number">Protection status for any custom links during events<item>
    ''' ChangesMade, TextProtected, and HyperlinkClicked -- always protected      </item><item>
    ''' InsertRtfText and SmartRtfText -- protected when text isn't selected or selected text contains custom links; otherwise unprotected</item><item>
    ''' EdtingWithLinksUnprotected -- always unprotected) </item></list>
    '''          </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnInsertRtfText(ByVal e As InsertRtfTextEventArgs)
        '   Give programmer a chance to define custom keystrokes
        RaiseEvent InsertRtfText(Me, e)
    End Sub

    ''' <summary> Raises the smart RTF text event.
    '''           Give programmer a chance to modify key characters  </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnSmartRtfText(ByVal e As SmartRtfTextEventArgs)
        RaiseEvent SmartRtfText(Me, e)
    End Sub

    ''' <summary> Raises the changes made event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Protected Overridable Sub OnChangesMade(ByVal e As EventArgs)
        '   Flag that change has been made to text
        '   get current protection status
        Dim cachedProtectingLinks As Boolean = Me._ProtectingLinks
        Try
            '   raise event with links protected
            Me.UpdateCustomLinks(True)
            Me._SettingProtection = False
            RaiseEvent ChangesMade(Me, e)
        Finally
            '   restore previous protection status
            Me._SettingProtection = True
            Me.UpdateCustomLinks(cachedProtectingLinks)
        End Try
    End Sub

    ''' <summary> Raises the cancel event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnTextProtected(ByVal e As CancelEventArgs)
        '   Flag that an attempt was made to modify protected text
        '   get current protection status
        Dim cachedProtectingLinks As Boolean = Me._ProtectingLinks

        Try
            '   raise event with links protected
            Me.UpdateCustomLinks(True)
            Me._SettingProtection = False
            RaiseEvent TextProtected(Me, e)
        Finally
            '   restore previous protection status
            Me._SettingProtection = True
            Me.UpdateCustomLinks(cachedProtectingLinks)
        End Try
    End Sub

    ''' <summary> Raises the custom link information event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnHyperlinkClicked(ByVal e As CustomLinkInfoEventArgs)
        '   Flag that a link was clicked
        '   get current protection status
        Dim cachedProtectingLinks As Boolean = Me._ProtectingLinks
        Try
            '   raise event with links protected
            Me.UpdateCustomLinks(True)
            Me._SettingProtection = False
            RaiseEvent HyperlinkClicked(Me, e)
        Finally
            '   restore previous protection status
            Me._SettingProtection = True
            Me.UpdateCustomLinks(cachedProtectingLinks)
        End Try
    End Sub

    ''' <summary> Raises the parameter event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnEditingWithLinksUnprotected(ByVal e As ParameterEventArgs)
        '   Give programmer a chance to make changes with custom links unprotected
        '   get current protection status
        Dim cachedProtectingLinks As Boolean = Me._ProtectingLinks
        Try
            '   raise event with links unprotected
            Me.UpdateCustomLinks(False)
            Me._SettingProtection = False
            RaiseEvent EditingWithLinksUnprotected(Me, e)
        Finally
            '   restore previous protection status
            Me._SettingProtection = True
            Me.UpdateCustomLinks(cachedProtectingLinks)
        End Try
    End Sub

#End Region

#Region " PUBLIC COMPONENTS SPECIFIC TO THIS USER CONTROL: EVENTS "

    ''' <summary>
    ''' InsertRtfText event --
    '''    allows user to specify special text (in RTF format)--for specific key sequences--
    '''    to insert at caret in place of any selected text
    ''' </summary>
    ''' <param name="sender">control instance</param>
    ''' <param name="e">InsertRtfTextEventArgs instance
    '''    (INPUT: e.KeyEventArgs = keyboard information
    '''        from underlying (internal) KeyDown event;
    '''     OUTPUT: e.RtfText = RTF String to insert (null for no change)</param>
    ''' <remarks>e.RtfText is expected to be a String in RICH-TEXT-FORMAT,
    ''' not "plain text"!</remarks>
    Public Event InsertRtfText(ByVal sender As Object, ByVal e As InsertRtfTextEventArgs)

    ''' <summary>
    ''' SmartRtfText event --
    '''    allows user to specify text (in RTF format) to replace the incoming character
    '''    being typed in (and optionally preceding characters) at caret inplace
    '''    of any selected type 
    ''' </summary>
    ''' <param name="sender">control instance</param>
    ''' <param name="e">SmartRtfTextEventArgs instance
    '''    (INPUT: e.KeyPressEventArgs = incoming keyboard character from underlying (internal)
    '''        KeyPress event;
    '''     OUTPUT: e.RtfText = RTF String to replace text with (null for no change),
    '''        e.PrecedingCharacterCount = number of preceding characters
    '''           to remove before inserting e.RtfText</param>
    Public Event SmartRtfText(ByVal sender As Object, ByVal e As SmartRtfTextEventArgs)

    ''' <summary>
    ''' ChangesMade event --
    '''    tracks when a change has been made by the user or programatically
    '''    (unlike underlying text box's TextChanged event, ignores "temporary" edits
    '''     [that reverse themselves] during a bigger edit operation of this control)
    ''' </summary>
    ''' <param name="sender">control instance</param>
    ''' <param name="e">EventArgs instance (no parameters)</param>
    ''' <remarks>If this event fires, then the IsTextChanged property is True</remarks>
    Public Event ChangesMade(ByVal sender As Object, ByVal e As EventArgs)

    ''' <summary>
    ''' TextProtected event --
    '''    tracks when an attempt is made to edit wholly or partially protected text
    '''    (unlike underlying text box's Protected events, handles attempts to remove
    '''     local links, and otherwise gives programmer ability to supress usual error
    '''     warning after handling event)
    ''' </summary>
    ''' <param name="sender">control instance</param>
    ''' <param name="e">CancelEventArgs instance
    '''    (OUTPUT: e.Cancel = True to pre-empt default warning after event,
    '''        False (default) to not)</param>
    Public Event TextProtected(ByVal sender As Object, ByVal e As CancelEventArgs)

    ''' <summary>
    ''' HyperlinkClicked event --
    '''    tracks when user clicks on a link or custom link; designed to handle custom links
    '''    (unlike underlying text box's LinkClicked event, handles text/hyperlink of custom
    '''     links, and distinguishes between multiple adjacent custom links)
    ''' </summary>
    ''' <param name="sender">control instance</param>
    ''' <param name="e">CustomLinkInfoEventArgs instance
    '''     (OUTPUT: e.CustomLinkInfo instance, with
    '''         e.CustomLinkInfo.Text = (visible) text,
    '''         e.CustomLinkInfo.Hyperlink = (invisible) hyperlink)</param>
    ''' <remarks>If the link is a standard (non-custom) RTB link, then
    ''' e.CustomLinkInfo.Hyperlink = e.CustomLinkInfo.Text</remarks>
    Public Event HyperlinkClicked(ByVal sender As Object, ByVal e As CustomLinkInfoEventArgs)

    ''' <summary>
    ''' EditingWithLinksUnprotected event --
    '''    allows programmer to change regions of text containing custom links
    '''    (i.e., formatting changes); temporarily unprotects links and disables
    '''    auto-protection of links, then re-protects links and re-enables auto-protection
    '''    upon completion
    ''' </summary>
    ''' <param name="sender">RichTextBox instances</param>
    ''' <param name="e">ParameterEventArgs instance
    '''     (INPUT/OUTPUT: e.Parameters = information to pass to and receive from
    '''          this event)</param>
    Public Event EditingWithLinksUnprotected(ByVal sender As Object, ByVal e As ParameterEventArgs)

#End Region

#Region " PROPERTIES "

    Private _ProtectingLinks As Boolean = True
    ''' <summary>
    ''' Get status of custom-link protection
    ''' </summary>
    ''' <returns>True if custom links are enabled and
    ''' currently protected, otherwise False</returns>
    ''' <remarks>NOTES:
    ''' 1. This READ-ONLY property can be used to determine if arbitrary edits affecting links
    '''    can be done in during an event where the protection-status is not guaranteed.
    '''    If this property returns True, then use the EditWithLinksUnprotected method and the
    '''    EditingWithLinksUnprotected event to make arbitrary edits that might affect links
    ''' 2. This property ALWAYS returns False when the control is not in custom-link mode
    '''    (that is, if DoCustomLinks is False)</remarks>
    <Browsable(False)>
    Public ReadOnly Property AreLinksProtected() As Boolean
        Get
            Return Me._DoCustomLinks AndAlso Me._ProtectingLinks
        End Get
    End Property

    Private _AutoDragDropInProgress As Boolean = False
    ''' <summary>
    ''' Get whether an automatic drag-and-drop is in progress
    ''' </summary>
    ''' <returns>True if internal text box is receiving a DragDrop event
    ''' (only possible if drop is auto-drop or if underlying text box's
    ''' AllowDrop property is set to True), otherwise False</returns>
    ''' <remarks>This property can be used by the outer class's DragDrop
    ''' event procedure to determine if the rich-text box should handle the
    ''' drop internally rather than imposing custom logic. To prevent the underlying
    ''' text box's default drop logic from occurring, the outer class's DragDrop
    ''' procedure should set e.Effects to DragDropEffects.None. (If custom logic
    ''' is not bypassed AND e.Effects is NOT set to DragDropEffects.None, then BOTH
    ''' the custom and default drop logic occur!)</remarks>
    <Browsable(False)>
    Public ReadOnly Property AutoDragDropInProgress() As Boolean
        Get
            Return Me._AutoDragDropInProgress
        End Get
    End Property

    ''' <summary> Gets the has text. </summary>
    ''' <value> The has text. </value>
    Public ReadOnly Property HasText As Boolean
        Get
            Return Me.RichTextBox.TextLength > 0
        End Get
    End Property

    ''' <summary> Gets or sets the is modified. </summary>
    ''' <value> The is modified. </value>
    Private Property IsModifiedThis As Boolean = False ' have recent (tracked) changes been made?

    ''' <summary>
    ''' Gets or sets whether the rich text box was modified
    ''' </summary>
    ''' <value>True to flag that the control was modified (ChangesMade event is fired),
    ''' False (default) to flag it as unmodified</value>
    ''' <returns>True if modified (or flagged as modified), False if not</returns>
    <Browsable(False)>
    Public Property IsModified() As Boolean
        Get
            Return Me.IsModifiedThis
        End Get
        Set(value As Boolean)
            If value <> Me.IsModified OrElse value <> Me.RichTextBox.Modified Then
                ' set modification status and raise ChangesMade event if necessary
                Me.IsModifiedThis = value
                Me.UpdateToolbar()
                Me.RichTextBox.Modified = value
                Me._SaveButton.Visible = Me.IsModified AndAlso Me.HasText
            End If
        End Set
    End Property

    ''' <summary> Handles the tracked change. Document whether change is made and raise ChangesMade 
    '''           event if True unless tacking changes is turned off. </summary>
    Private Sub HandleTrackedChange()
        '   has text been modified?
        If Me._TrackingChanges AndAlso Me.RichTextBox.Modified Then
            '   a permanent (tracked) change has been made
            Me.IsModifiedThis = True
            Me.OnChangesMade(New EventArgs())
        End If
        '   don't fire ChangesMade again until a new change is actually made
        Me.RichTextBox.Modified = False
    End Sub

    ''' <summary> Gets or sets information describing the search. </summary>
    ''' <value> Information describing the search. </value>
    <Browsable(False)>
    Private ReadOnly Property SearchInfo As SearchCriteria 'what kind of search/replace are we to do?

    ''' <summary>
    ''' Gets or sets whether to Find/Replace is to skip text overlapping custom links
    ''' when replacing
    ''' </summary>
    ''' <value>True (default) for avoiding text containing links, False to allow text
    ''' containing links to be replaced</value>
    ''' <returns>True for avoid links, False to allow replacement</returns>
    ''' <remarks>NOTES:
    ''' 1. When an occurrence of search text containing custom links is replaced, any
    '''    contained links are replaced in their entirety with the replacement text
    '''    (along with any adjacent non-link text), even link text just outside
    '''    the search text
    ''' 2. This value can only be changed when custom links are allowed--that is,
    '''    when the DoCustomLinks property is set to True</remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Allow or disallow avoiding custom links when replacing text")>
    Public Property SkipLinksOnReplace() As Boolean
        Get
            Return Me._SearchInfo.AvoidLinks
        End Get
        Set(value As Boolean)
            If Me._DoCustomLinks Then
                Me._SearchInfo.AvoidLinks = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets whether to allow user to specify color with font
    ''' when invoking the Font Dialog
    ''' </summary>
    ''' <value>True (default) for setting color and font together, False for just font</value>
    ''' <returns>True for setting color and font together, False for just font</returns>
    ''' <remarks>Setting this property to True still allows one to invoke
    ''' the Color Dialog to set only color</remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Allow or disallow color to be specified in Font Dialog")>
    Public Property SetColorWithFont() As Boolean = True

    Private _ShowToolStrip As Boolean = True
    ''' <summary>
    ''' Gets or sets whether to show ToolStrip above RichTextBox
    ''' </summary>
    ''' <value>True (default) to show ToolStrip, False to hide it</value>
    ''' <returns>True if showing ToolStrip, False if not</returns>
    ''' <remarks>All the shortcut keys and special-character options
    ''' still work if ToolStrip is hidden</remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Show Or hide tool bar")>
    Public Property ShowToolStrip() As Boolean
        Get
            Return Me._ShowToolStrip
        End Get
        Set(value As Boolean)
            '   invoke VisibleChanged event for ToolStrip
            Me._ShowToolStrip = value
            Me._TopToolStrip.Visible = value
        End Set
    End Property

    Private _ShowRuler As Boolean = True
    ''' <summary>
    ''' Gets or sets whether to show TextRuler above RichTextBox
    ''' </summary>
    ''' <value>True (default) to show TextRuler, False to hide it</value>
    ''' <returns>True if showing TextRuler, False if not</returns>
    <Browsable(True), Category("Behavior")>
    <Description("Show Or hide ruler bar")>
    Public Property ShowRuler() As Boolean
        Get
            Return Me._ShowRuler
        End Get
        Set(value As Boolean)
            '   invoke VisibleChanged event for TextRuler
            Me._ShowRuler = value
            Me._TopRuler.Visible = value
        End Set
    End Property

    Private _DoCustomLinks As Boolean = False
    ''' <summary>
    ''' Gets or sets whether to allow arbitrary text to be used as links
    ''' </summary>
    ''' <value>True to allow custom links, False (default) to disallow</value>
    ''' <returns>True if allowing custom links, False if not</returns>
    ''' <remarks>
    ''' 1. Setting this property to True sets the DetectURLs property of
    '''    the underlying text box to False, and DetectURLs should remain false
    '''    while this property is True, in order to preserve link formatting
    '''    when adjacent text is modified
    ''' 2. While this property is True, the LinkClicked event of the underlying
    '''    text box returns both the main text and the hypertext in the form {text|hyperlink},
    '''    and the HyperlinkClicked event in THIS control returns
    '''    the text and hyperlink of the link in a CustomLinkInfo instance
    '''    within CustomLinkInfoEventArgs</remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable making links with arbitrary custom text")>
    Public Property DoCustomLinks() As Boolean
        Get
            Return Me._DoCustomLinks
        End Get
        Set(value As Boolean)
            '   turn on or off custom hyper-linking
            If value = Me._DoCustomLinks Then
                Exit Property 'value not changed
            End If
            '   changing value
            Me._DoCustomLinks = value
            Me._CurrentCustomLink = Nothing
            Me._HyperlinksContextMenuItem.Enabled = value
            Me._HyperlinksContextMenuItem.Visible = value
            Me._InsertEditRemoveHyperlinkContextMenuItem.Enabled = value
            Me._InsertEditRemoveHyperlinkContextMenuItem.Visible = value
            Me._RemoveHyperLinksContextMenuItem.Enabled = value
            Me._RemoveHyperLinksContextMenuItem.Visible = value
            Me._KeepHypertextContextSeparator.Visible = value
            Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Enabled = value
            Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Visible = value
            Me._LinksToolStripSeparator.Visible = value
            Me._HyperlinkButton.Visible = value
            Me._HyperlinkButton.Enabled = value
            Me._SettingLinkMode = True 'prevent event cascade
            If value Then
                '   turn on custom-link mode and find any custom-link-qualifying protected text
                Me.RichTextBox.DetectUrls = False
                Me.UpdateCustomLinks()
            Else
                '   remove custom links and turn off custom-link mode
                Me.RemoveCustomLinksInSelection(True)
                Me.RichTextBox.DetectUrls = True
                Me.KeepHypertextOnRemove = False
            End If
            Me._SettingLinkMode = False
            Me.HandleSelectionChanged(Me.RichTextBox, EventArgs.Empty)
        End Set
    End Property

    Private _KeepHypertextOnRemove As Boolean = False
    ''' <summary>
    ''' Gets or sets whether to retain hyperlink text in document when
    ''' removing custom links, or just return main text
    ''' </summary>
    ''' <value>True to allow keep hyper-links, False (default) to remove</value>
    ''' <returns>True if keeping links, False if not</returns>
    ''' <remarks>If the DoCustomLinks property is False, then 
    ''' this property is always False and attempts to set it to True are ignored</remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Get Or set whether to preserve hypertext when removing custom link")>
    Public Property KeepHypertextOnRemove() As Boolean
        Get
            Return Me._KeepHypertextOnRemove
        End Get
        Set(value As Boolean)
            If Me._DoCustomLinks Then
                Me._KeepHypertextOnRemove = value
                Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Checked = value
            End If
        End Set
    End Property

    Private _AllowTabs As Boolean = True
    ''' <summary>
    ''' Gets or sets whether tabs are supported
    ''' </summary>
    ''' <value>True (default) to allow tabs, False to disallow then</value>
    ''' <returns>True if tab marking is enabled, else False</returns>
    ''' <remarks>ShowRuler property must be True, as tabs must be set using ruler</remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable support for Tabs")>
    Public Property AllowTabs() As Boolean
        Get
            Return Me._AllowTabs
        End Get
        Set(value As Boolean)
            '   show/hide spell-check ToolStrip and MenuStrip items
            Me._AllowTabs = value
            Me._TopRuler.TabsEnabled = value
        End Set
    End Property

    Private _AllowSpellCheck As Boolean = True
    ''' <summary>
    ''' Gets or sets whether spell checking is available
    ''' </summary>
    ''' <value>True (default) to enable spell checking, False to disable it</value>
    ''' <returns>True if spell checking is enabled, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable spell-checking")>
    Public Property AllowSpellCheck() As Boolean
        Get
            Return Me._AllowSpellCheck
        End Get
        Set(value As Boolean)
            '   show/hide spell-check ToolStrip and MenuStrip items
            Me._AllowSpellCheck = value
            Me._SpellCheckButton.Visible = Me._AllowSpellCheck
            Me._SpellCheckContextMenuItem.Visible = Me._AllowSpellCheck
            Me._SpellCheckContextMenuItem.Enabled = Me._AllowSpellCheck
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets whether default custom-text insertions are allowed
    ''' </summary>
    ''' <value>True (default) to enable default custom text, False to disable it</value>
    ''' <returns>True if enabling custom-insert text, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable default custom text")>
    Public Property AllowDefaultInsertText() As Boolean = True
    ''' <summary>
    ''' Gets or sets whether default smart-text replacements are allowed
    ''' </summary>
    ''' <value>True (default) to enable default smart text, False to disable it</value>
    ''' <returns>True if enabling smart-text replacement, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable default smart text")>
    Public Property AllowDefaultSmartText() As Boolean = True

    Private _AllowLists As Boolean = True
    ''' <summary>
    ''' Gets or sets whether lists are available
    ''' </summary>
    ''' <value>True (default) to enable lists, False to disable it</value>
    ''' <returns>True if lists are enabled, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable lists")>
    Public Property AllowLists() As Boolean
        Get
            Return Me._AllowLists
        End Get
        Set(value As Boolean)
            '   show/hide spell-check ToolStrip and MenuStrip items
            Me._AllowLists = value
            Me._ListDropDownButton.Visible = Me._AllowLists
            Me._ListDropDownButton.Enabled = Me._AllowLists
            Me._ListContextMenuItem.Visible = Me._AllowLists
            Me._ListContextMenuItem.Enabled = Me._AllowLists
        End Set
    End Property

    Private _AllowPictures As Boolean = True
    ''' <summary>
    ''' Gets or sets whether picture-inserting is available
    ''' </summary>
    ''' <value>True (default) to enable picture insertion, False to disable it</value>
    ''' <returns>True if picture insertion is enabled, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable inserting of pictures")>
    Public Property AllowPictures() As Boolean
        Get
            Return Me._AllowPictures
        End Get
        Set(value As Boolean)
            '   show/hide spell-check ToolStrip and MenuStrip items
            Me._AllowPictures = value
            Me._InsertPictureButton.Visible = Me._AllowPictures
            Me._InsertContextMenuItem.Visible = Me._AllowPictures
            Me._InsertContextMenuItem.Enabled = Me._AllowPictures
        End Set
    End Property

    Private _AllowSymbols As Boolean = True
    ''' <summary>
    ''' Gets or sets whether symbol-inserting is available
    ''' </summary>
    ''' <value>True (default) to enable symbol insertion, False to disable it</value>
    ''' <returns>True if symbol insertion is enabled, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable inserting of symbols")>
    Public Property AllowSymbols() As Boolean
        Get
            Return Me._AllowSymbols
        End Get
        Set(value As Boolean)
            '   show/hide spell-check ToolStrip and MenuStrip items
            Me._AllowSymbols = value
            Me._InsertSymbolButton.Visible = Me._AllowSymbols
            Me._InsertSymbolContextMenuItem.Visible = Me._AllowSymbols
            Me._InsertSymbolContextMenuItem.Enabled = Me._AllowSymbols
        End Set
    End Property

    Private _AllowHyphenation As Boolean = True
    ''' <summary>
    ''' Gets or sets whether hyphenation-searching is available
    ''' </summary>
    ''' <value>True (default) to enable hyphenation-searching, False to disable it</value>
    ''' <returns>True if hyphenation-searching is enabled, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable searching for words to hyphenate")>
    Public Property AllowHyphenation() As Boolean
        Get
            Return Me._AllowHyphenation
        End Get
        Set(value As Boolean)
            '   show/hide spell-check ToolStrip and MenuStrip items
            Me._AllowHyphenation = value
            Me._HyphenateButton.Visible = Me._AllowHyphenation
            Me._HyphenationContextMenuItem.Visible = Me._AllowHyphenation
            Me._HyphenationContextMenuItem.Enabled = Me._AllowHyphenation
            Me._RemoveHyphensContextMenuItem.Enabled = Me._AllowHyphenation
            Me._RemoveHiddenHyphensContextMenuItem.Enabled = Me._AllowHyphenation
        End Set
    End Property
    ''' <summary>
    ''' Gets or sets whether selection of selected text is
    ''' restored after Find, Replace, Hyphenation, Spell-Check, or
    ''' removal of all custom links dialogs in a region
    ''' </summary>
    ''' <value>True (default) to enable re-selection, False to disable it</value>
    ''' <returns>True if re-selection is enabled, else False</returns>
    ''' <remarks></remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Enable Or disable restoring text-selection after dialogs")>
    Public Property MaintainSelection() As Boolean = False 'restoring selection after edit?

    ''' <summary>
    ''' Gets or sets units used for ruler
    ''' </summary>
    ''' <value>TextRuler.UnitType.Inches or TextRuler.UnitType.Centimeters</value>
    ''' <returns>TextRuler.UnitType.Inches or TextRuler.UnitType.Centimeters</returns>
    <Browsable(True), Category("Appearance")>
    <Description("Get Or set ruler unit type")>
    Public Property UnitsForRuler() As RulerUnitType
        Get
            Return Me._TopRuler.Units
        End Get
        Set(value As RulerUnitType)
            Me._TopRuler.Units = value
            Me.GetHorizontalInfo()
        End Set
    End Property

    ''' <summary>
    ''' Gets or set maximum width of all lines of text in pixels
    ''' </summary>
    ''' <value>Text width in pixels</value>
    ''' <returns>Text width in pixels</returns>
    ''' <remarks>This is the same as the underlying rtb.RightMargin value,
    ''' except that setting it IMMEDIATELY affects the rich-text box and ruler</remarks>
    <Browsable(True), Category("Behavior")>
    <Description("Get Or set the width of the printable area in pixels")>
    Public Property RightMargin() As Integer
        Get
            Return Me.RichTextBox.RightMargin
        End Get
        Set(value As Integer)
            Me.RichTextBox.RightMargin = value
            Me.GetHorizontalInfo()
        End Set
    End Property

    ''' <summary> Gets or sets the rich text format header. </summary>
    ''' <value> The rich text format header. </value>
    <Browsable(False)>
    Public Property RichTextFormatHeader As String

    ''' <summary>
    ''' Gets or sets plain text in text box
    ''' </summary>
    ''' <value>New plain Text (ChangesMade event is fired if a change
    ''' in existing text or formatting occurs)</value>
    ''' <returns>Existing plain Text</returns>
    ''' <remarks>This is the same as the underlying rtb.Text value,
    ''' except that setting always marks the text as modified</remarks>
    <Browsable(False), Category("Data")>
    <Description("Get Or set plain text of rich-text box")>
    Public Overrides Property Text() As String
        Get
            Return Me.RichTextBox.Text
        End Get
        Set(value As String)
            Me.RichTextBox.Text = value
            Me.RichTextBox.Modified = True
            Me.UpdateToolbar()
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets RTF text in text box
    ''' </summary>
    ''' <value>New RTF Text (ChangesMade event is fired if different from existing)</value>
    ''' <returns>Existing RTF Text</returns>
    ''' <remarks>This is the same as the underlying rtb.Rtf value</remarks>
    <Browsable(True), Category("Data")>
    <Description("Get Or set RTF text of rich-text box")>
    Public Property Rtf() As String
        Get
            Return Me.RichTextBox.Rtf
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.RichTextBox.Rtf) Then
                Me.RichTextBox.Rtf = value
                Me.UpdateToolbar()
            End If
        End Set
    End Property
    '''<summary>
    ''' Gets or sets directory for any text and pictures saved for loaded
    ''' </summary>
    ''' <value>New file path</value>
    ''' <returns>Existing file path</returns>
    <Browsable(True), Category("Data")>
    <Description("Get Or set path for files")>
    Public Property FilePath() As String = String.Empty

#End Region

#Region " METHODS: FILE "

    Private _FileName As String
    ''' <summary> Gets or sets the filename of the file. </summary>
    ''' <value> The name of the file. </value>
    <Browsable(False)>
    Public Property FileName As String
        Get
            Return Me._FileName
        End Get
        Protected Set(value As String)
            If Not String.Equals(value, Me.FileName) Then
                Me._FileName = value
            End If
            If Not String.IsNullOrWhiteSpace(value) Then
                Dim fileInfo As New System.IO.FileInfo(value)
                Me.FilePath = fileInfo.DirectoryName
            End If
        End Set
    End Property

    ''' <summary>
    ''' Save contents of the rich-text box
    ''' </summary>
    ''' <param name="fileName">Name of RTF/plain-text file
    ''' (if null or omitted, then a file dialog is invoked)</param>
    ''' <param name="streamType">Format of file (user-specified if -1 or omitted)</param>
    ''' <returns>False if dialog was canceled, True if text was saved</returns>
    ''' <remarks>When saving as plain text, any custom
    ''' links take the form "{text|hyperlink}"</remarks>
    Public Overridable Function SaveFile(Optional ByVal fileName As String = "", Optional ByVal streamType As RichTextBoxStreamType = _NoSpecificFileFormat) As Boolean
        If String.IsNullOrEmpty(fileName) Then
            '   no file specified? then invoke file dialog
            Using sfd As New SaveFileDialog()
                With sfd
                    '   initialize file dialog
                    .Title = "Save Text File"
                    .CheckPathExists = True
                    .OverwritePrompt = True
                    Me.SetFileFormatFilters(sfd, streamType)
                    .FilterIndex = 1
                    .InitialDirectory = Me.FilePath
                    .RestoreDirectory = True
                    .ShowHelp = False
                    '   show dialog and get file
                    fileName = If(.ShowDialog() = DialogResult.Cancel, String.Empty, .FileName)
                End With
            End Using
        End If
        If Not String.IsNullOrWhiteSpace(fileName) Then
            '   load RTF text into rich text-box
            streamType = Me.GetFileFormat(fileName, streamType)
            Me.RichTextBox.SaveFile(fileName, streamType)
            Me.IsModified = False
        End If
        Me.FileName = fileName
        Return Not String.IsNullOrWhiteSpace(fileName)
    End Function

    ''' <summary> Creates a new document. </summary>
    Public Sub NewDocument()
        Me.Rtf = Me.RichTextFormatHeader
        Me.IsModified = False
    End Sub

    ''' <summary>
    ''' Load a file into the rich-text box
    ''' </summary>
    ''' <param name="fileName">Name of RTF/plain-text file
    ''' (if null or omitted, then a file dialog is invoked)</param>
    ''' <param name="streamType">Format of file (user-specified if -1 or omitted)</param>
    ''' <returns>False if dialog was canceled, True if text was loaded</returns>
    Public Overridable Function LoadFile(Optional ByVal fileName As String = "", Optional ByVal streamType As RichTextBoxStreamType = _NoSpecificFileFormat) As Boolean
        If String.IsNullOrEmpty(fileName) Then
            '   no file specified? then invoke file dialog
            Dim ofd As OpenFileDialog = New OpenFileDialog()
            With ofd
                '   initialize file dialog
                .Title = "Open Text File"
                .CheckPathExists = True
                .CheckFileExists = True
                Me.SetFileFormatFilters(ofd, streamType)
                .FilterIndex = 1
                .InitialDirectory = Me.FilePath
                .RestoreDirectory = True
                .ShowHelp = False
                '   show dialog and get file
                fileName = If(.ShowDialog() = DialogResult.Cancel, String.Empty, .FileName)
            End With
        End If
        If Not String.IsNullOrWhiteSpace(fileName) Then
            '   load RTF text into rich text-box
            streamType = Me.GetFileFormat(fileName, streamType)
            Me.RichTextBox.LoadFile(fileName, streamType)
            Me.UpdateToolbar()
            Me.IsModified = False
        End If
        Me.FileName = fileName
        Return Not String.IsNullOrWhiteSpace(fileName)
    End Function

    Private _PageSettings1 As Printing.PageSettings
    Private Property PageSettings As Printing.PageSettings
        Get
            Return Me._PageSettings1
        End Get
        Set(value As Printing.PageSettings)
            '   set right-margin of extended rich-text box
            Me.RichTextBox.SetRightMarginToPrinterWidth(value)
            '   reset printer width in case of approximation error
            Me.RichTextBox.SetPrinterWidthToRightMargin(value)
            Me._PageSettings1 = value
        End Set
    End Property

    Private Property PrinterSettings As Printing.PrinterSettings

    ''' <summary> Sets up the printing page. </summary>
    Public Sub PageSetup()
        Using pageSetupDialog As New PageSetupDialog
            With pageSetupDialog
                '   prepare set-up dialog
                .PageSettings = New System.Drawing.Printing.PageSettings()
                .PrinterSettings = New System.Drawing.Printing.PrinterSettings()
                If Windows.Forms.DialogResult.OK = .ShowDialog() Then
                    '    set page/printer settings
                    Me.PageSettings = .PageSettings
                    Me.PrinterSettings = .PrinterSettings
                End If
            End With
        End Using
    End Sub

    ''' <summary> Quick print. </summary>
    Public Sub QuickPrint()
        Using printDocument As Printing.PrintDocument = New Printing.PrintDocument With {.PrinterSettings = Me.PrinterSettings}
            Me.RichTextBox.Print(printDocument)
        End Using
    End Sub

    ''' <summary> Prints the rich text format. </summary>
    ''' <param name="preview"> True to preview. </param>
    ''' <returns> A DialogResult. </returns>
    Public Function Print(ByVal preview As Boolean) As DialogResult

        Dim printDialogResult As DialogResult = DialogResult.None
        Using printDialog As New PrintDialog
            With printDialog
                '   invoke print dialog
                .AllowPrintToFile = True : .AllowSomePages = True
                .AllowCurrentPage = True : .AllowSelection = True
                With .PrinterSettings
                    .MinimumPage = 1
                    .MaximumPage = Integer.MaxValue
                    .FromPage = 1
                    .ToPage = Integer.MaxValue
                End With
                printDialogResult = .ShowDialog
                If Windows.Forms.DialogResult.OK = printDialogResult Then
                    Me.PrinterSettings = .PrinterSettings
                End If
            End With
        End Using

        If Windows.Forms.DialogResult.OK = printDialogResult Then
            '   prepare to print
            If Me.PrinterSettings.PrintToFile Then
                '   handle printing to file (get file name)
                Using fileDialog As New SaveFileDialog()
                    fileDialog.AddExtension = True
                    fileDialog.DefaultExt = "prn"
                    fileDialog.OverwritePrompt = True
                    fileDialog.RestoreDirectory = True
                    fileDialog.InitialDirectory = Application.StartupPath
                    printDialogResult = fileDialog.ShowDialog()
                    If Windows.Forms.DialogResult.OK = printDialogResult Then
                        Me.PrinterSettings.PrintFileName = fileDialog.FileName
                    End If
                End Using
            End If
        End If

        If Windows.Forms.DialogResult.OK = printDialogResult Then
            Using printDocument As Printing.PrintDocument = New Printing.PrintDocument With {.PrinterSettings = Me.PrinterSettings}
                If preview Then
                    Using printPreviewDialog As New PrintPreviewDialog
                        printPreviewDialog.Document = printDocument
                        printDialogResult = Me.RichTextBox.PrintPreview(printPreviewDialog)
                    End Using
                End If
                If Windows.Forms.DialogResult.OK = printDialogResult Then
                    Me.RichTextBox.Print(printDocument)
                End If
            End Using
        End If
        Return printDialogResult
    End Function

#End Region

#Region " METHODS: INSERT "

    ''' <summary>
    ''' pastes a picture into the rich-text box at where text is selected
    ''' </summary>
    ''' <param name="fileName">Name of picture file
    ''' (if null or omitted, then a file dialog is invoked)</param>
    ''' <returns>False if dialog was canceled,
    ''' True (and ChangesMade event is fired) if picture was inserted</returns>
    Public Overridable Function InsertPicture(Optional ByVal fileName As String = "") As Boolean
        If String.IsNullOrEmpty(fileName) Then
            '   no file specified? then invoke file dialog
            Dim ofd As OpenFileDialog = New OpenFileDialog()
            With ofd
                '   initialize file dialog
                .DefaultExt = "bmp"
                .Title = "Insert Picture"
                .CheckPathExists = True
                .CheckFileExists = True
                .Filter =
                "Picture files (*.bmp; *.gif; *.jpeg; *.jpg; *.png; *.tif; *.tiff)|*.bmp;*.gif;*.jpeg;*.jpg;*.png;*.tif;*.tiff|All files (*.*)|*.*"
                .FilterIndex = 1
                .InitialDirectory = Me.FilePath
                .RestoreDirectory = True
                .ShowHelp = False
                '   show dialog and get file
                If .ShowDialog() = DialogResult.Cancel Then
                    Return False
                Else
                    fileName = .FileName
                End If
            End With
        End If
        '   paste picture into rich text-box
        Using Bmp As New Bitmap(fileName)
            Clipboard.SetImage(Bmp)
            Me.RichTextBox.Paste()
        End Using
        Me.IsModified = Me._IsNotProtected
        Me._IsNotProtected = True
        Return True
    End Function

    ''' <summary>
    ''' find out if a link is at the given position
    ''' </summary>
    ''' <param name="position">position to check for link
    ''' (current caret position if omitted or negative)</param>
    ''' <param name="areLookingBackwards">True to look backwards from position,
    ''' False (default if omitted) to look forwards from position</param>
    ''' <returns>CustomLinkInfo instance with starting position, text, and hyperlink</returns>
    ''' <remarms>NOTES:
    ''' 1. An exception is thrown if the custom links are disallowed (if the
    '''    DoCustomLinks property is False)
    ''' 2. If no link is present at the given position, then Nothing is returned</remarms>
    Public Function CheckForCustomLink(Optional ByVal position As Integer = -1, Optional ByVal areLookingBackwards As Boolean = False) As CustomLinkInfo
        '   make sure we can do this
        If Not Me._DoCustomLinks Then
            Throw New InvalidOperationException("Custom links are not enabled!")
        End If
        '   look for link
        If position < 0 OrElse position >= Me.RichTextBox.TextLength Then
            position = Me.RichTextBox.SelectionStart 'default to caret
        End If
        If areLookingBackwards Then
            position = -position 'going backwards
        End If
        Return Me.GetCustomLink(position, True)
    End Function

    ''' <summary>
    ''' add link with given information
    ''' </summary>
    ''' <param name="customLinkInfo">Information about insertion position,
    ''' text, and hyperlink</param>
    ''' <returns>True if added, False if not</returns>
    ''' <remarks>><list type="number">NOTES:<item>
    ''' An exception is thrown if the custom links are disallowed (if the DoCustomLinks property is False) or if CustomLinkInfo contains invalid data</item><item>
    ''' If a link is successfully added, then <list type="number"><item>
    '''  The caret moves to the end of the link, and</item><item>
    '''  A "hidden" syllable hyphen prepends the link text/hyperlink info, CustomLinkInfo.Position is incremented by 1</item></list></item></list>
    '''  </remarks>
    Public Function AddCustomLink(ByRef customLinkInfo As CustomLinkInfo) As Boolean
        With customLinkInfo
            '   make sure we can do this
            If Not Me._DoCustomLinks Then
                Throw New InvalidOperationException("Custom links are not enabled!")
            End If
            If customLinkInfo Is Nothing OrElse .Position < 0 OrElse .Position > Me.RichTextBox.TextLength OrElse
                String.IsNullOrEmpty(.Text) OrElse Regex.IsMatch(.Text, "[\{\|\}]") OrElse String.IsNullOrEmpty(.Hyperlink) OrElse
                Regex.IsMatch(.Hyperlink, "[\{\|\}]") Then
                Throw New ArgumentException("Invalid link information!")
            End If
            '   insert link
            Me.GetRange()
            Dim isSuccessful As Boolean = Me.InsertCustomLink(customLinkInfo)
            If isSuccessful Then
                '   if we did it, move caret to end of new link
                Me._StartPosition = Me.RichTextBox.SelectionStart
                Me._EndPosition = Me._StartPosition
            End If
            '   report result
            Me.RestoreRange()
            Return isSuccessful
        End With
    End Function

    ''' <summary>
    ''' remove link at a given position
    ''' </summary>
    ''' <param name="position">Starting position of link</param>
    ''' <returns>True if link removed, False if not</returns>
    ''' <remarms>>NOTES:
    ''' 1. An exception is thrown if the custom links are disallowed
    '''   (if the DoCustomLinks property is False), or if position is invalid
    ''' 2. If link is successfully removed, then caret move to end of de-linked text
    ''' 3. If the KeepHyperlinksOnRemove property is True, then the de-linked text
    '''    is of the form "{text|hyperlink}", including both main text and hyperlink text;
    '''    otherwise, it is "text", including only the main text</remarms>
    Public Function RemoveCustomLink(ByVal position As Integer) As Boolean
        '   make sure we can do this
        If Not Me._DoCustomLinks Then
            Throw New InvalidOperationException("Custom links are not enabled!")
        End If
        If position < 0 AndAlso position >= Me.RichTextBox.TextLength Then
            Throw New ArgumentOutOfRangeException("Invalid link position!")
        End If
        '   remove link
        Me.GetRange()
        Dim isSuccessful As Boolean = Me.DeleteCustomLink(position)
        If isSuccessful Then
            '   if we did it, move caret to end of de-linked text
            Me._StartPosition = Me.RichTextBox.SelectionStart
            Me._EndPosition = Me._StartPosition
        End If
        '   report result
        Me.RestoreRange()
        Return isSuccessful
    End Function

    ''' <summary>
    ''' Get a list of all custom links currently existing in selection or document
    ''' </summary>
    ''' <param name="inSelectionOnly">True to get custom links in selected text only,
    ''' False (default) to get links in entire document</param>
    ''' <returns>Array of CustomLinkInfo instances with each
    ''' link's position, visible text, and hyperlink text</returns>
    ''' <remarks>An exception is thrown if custom links are disallowed
    ''' (if DoCustomLinks is False)</remarks>
    Public Function GetCustomLinks(Optional ByVal inSelectionOnly As Boolean = False) As CustomLinkInfo()
        '   make sure we can do this
        If Not Me._DoCustomLinks Then
            Throw New InvalidOperationException("Custom links are not enabled!")
        End If
        If inSelectionOnly Then
            '   links in selection
            Return Me.GetCustomLinksAtPositions(Me.RichTextBox.SelectionStart, Me.RichTextBox.SelectionStart + Me.RichTextBox.SelectionLength)
        Else
            '   all links
            Return Me._CustomLinks.Values.ToArray
        End If
    End Function

    ''' <summary>
    ''' Temporarily unprotect custom link and disable auto-protection of links,
    ''' then fire EditingWithLinksUnprotected event to allow programmatic edits to
    ''' regions containing custom links (i.e., formatting changes)
    ''' </summary>
    ''' <param name="parameters">Information to be give to and receive from the
    ''' EditingWithLinksUnprotected event's procedure (can be omitted for no info)</param>
    ''' <remarks>NOTES:
    ''' 1. An exception is thrown if custom links are disallowed (if DoCustomLinks is False)
    ''' 2. Links are re-protected and auto-protection is re-enabled after the event finishes,
    '''    even if the event's procedure throws an unhandled exception
    ''' 3. This method is ignored if the EditingWithLinksUnprotected event is ALREADY
    '''    being processed</remarks>
    Public Sub EditWithLinksUnprotected(Optional ByRef parameters As Object = Nothing)
        '   make sure we can do this
        If Not Me._DoCustomLinks Then
            Throw New InvalidOperationException("Custom links are not enabled!")
        End If
        '   raise event and return an parameter changes
        Dim args As ParameterEventArgs = New ParameterEventArgs(parameters)
        Me.OnEditingWithLinksUnprotected(args)
        parameters = args.Parameters
    End Sub

#End Region

End Class

''' <summary>
''' Information about a link--position, visible text, and invisible hyperlink
''' </summary>
''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17 </para></remarks>
#Disable Warning IDE1006 ' Naming Styles
Public Class CustomLinkInfo
    Public Position As Integer 'position
    Public Text As String      'visible text
    Public Hyperlink As String 'invisible hyperlink text
End Class
#Enable Warning IDE1006 ' Naming Styles

''' <summary>
''' Event Arguments for InsertRtfText event--which allows one
''' to specify custom text for specific key sequences
''' </summary>
''' <remarks>If KeyEventArgs.SuppressKeyPress is True, then no change is made to text(c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17 </para></remarks>
Public Class InsertRtfTextEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' Supplies initial keyboard information
    ''' </summary>
    ''' <param name="keyEventArgs">KeyEventArgs instance for underlying KeyDown event</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal keyEventArgs As System.Windows.Forms.KeyEventArgs)
        Me.KeyEventArgs = keyEventArgs
    End Sub

    ''' <summary>
    ''' RTF text to be inserted
    ''' </summary>
    ''' <value>Text to be inserted in RTF format</value>
    ''' <returns>Text to be inserted in RTF format</returns>
    ''' <remarks>Defaults to null for making no change, except any default text allowed</remarks>
    Public Property RtfText As String = String.Empty

    ''' <summary>
    ''' Keyboard information
    ''' </summary>
    ''' <value>KeyEventArgs instance from underlying KeyDown event</value>
    ''' <returns>KeyEventArgs instance from underlying KeyDown event</returns>
    ''' <remarks>Set KeyEventArgs.SuppressKeyPress to True ensure that NO text is
    ''' inserted, even any default text</remarks>
    Public Property KeyEventArgs As System.Windows.Forms.KeyEventArgs 'keyboard info
End Class

''' <summary>
''' Event Arguments for SmartRtfText event, which allows one to replace a recent set of
''' characters (including an incoming one) with custom text.
''' </summary>
''' <remarks>
''' If no RTF text or length of text to replace is specified on output, no change is made to text. <para>
''' (c) 2008 Razi Syed. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17 </para></remarks>
Public Class SmartRtfTextEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' Supplies initial keyboard information
    ''' </summary>
    ''' <param name="keyPressEventArgs">KeyPressEventArgs instance
    ''' for underlying KeyPress event</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal keyPressEventArgs As System.Windows.Forms.KeyPressEventArgs)
        Me.KeyPressEventArgs = keyPressEventArgs
    End Sub

    ''' <summary>
    ''' Smart text to be inserted
    ''' </summary>
    ''' <value>Smart text to be inserted in RTF format</value>
    ''' <returns>Smart text to be inserted in RTF format</returns>
    ''' <remarks>Defaults to null for making no change</remarks>
    Public Property RtfText As String = String.Empty

    ''' <summary>
    ''' Number of characters to replace prior to incoming character
    ''' </summary>
    ''' <value># of characters to replace</value>
    ''' <returns># of characters to replace</returns>
    ''' <remarks>Characters preceding the incoming character are removed
    ''' before smart text is inserted</remarks>
    Public Property PrecedingCharacterCount As Integer = 0

    ''' <summary>
    ''' Keyboard information
    ''' </summary>
    ''' <value>KeyEventArgs instance from underlying KeyPress event</value>
    ''' <returns>KeyEventArgs instance from underlying KeyPress event</returns>
    ''' <remarks>e.KeyChar is the incoming character</remarks>
    Public Property KeyPressEventArgs As System.Windows.Forms.KeyPressEventArgs
End Class

''' <summary>
''' Event Arguments for HyperlinkClicked event-- returns CustomLinkInfo instance with position,
''' text, and hyperlink.
''' </summary>
''' <remarks>
''' If custom links are not enabled, then position is -1 and text and hyperlink are the same. <para>
''' (c) 2008 Razi Syed. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17 </para></remarks>
Public Class CustomLinkInfoEventArgs
    Inherits EventArgs
    ''' <summary>
    ''' Information about text and hyperlink link
    ''' </summary>
    ''' <returns>.Text = (visible) text, and .Hyperlink = (invisible) hyperlink</returns>
    Public Property CustomLinkInfo As CustomLinkInfo
End Class

''' <summary>
''' Event Arguments for EditingWithLinksUnprotected event--which allows one
''' to specify information for event
''' </summary>
Public Class ParameterEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' Supplies initial event information
    ''' </summary>
    ''' <param name="parameters">Information for event</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal parameters As Object)
        Me.Parameters = parameters
    End Sub

    ''' <summary>
    ''' Information for event procedure
    ''' </summary>
    ''' <value>Information supplied to event</value>
    ''' <returns>Information returned from event</returns>
    Public Property Parameters As Object
End Class
