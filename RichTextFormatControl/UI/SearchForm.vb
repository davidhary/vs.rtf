﻿Imports isr.Controls.RichTextFormat.TextBoxExtensions
''' <summary> Form for viewing the search. </summary>
''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17, 1.0.*">
''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex. 
''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox </para></remarks>
Public Class SearchForm

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <param name="searchInfo">          Information describing the search. </param>
    ''' <param name="allowingReplacing">   The allowing replacing. </param>
    ''' <param name="allowingCustomLinks"> The allowing custom links. </param>
    Public Sub New(ByVal searchInfo As SearchCriteria, ByVal allowingReplacing As Boolean, ByVal allowingCustomLinks As Boolean)
        Me.SearchInfo = searchInfo
        Me.AllowingReplacing = allowingReplacing
        Me.InitializeComponent()
    End Sub

    ''' <summary> Input: Gets the allowing replacing. </summary>
    ''' <value> The allowing replacing. </value>
    Public Property AllowingReplacing As Boolean

    ''' <summary> Input: Gets the allowing custom links. </summary>
    ''' <value> The allowing custom links. </value>
    Public Property AllowingCustomLinks As Boolean

    ''' <summary> Input/Output: Gets information describing the search. </summary>
    ''' <value> Information describing the search. </value>
    Public Property SearchInfo As SearchCriteria

    ''' <summary> Output: Gets the search option. </summary>
    ''' <value> The search option. </value>
    Public Property SearchOption As SearchOptions

    ''' <summary> Gets or sets the initializing. </summary>
    ''' <value> The initializing. </value>
    Private Property Initializing As Boolean = True

    ''' <summary> Searches for the first up radio button checked changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SearchUpRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SearchUpRadioButton.CheckedChanged
        With Me.SearchInfo
            If Me._SearchUpRadioButton.Checked Then
                '   search up (backward)
                .SearchFinds = (.SearchFinds Or RichTextBoxFinds.Reverse)
            Else
                '   search down (forward)
                .SearchFinds = (.SearchFinds And Not RichTextBoxFinds.Reverse)
            End If
        End With
    End Sub


    ''' <summary> Match case check box checked changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MatchCaseCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MatchCaseCheckBox.CheckedChanged
        With Me.SearchInfo
            If Me._MatchCaseCheckBox.Checked Then
                '   case sensitive search
                .SearchFinds = (.SearchFinds Or RichTextBoxFinds.MatchCase)
            Else
                '   case insensitive search
                .SearchFinds = (.SearchFinds And Not RichTextBoxFinds.MatchCase)
            End If
        End With

    End Sub

    ''' <summary> Match whole word check box checked changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MatchWholeWordCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MatchWholeWordCheckBox.CheckedChanged
        With Me.SearchInfo
            If Me._MatchWholeWordCheckBox.Checked Then
                '   find whole word
                .SearchFinds = (.SearchFinds Or RichTextBoxFinds.WholeWord)
            Else
                '   find any match
                .SearchFinds = (.SearchFinds And Not RichTextBoxFinds.WholeWord)
            End If

        End With
    End Sub

    ''' <summary> Searches for the first text box key down. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub SearchTextBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles _SearchTextBox.KeyDown
        InsertSpecialCharacters(Me._SearchTextBox, e)
    End Sub

    ''' <summary> Searches for the first text box text changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SearchTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _SearchTextBox.TextChanged

        Me.ShowOrHideControls()
        ' update info on rich text format control
        If Not Me.Initializing Then Me.SearchInfo.IsFirstFind = True
    End Sub

    ''' <summary> Replacement text box key down. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub ReplacementTextBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles _ReplacementTextBox.KeyDown
        InsertSpecialCharacters(Me._ReplacementTextBox, e)
    End Sub

    ''' <summary> Replacement text box text changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReplacementTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _ReplacementTextBox.TextChanged
        If Me.AllowingReplacing Then
            ' update info on rich text format control
            If Not Me.Initializing Then Me.SearchInfo.IsFirstFind = True
        End If
    End Sub


    ''' <summary> Cancel button click. abort search dialog. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.SearchInfo.IsFirstFind = True
        Me.SearchOption = SearchOptions.None
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub


    ''' <summary> Searches for the next button click. find next occurrence. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FindNextButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FindNextButton.Click
        Me.SetText()
        Me.SearchOption = SearchOptions.FindNext
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    ''' <summary> Replace button click. replace current occurrence </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReplaceButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReplaceButton.Click
        Me.SetText()
        Me.SearchOption = SearchOptions.Replace
        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub

    Private Sub ReplaceAllButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReplaceAllButton.Click
        '   replace all occurrences from here on
        Me.SetText()
        Me.SearchOption = SearchOptions.ReplaceAll
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    ''' <summary> Avoid links check box checked changed. 
    ''' Turns on or off protection of links from replacement. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AvoidLinksCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _AvoidLinksCheckBox.CheckedChanged
        Me.SearchInfo.AvoidLinks = Me._AvoidLinksCheckBox.Checked
    End Sub


    ''' <summary> Control load. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Control_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '   set up controls for search and replacement text
        Me.Initializing = True
        With Me.SearchInfo
            Me._SearchTextBox.Text = .SearchText
            Me._ReplacementTextBox.Text = .ReplacementText
            Me.ShowOrHideControls()
            '   set check boxes
            Me._MatchCaseCheckBox.Checked = CBool(.SearchFinds And RichTextBoxFinds.MatchCase)
            Me._MatchWholeWordCheckBox.Checked = CBool(.SearchFinds And RichTextBoxFinds.WholeWord)
            '   set search direction
            If (.SearchFinds And RichTextBoxFinds.Reverse) = 0 Then
                Me._SearchDownRadioButton.Checked = True 'forward
            Else
                Me._SearchUpRadioButton.Checked = True   'backward
            End If
            '   can we overwrite links?
            Me._AvoidLinksCheckBox.Checked = .AvoidLinks
        End With
        Me.Initializing = False
    End Sub

    ''' <summary> Shows the or hide controls. </summary>
    Private Sub ShowOrHideControls()
        '   is there anything to find?
        Dim AreEnabling As Boolean = Not String.IsNullOrEmpty(Me._SearchTextBox.Text)
        Me._FindNextButton.Enabled = AreEnabling
        Me._ReplaceButton.Enabled = AreEnabling
        Me._ReplaceAllButton.Enabled = AreEnabling
        Me._ReplacementTextBoxLabel.Enabled = AreEnabling
        Me._ReplacementTextBox.Enabled = AreEnabling
        Me._AvoidLinksCheckBox.Enabled = AreEnabling
        '   can we replace?
        If Me.AllowingReplacing Then
            '   find/replace   
            Me.Text = "Search/Replace Text"
            Me._ReplaceButton.Visible = True
            Me._ReplaceAllButton.Visible = True
            Me._ReplacementTextBoxLabel.Visible = True
            Me._ReplacementTextBox.Visible = True
            Me._AvoidLinksCheckBox.Visible = True
        Else
            '   find only
            Dim Reduction As Integer = Me._SearchOptionsGroupBox.Top - Me._ReplacementTextBoxLabel.Top
            Me._SearchOptionsGroupBox.Top -= Reduction
            Me._CancelButton.Top -= Reduction
            Me.Height -= Reduction
        End If
    End Sub

    ''' <summary> Sets the search and replacement text for host control. </summary>
    Private Sub SetText()
        With Me.SearchInfo
            .SearchText = Me._SearchTextBox.Text
            .ReplacementText = Me._ReplacementTextBox.Text
        End With
    End Sub

#End Region

End Class

''' <summary> A search criteria. </summary>
''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17 </para></remarks>
Public Class SearchCriteria

    Public Sub New(ByVal searchText As String, ByVal replacementText As String, ByVal searchFinds As RichTextBoxFinds, ByVal isFirstFind As Boolean)
        MyBase.New()
        Me.SearchText = searchText
        Me.ReplacementText = replacementText
        Me.SearchFinds = searchFinds
        Me.IsFirstFind = isFirstFind
        Me.AvoidLinks = True
    End Sub

    ''' <summary> Gets or sets the search text. </summary>
    ''' <value> The search text. </value>
    Public Property SearchText As String

    ''' <summary> Gets or sets the replacement text. </summary>
    ''' <value> The replacement text. </value>
    Public Property ReplacementText As String

    ''' <summary> Gets or sets the search finds: How to search for text. </summary>
    ''' <value> The search finds. </value>
    Public Property SearchFinds As RichTextBoxFinds

    ''' <summary> Gets or sets the is first find: Whether or not we should be searching. </summary>
    ''' <value> The is first find. </value>
    Public Property IsFirstFind As Boolean

    ''' <summary> Gets or sets the avoid links. </summary>
    ''' <value> The avoid links. </value>
    Public Property AvoidLinks As Boolean

End Class

''' <summary> Values that represent search options. </summary>
Public Enum SearchOptions
    None
    FindNext
    Replace
    ReplaceAll
End Enum
