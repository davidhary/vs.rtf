﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SearchForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SearchForm))
        Me._SearchTextBoxLabel = New System.Windows.Forms.Label()
        Me._SearchTextBox = New System.Windows.Forms.TextBox()
        Me._FindNextButton = New System.Windows.Forms.Button()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._ReplacementTextBoxLabel = New System.Windows.Forms.Label()
        Me._ReplacementTextBox = New System.Windows.Forms.TextBox()
        Me._ReplaceAllButton = New System.Windows.Forms.Button()
        Me._ReplaceButton = New System.Windows.Forms.Button()
        Me._SearchOptionsGroupBox = New System.Windows.Forms.GroupBox()
        Me._SearchDownRadioButton = New System.Windows.Forms.RadioButton()
        Me._SearchUpRadioButton = New System.Windows.Forms.RadioButton()
        Me._MatchCaseCheckBox = New System.Windows.Forms.CheckBox()
        Me._MatchWholeWordCheckBox = New System.Windows.Forms.CheckBox()
        Me._AvoidLinksCheckBox = New System.Windows.Forms.CheckBox()
        Me._SearchOptionsGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SearchTextBoxLabel
        '
        resources.ApplyResources(Me._SearchTextBoxLabel, "_SearchTextBoxLabel")
        Me._SearchTextBoxLabel.Name = "_SearchTextBoxLabel"
        '
        '_SearchTextBox
        '
        resources.ApplyResources(Me._SearchTextBox, "_SearchTextBox")
        Me._SearchTextBox.Name = "_SearchTextBox"
        '
        '_FindNextButton
        '
        resources.ApplyResources(Me._FindNextButton, "_FindNextButton")
        Me._FindNextButton.Name = "_FindNextButton"
        Me._FindNextButton.UseVisualStyleBackColor = True
        '
        '_CancelButton
        '
        Me._CancelButton.CausesValidation = False
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me._CancelButton, "_CancelButton")
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.UseVisualStyleBackColor = True
        '
        '_ReplacementTextBoxLabel
        '
        resources.ApplyResources(Me._ReplacementTextBoxLabel, "_ReplacementTextBoxLabel")
        Me._ReplacementTextBoxLabel.Name = "_ReplacementTextBoxLabel"
        '
        '_ReplacementTextBox
        '
        resources.ApplyResources(Me._ReplacementTextBox, "_ReplacementTextBox")
        Me._ReplacementTextBox.Name = "_ReplacementTextBox"
        '
        '_ReplaceAllButton
        '
        resources.ApplyResources(Me._ReplaceAllButton, "_ReplaceAllButton")
        Me._ReplaceAllButton.Name = "_ReplaceAllButton"
        Me._ReplaceAllButton.UseVisualStyleBackColor = True
        '
        '_ReplaceButton
        '
        resources.ApplyResources(Me._ReplaceButton, "_ReplaceButton")
        Me._ReplaceButton.Name = "_ReplaceButton"
        Me._ReplaceButton.UseVisualStyleBackColor = True
        '
        '_SearchOptionsGroupBox
        '
        Me._SearchOptionsGroupBox.Controls.Add(Me._SearchDownRadioButton)
        Me._SearchOptionsGroupBox.Controls.Add(Me._SearchUpRadioButton)
        Me._SearchOptionsGroupBox.Controls.Add(Me._MatchCaseCheckBox)
        Me._SearchOptionsGroupBox.Controls.Add(Me._MatchWholeWordCheckBox)
        resources.ApplyResources(Me._SearchOptionsGroupBox, "_SearchOptionsGroupBox")
        Me._SearchOptionsGroupBox.Name = "_SearchOptionsGroupBox"
        Me._SearchOptionsGroupBox.TabStop = False
        '
        '_SearchDownRadioButton
        '
        resources.ApplyResources(Me._SearchDownRadioButton, "_SearchDownRadioButton")
        Me._SearchDownRadioButton.CausesValidation = False
        Me._SearchDownRadioButton.Name = "_SearchDownRadioButton"
        Me._SearchDownRadioButton.TabStop = True
        Me._SearchDownRadioButton.UseVisualStyleBackColor = True
        '
        '_SearchUpRadioButton
        '
        resources.ApplyResources(Me._SearchUpRadioButton, "_SearchUpRadioButton")
        Me._SearchUpRadioButton.CausesValidation = False
        Me._SearchUpRadioButton.Name = "_SearchUpRadioButton"
        Me._SearchUpRadioButton.TabStop = True
        Me._SearchUpRadioButton.UseVisualStyleBackColor = True
        '
        '_MatchCaseCheckBox
        '
        resources.ApplyResources(Me._MatchCaseCheckBox, "_MatchCaseCheckBox")
        Me._MatchCaseCheckBox.CausesValidation = False
        Me._MatchCaseCheckBox.Name = "_MatchCaseCheckBox"
        Me._MatchCaseCheckBox.UseVisualStyleBackColor = True
        '
        '_MatchWholeWordCheckBox
        '
        resources.ApplyResources(Me._MatchWholeWordCheckBox, "_MatchWholeWordCheckBox")
        Me._MatchWholeWordCheckBox.CausesValidation = False
        Me._MatchWholeWordCheckBox.Name = "_MatchWholeWordCheckBox"
        Me._MatchWholeWordCheckBox.UseVisualStyleBackColor = True
        '
        '_AvoidLinksCheckBox
        '
        resources.ApplyResources(Me._AvoidLinksCheckBox, "_AvoidLinksCheckBox")
        Me._AvoidLinksCheckBox.Name = "_AvoidLinksCheckBox"
        Me._AvoidLinksCheckBox.UseVisualStyleBackColor = True
        '
        'SearchForm
        '
        Me.AcceptButton = Me._FindNextButton
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me._CancelButton
        Me.Controls.Add(Me._AvoidLinksCheckBox)
        Me.Controls.Add(Me._SearchOptionsGroupBox)
        Me.Controls.Add(Me._ReplaceButton)
        Me.Controls.Add(Me._ReplaceAllButton)
        Me.Controls.Add(Me._ReplacementTextBox)
        Me.Controls.Add(Me._ReplacementTextBoxLabel)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._FindNextButton)
        Me.Controls.Add(Me._SearchTextBox)
        Me.Controls.Add(Me._SearchTextBoxLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SearchForm"
        Me._SearchOptionsGroupBox.ResumeLayout(False)
        Me._SearchOptionsGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SearchTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SearchTextBox As System.Windows.Forms.TextBox
    Private WithEvents _FindNextButton As System.Windows.Forms.Button
    Private WithEvents _ReplacementTextBoxLabel As Label
    Private WithEvents _ReplaceAllButton As Button
    Private WithEvents _ReplaceButton As Button
    Private WithEvents _SearchOptionsGroupBox As GroupBox
    Private WithEvents _SearchDownRadioButton As RadioButton
    Private WithEvents _SearchUpRadioButton As RadioButton
    Private WithEvents _MatchCaseCheckBox As CheckBox
    Private WithEvents _MatchWholeWordCheckBox As CheckBox
    Private WithEvents _AvoidLinksCheckBox As CheckBox
    Private WithEvents _ReplacementTextBox As TextBox
    Private WithEvents _CancelButton As Button
End Class
