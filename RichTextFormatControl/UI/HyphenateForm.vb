''' <summary> Form for viewing the hyphenate. </summary>
''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17, 1.0.*">
''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex. 
''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox </para></remarks>
Public Class HyphenateForm

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <param name="word">        The word. </param>
    ''' <param name="maxPosition"> The maximum position. </param>
    Public Sub New(ByVal word As String, ByVal maxPosition As Integer)
        '   set parameters
        Me.Word = word
        Me.MaxPosition = maxPosition
        Me.DesiredPosition = 0
        Me.PendingDesiredPosition = maxPosition
        '   this call is required by the designer.
        Me.InitializeComponent()
    End Sub

#End Region

#Region " PUBLIC COMPONENTS: PROPERTIES "

    ''' <summary> Input: Gets the Text to hyphenate. </summary>
    ''' <value> The word. </value>
    Public Property Word As String

    ''' <summary> Input: Gets the maximum hyphenation position. </summary>
    ''' <value> The maximum position. </value>
    Public Property MaxPosition As Integer

    ''' <summary> Output: Gets the desired position. </summary>
    ''' <value> The desired position. </value>
    Public Property DesiredPosition As Integer

#End Region

#Region " PRIVATE COMPONENTS: VARIABLES "

    ''' <summary> Gets or sets the pending desired position. </summary>
    ''' <value> The pending desired position. </value>
    Private Property PendingDesiredPosition As Integer = 0


#End Region

#Region " PRIVATE COMPONENTS: EVENT HANDERS "

    ''' <summary> Control load. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Control_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me._WordTextBox.Text = Me.Word
        Me._WordTextBox.Select(Me.PendingDesiredPosition, 0)
        Me.UpdateHyphenationStatus()
    End Sub

    ''' <summary> Word text box key up. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub WordTextBox_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _WordTextBox.KeyUp
        Select Case e.KeyCode
            Case Keys.OemMinus, Keys.Subtract
                '   "-"" was pressed--hyphenate here
                Me.HyphenateButton_Click(sender, e)
            Case Else
                '   update display status
                Me.UpdateHyphenationStatus()
        End Select
    End Sub


    ''' <summary> Word text box mouse down. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub WordTextBox_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles _WordTextBox.MouseDown
        Me.UpdateHyphenationStatus()
    End Sub

    ''' <summary> Cancel button click. Stop looking for words to hyphenate.	</summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub


    ''' <summary> Hyphenate button click. hyphenate this word at given position. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HyphenateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HyphenateButton.Click
        Me.DesiredPosition = Me.PendingDesiredPosition

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    ''' <summary> Skip button click. leave this word not hyphenated </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>

    Private Sub SkipButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SkipButton.Click
        Me.DesiredPosition = 0
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

#End Region


#Region " PRIVATE COMPONENTS: NON-EVENT PROCEDURES "

    ''' <summary> Updates the hyphenation status. </summary>
    Private Sub UpdateHyphenationStatus()
        '   get and normalize hyphenation point
        Me._WordTextBox.SuspendLayout()
        Me.PendingDesiredPosition = Me._WordTextBox.SelectionStart
        If Me.PendingDesiredPosition < 2 Then
            '   must have at least 1 char on top line
            Me.PendingDesiredPosition = 2
        ElseIf Me.PendingDesiredPosition > Me.MaxPosition Then
            '   must not go past end of top line
            Me.PendingDesiredPosition = Me.MaxPosition
        End If
        Me._WordTextBox.Select(Me.PendingDesiredPosition, 0)
        Me._WordTextBox.Refresh()
        '   display broken text
        Dim HyphenatedText As String = $"Word as HYPHENATED:{ControlChars.CrLf}{Me.Word.Insert(Me.PendingDesiredPosition, $"-{ControlChars.CrLf}")}"
        Using g As Graphics = Me._WordTextBox.CreateGraphics()
            With Me._HyphenatedWordLabel
                Dim s As SizeF = g.MeasureString(HyphenatedText, .Font)
                .Width = CInt(s.Width)
                .Height = CInt(.Height)
                .Text = HyphenatedText
            End With
        End Using
    End Sub

#End Region

End Class
