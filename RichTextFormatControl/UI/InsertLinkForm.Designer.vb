﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InsertLinkForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InsertLinkForm))
        Me._LinkCaptionTextBoxLabel = New System.Windows.Forms.Label()
        Me._HyperlinkTextBoxLabel = New System.Windows.Forms.Label()
        Me._LinkCaptionTextBox = New System.Windows.Forms.TextBox()
        Me._HyperlinkTextBox = New System.Windows.Forms.TextBox()
        Me._HyperlinkSchemeContextMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me._FileMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FtpMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._GopherMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Separator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._HttpMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Http3WMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Separator2 = New System.Windows.Forms.ToolStripSeparator()
        Me._HttpsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Https3wMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Separator3 = New System.Windows.Forms.ToolStripSeparator()
        Me._MailtoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._NetPipeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._NetTcpMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._NewsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._NntpMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Separator4 = New System.Windows.Forms.ToolStripSeparator()
        Me._WwwMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InsertOrUpdateButton = New System.Windows.Forms.Button()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._RemoveButton = New System.Windows.Forms.Button()
        Me._KeepHypertextCheckBox = New System.Windows.Forms.CheckBox()
        Me._HyperlinkSchemeContextMenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_LinkCaptionTextBoxLabel
        '
        Me._LinkCaptionTextBoxLabel.AutoSize = True
        Me._LinkCaptionTextBoxLabel.Location = New System.Drawing.Point(23, 12)
        Me._LinkCaptionTextBoxLabel.Name = "_LinkCaptionTextBoxLabel"
        Me._LinkCaptionTextBoxLabel.Size = New System.Drawing.Size(68, 15)
        Me._LinkCaptionTextBoxLabel.TabIndex = 0
        Me._LinkCaptionTextBoxLabel.Text = "Visible &Text:"
        '
        '_HyperlinkTextBoxLabel
        '
        Me._HyperlinkTextBoxLabel.AutoSize = True
        Me._HyperlinkTextBoxLabel.Location = New System.Drawing.Point(33, 46)
        Me._HyperlinkTextBoxLabel.Name = "_HyperlinkTextBoxLabel"
        Me._HyperlinkTextBoxLabel.Size = New System.Drawing.Size(58, 15)
        Me._HyperlinkTextBoxLabel.TabIndex = 2
        Me._HyperlinkTextBoxLabel.Text = "&Hyperlink"
        '
        '_LinkCaptionTextBox
        '
        Me._LinkCaptionTextBox.Location = New System.Drawing.Point(93, 7)
        Me._LinkCaptionTextBox.Name = "_LinkCaptionTextBox"
        Me._LinkCaptionTextBox.Size = New System.Drawing.Size(255, 23)
        Me._LinkCaptionTextBox.TabIndex = 1
        '
        '_HyperlinkTextBox
        '
        Me._HyperlinkTextBox.ContextMenuStrip = Me._HyperlinkSchemeContextMenuStrip
        Me._HyperlinkTextBox.Location = New System.Drawing.Point(93, 42)
        Me._HyperlinkTextBox.Name = "_HyperlinkTextBox"
        Me._HyperlinkTextBox.Size = New System.Drawing.Size(255, 23)
        Me._HyperlinkTextBox.TabIndex = 3
        '
        '_HyperlinkSchemeContextMenuStrip
        '
        Me._HyperlinkSchemeContextMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileMenuItem, Me._FtpMenuItem, Me._GopherMenuItem, Me._Separator1, Me._HttpMenuItem, Me._Http3WMenuItem, Me._Separator2, Me._HttpsMenuItem, Me._Https3wMenuItem, Me._Separator3, Me._MailtoMenuItem, Me._NetPipeMenuItem, Me._NetTcpMenuItem, Me._NewsMenuItem, Me._NntpMenuItem, Me._Separator4, Me._WwwMenuItem})
        Me._HyperlinkSchemeContextMenuStrip.Name = "_HyperlinkSchemeContextMenuStrip"
        Me._HyperlinkSchemeContextMenuStrip.Size = New System.Drawing.Size(145, 314)
        '
        '_FileMenuItem
        '
        Me._FileMenuItem.Name = "_FileMenuItem"
        Me._FileMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._FileMenuItem.Text = "file://"
        '
        '_FtpMenuItem
        '
        Me._FtpMenuItem.Name = "_FtpMenuItem"
        Me._FtpMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._FtpMenuItem.Text = "ftp://"
        '
        '_GopherMenuItem
        '
        Me._GopherMenuItem.Name = "_GopherMenuItem"
        Me._GopherMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._GopherMenuItem.Text = "gopher://"
        '
        '_Separator1
        '
        Me._Separator1.Name = "_Separator1"
        Me._Separator1.Size = New System.Drawing.Size(141, 6)
        '
        '_HttpMenuItem
        '
        Me._HttpMenuItem.Name = "_HttpMenuItem"
        Me._HttpMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._HttpMenuItem.Text = "http://"
        '
        '_Http3WMenuItem
        '
        Me._Http3WMenuItem.Name = "_Http3WMenuItem"
        Me._Http3WMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._Http3WMenuItem.Text = "http://www."
        '
        '_Separator2
        '
        Me._Separator2.Name = "_Separator2"
        Me._Separator2.Size = New System.Drawing.Size(141, 6)
        '
        '_HttpsMenuItem
        '
        Me._HttpsMenuItem.Name = "_HttpsMenuItem"
        Me._HttpsMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._HttpsMenuItem.Text = "https://"
        '
        '_Https3wMenuItem
        '
        Me._Https3wMenuItem.Name = "_Https3wMenuItem"
        Me._Https3wMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._Https3wMenuItem.Text = "https://www."
        '
        '_Separator3
        '
        Me._Separator3.Name = "_Separator3"
        Me._Separator3.Size = New System.Drawing.Size(141, 6)
        '
        '_MailtoMenuItem
        '
        Me._MailtoMenuItem.Name = "_MailtoMenuItem"
        Me._MailtoMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._MailtoMenuItem.Text = "mailto:"
        '
        '_NetPipeMenuItem
        '
        Me._NetPipeMenuItem.Name = "_NetPipeMenuItem"
        Me._NetPipeMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._NetPipeMenuItem.Text = "net.pipe://"
        '
        '_NetTcpMenuItem
        '
        Me._NetTcpMenuItem.Name = "_NetTcpMenuItem"
        Me._NetTcpMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._NetTcpMenuItem.Text = "net.tcp://"
        '
        '_NewsMenuItem
        '
        Me._NewsMenuItem.Name = "_NewsMenuItem"
        Me._NewsMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._NewsMenuItem.Text = "news:"
        '
        '_NntpMenuItem
        '
        Me._NntpMenuItem.Name = "_NntpMenuItem"
        Me._NntpMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._NntpMenuItem.Text = "nntp://"
        '
        '_Separator4
        '
        Me._Separator4.Name = "_Separator4"
        Me._Separator4.Size = New System.Drawing.Size(141, 6)
        '
        '_WwwMenuItem
        '
        Me._WwwMenuItem.Name = "_WwwMenuItem"
        Me._WwwMenuItem.Size = New System.Drawing.Size(144, 22)
        Me._WwwMenuItem.Text = "www."
        '
        '_InsertOrUpdateButton
        '
        Me._InsertOrUpdateButton.Location = New System.Drawing.Point(26, 100)
        Me._InsertOrUpdateButton.Name = "_InsertOrUpdateButton"
        Me._InsertOrUpdateButton.Size = New System.Drawing.Size(87, 27)
        Me._InsertOrUpdateButton.TabIndex = 5
        Me._InsertOrUpdateButton.Text = "&Insert"
        Me._InsertOrUpdateButton.UseVisualStyleBackColor = True
        '
        '_CancelButton
        '
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.Location = New System.Drawing.Point(259, 100)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(87, 27)
        Me._CancelButton.TabIndex = 7
        Me._CancelButton.Text = "&Cancel"
        Me._CancelButton.UseVisualStyleBackColor = True
        '
        '_RemoveButton
        '
        Me._RemoveButton.Enabled = False
        Me._RemoveButton.Location = New System.Drawing.Point(142, 100)
        Me._RemoveButton.Name = "_RemoveButton"
        Me._RemoveButton.Size = New System.Drawing.Size(87, 27)
        Me._RemoveButton.TabIndex = 6
        Me._RemoveButton.Text = "&Remove"
        Me._RemoveButton.UseVisualStyleBackColor = True
        Me._RemoveButton.Visible = False
        '
        '_KeepHypertextCheckBox
        '
        Me._KeepHypertextCheckBox.AutoSize = True
        Me._KeepHypertextCheckBox.Location = New System.Drawing.Point(66, 72)
        Me._KeepHypertextCheckBox.Name = "_KeepHypertextCheckBox"
        Me._KeepHypertextCheckBox.Size = New System.Drawing.Size(201, 19)
        Me._KeepHypertextCheckBox.TabIndex = 4
        Me._KeepHypertextCheckBox.Text = "&Keep hypertext in text on remove"
        Me._KeepHypertextCheckBox.UseVisualStyleBackColor = True
        '
        'InsertLinkForm
        '
        Me.AcceptButton = Me._InsertOrUpdateButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.CancelButton = Me._CancelButton
        Me.ClientSize = New System.Drawing.Size(373, 134)
        Me.Controls.Add(Me._KeepHypertextCheckBox)
        Me.Controls.Add(Me._RemoveButton)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._InsertOrUpdateButton)
        Me.Controls.Add(Me._HyperlinkTextBox)
        Me.Controls.Add(Me._LinkCaptionTextBox)
        Me.Controls.Add(Me._HyperlinkTextBoxLabel)
        Me.Controls.Add(Me._LinkCaptionTextBoxLabel)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "InsertLinkForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Insert Link"
        Me._HyperlinkSchemeContextMenuStrip.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _LinkCaptionTextBoxLabel As Label
    Private WithEvents _KeepHypertextCheckBox As CheckBox
    Private WithEvents _HyperlinkSchemeContextMenuStrip As ContextMenuStrip
    Private WithEvents _FileMenuItem As ToolStripMenuItem
    Private WithEvents _FtpMenuItem As ToolStripMenuItem
    Private WithEvents _GopherMenuItem As ToolStripMenuItem
    Private WithEvents _HttpMenuItem As ToolStripMenuItem
    Private WithEvents _Http3WMenuItem As ToolStripMenuItem
    Private WithEvents _HttpsMenuItem As ToolStripMenuItem
    Private WithEvents _Https3wMenuItem As ToolStripMenuItem
    Private WithEvents _MailtoMenuItem As ToolStripMenuItem
    Private WithEvents _NewsMenuItem As ToolStripMenuItem
    Private WithEvents _NntpMenuItem As ToolStripMenuItem
    Private WithEvents _NetPipeMenuItem As ToolStripMenuItem
    Private WithEvents _NetTcpMenuItem As ToolStripMenuItem
    Private WithEvents _WwwMenuItem As ToolStripMenuItem
    Private WithEvents _Separator1 As ToolStripSeparator
    Private WithEvents _Separator2 As ToolStripSeparator
    Private WithEvents _Separator3 As ToolStripSeparator
    Private WithEvents _Separator4 As ToolStripSeparator
    Private WithEvents _LinkCaptionTextBox As TextBox
    Private WithEvents _HyperlinkTextBoxLabel As Label
    Private WithEvents _HyperlinkTextBox As TextBox
    Private WithEvents _CancelButton As Button
    Private WithEvents _RemoveButton As Button
    Private WithEvents _InsertOrUpdateButton As Button
End Class
