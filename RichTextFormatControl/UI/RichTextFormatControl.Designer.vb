﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RichTextFormatControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RichTextFormatControl))
        Me._Rtb = New System.Windows.Forms.RichTextBox()
        Me._ContextMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me._FileContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._NewFileContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileOpenContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileSaveContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileSaveAsContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileCloseContextSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._FileCloseContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PrintContextSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._PrintContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._QuickPrintContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PrintPreviewContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PageSetupContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FileContextSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._EditContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SelectAllContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CopyContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CutContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PasteContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._UndoContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RedoContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FontContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._BoldContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ItalicContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._UnderlineContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._StrikeThroughContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._GeneralFontSettingsContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AlignmentContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LeftAlignContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CenterAlignContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RightAlignContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._VerticalPositionContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SuperscriptContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SubscriptContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SearchContextSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._SearchContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FindContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FindNextContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FindPreviousContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReplaceContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SpellCheckContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SpellCheckContextSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._ListContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListNoneContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListBulletContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListNumberContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListLowerCaseAlphaContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListUpperCaseAlphaContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListLowerCaseRomanContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListUpperCaseRomanContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InsertContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InsertPictureContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InsertSymbolContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._HyphenationContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._HyphenateTextContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RemoveHyphensContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RemoveHiddenHyphensContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._HyperlinksContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InsertEditRemoveHyperlinkContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RemoveHyperLinksContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._KeepHypertextContextSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._KeepHyperTextWhenRemovingLinksContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SpellChecker = New NetSpell.SpellChecker.Spelling(Me.components)
        Me._FontButton = New System.Windows.Forms.ToolStripButton()
        Me._FontColorButton = New System.Windows.Forms.ToolStripButton()
        Me._BoldButton = New System.Windows.Forms.ToolStripButton()
        Me._ItalicButton = New System.Windows.Forms.ToolStripButton()
        Me._UnderlineButton = New System.Windows.Forms.ToolStripButton()
        Me._AlignmentToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._LeftAlignButton = New System.Windows.Forms.ToolStripButton()
        Me._CenterAlignButton = New System.Windows.Forms.ToolStripButton()
        Me._RightAlignButton = New System.Windows.Forms.ToolStripButton()
        Me._InsertToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._SpellCheckButton = New System.Windows.Forms.ToolStripButton()
        Me._SearchToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._FindNextButton = New System.Windows.Forms.ToolStripButton()
        Me._TopToolStrip = New System.Windows.Forms.ToolStrip()
        Me._NewFileButton = New System.Windows.Forms.ToolStripButton()
        Me._SaveButton = New System.Windows.Forms.ToolStripButton()
        Me._FileSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._BackColorButton = New System.Windows.Forms.ToolStripButton()
        Me._FontToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._FontNameComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._FontSizeComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._FindButton = New System.Windows.Forms.ToolStripButton()
        Me._InsertPictureButton = New System.Windows.Forms.ToolStripButton()
        Me._InsertSymbolButton = New System.Windows.Forms.ToolStripButton()
        Me._HyphenToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._HyphenateButton = New System.Windows.Forms.ToolStripButton()
        Me._SpellingToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._ListDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._ListNoneMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListBulletMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListNumbersMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListLowerCaseAlphaMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListUpperCaseAlphaMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListLowerCaseRomanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ListUpperCaseRomanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LinksToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._HyperlinkButton = New System.Windows.Forms.ToolStripButton()
        Me._LastToolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._TopRuler = New isr.Controls.RichTextFormat.TextRuler()
        Me._RulerContextMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me._InchesContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CentimetersContextMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ContextMenuStrip.SuspendLayout()
        Me._TopToolStrip.SuspendLayout()
        Me._RulerContextMenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Rtb
        '
        Me._Rtb.AcceptsTab = True
        Me._Rtb.ContextMenuStrip = Me._ContextMenuStrip
        Me._Rtb.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Rtb.EnableAutoDragDrop = True
        Me._Rtb.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Rtb.HideSelection = False
        Me._Rtb.Location = New System.Drawing.Point(0, 45)
        Me._Rtb.Name = "_Rtb"
        Me._Rtb.ShowSelectionMargin = True
        Me._Rtb.Size = New System.Drawing.Size(769, 173)
        Me._Rtb.TabIndex = 0
        Me._Rtb.Text = String.Empty
        Me._Rtb.WordWrap = False
        '
        '_ContextMenuStrip
        '
        Me._ContextMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FileContextMenuItem, Me._FileContextSeparator, Me._EditContextMenuItem, Me._FontContextMenuItem, Me._AlignmentContextMenuItem, Me._VerticalPositionContextMenuItem, Me._SearchContextSeparator, Me._SearchContextMenuItem, Me._SpellCheckContextMenuItem, Me._SpellCheckContextSeparator, Me._ListContextMenuItem, Me._InsertContextMenuItem, Me._HyphenationContextMenuItem, Me._HyperlinksContextMenuItem})
        Me._ContextMenuStrip.Name = "_ContextMenuStrip"
        Me._ContextMenuStrip.Size = New System.Drawing.Size(224, 264)
        '
        '_FileContextMenuItem
        '
        Me._FileContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._NewFileContextMenuItem, Me._FileOpenContextMenuItem, Me._FileSaveContextMenuItem, Me._FileSaveAsContextMenuItem, Me._FileCloseContextSeparator, Me._FileCloseContextMenuItem, Me._PrintContextSeparator, Me._PrintContextMenuItem, Me._QuickPrintContextMenuItem, Me._PrintPreviewContextMenuItem, Me._PageSetupContextMenuItem})
        Me._FileContextMenuItem.Name = "_FileContextMenuItem"
        Me._FileContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._FileContextMenuItem.Text = "&File"
        '
        '_NewFileContextMenuItem
        '
        Me._NewFileContextMenuItem.Name = "_NewFileContextMenuItem"
        Me._NewFileContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me._NewFileContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._NewFileContextMenuItem.Text = "&New"
        Me._NewFileContextMenuItem.ToolTipText = "New file"
        '
        '_FileOpenContextMenuItem
        '
        Me._FileOpenContextMenuItem.Name = "_FileOpenContextMenuItem"
        Me._FileOpenContextMenuItem.ShortcutKeyDisplayString = String.Empty
        Me._FileOpenContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me._FileOpenContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._FileOpenContextMenuItem.Text = "&Open ..."
        Me._FileOpenContextMenuItem.ToolTipText = "Open a new file"
        '
        '_FileSaveContextMenuItem
        '
        Me._FileSaveContextMenuItem.Name = "_FileSaveContextMenuItem"
        Me._FileSaveContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me._FileSaveContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._FileSaveContextMenuItem.Text = "&Save"
        Me._FileSaveContextMenuItem.ToolTipText = "Save the file"
        '
        '_FileSaveAsContextMenuItem
        '
        Me._FileSaveAsContextMenuItem.Name = "_FileSaveAsContextMenuItem"
        Me._FileSaveAsContextMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me._FileSaveAsContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._FileSaveAsContextMenuItem.Text = "Save &As ,,,"
        Me._FileSaveAsContextMenuItem.ToolTipText = "Save the file choosing a new name"
        '
        '_FileCloseContextSeparator
        '
        Me._FileCloseContextSeparator.Name = "_FileCloseContextSeparator"
        Me._FileCloseContextSeparator.Size = New System.Drawing.Size(203, 6)
        '
        '_FileCloseContextMenuItem
        '
        Me._FileCloseContextMenuItem.Name = "_FileCloseContextMenuItem"
        Me._FileCloseContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._FileCloseContextMenuItem.Text = "&Close"
        Me._FileCloseContextMenuItem.ToolTipText = "Close the file and remove text"
        '
        '_PrintContextSeparator
        '
        Me._PrintContextSeparator.Name = "_PrintContextSeparator"
        Me._PrintContextSeparator.Size = New System.Drawing.Size(203, 6)
        '
        '_PrintContextMenuItem
        '
        Me._PrintContextMenuItem.Name = "_PrintContextMenuItem"
        Me._PrintContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me._PrintContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._PrintContextMenuItem.Text = "&Print ..."
        '
        '_QuickPrintContextMenuItem
        '
        Me._QuickPrintContextMenuItem.Name = "_QuickPrintContextMenuItem"
        Me._QuickPrintContextMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me._QuickPrintContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._QuickPrintContextMenuItem.Text = "&Quick Print"
        Me._QuickPrintContextMenuItem.ToolTipText = "Print using default or last settings"
        '
        '_PrintPreviewContextMenuItem
        '
        Me._PrintPreviewContextMenuItem.Name = "_PrintPreviewContextMenuItem"
        Me._PrintPreviewContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._PrintPreviewContextMenuItem.Text = "Print Pre&view ..."
        Me._PrintPreviewContextMenuItem.ToolTipText = "Print after preview"
        '
        '_PageSetupContextMenuItem
        '
        Me._PageSetupContextMenuItem.Name = "_PageSetupContextMenuItem"
        Me._PageSetupContextMenuItem.Size = New System.Drawing.Size(206, 22)
        Me._PageSetupContextMenuItem.Text = "Page Set&up ..."
        Me._PageSetupContextMenuItem.ToolTipText = "Sets up the printing page"
        '
        '_FileContextSeparator
        '
        Me._FileContextSeparator.Name = "_FileContextSeparator"
        Me._FileContextSeparator.Size = New System.Drawing.Size(220, 6)
        '
        '_EditContextMenuItem
        '
        Me._EditContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SelectAllContextMenuItem, Me._CopyContextMenuItem, Me._CutContextMenuItem, Me._PasteContextMenuItem, Me._UndoContextMenuItem, Me._RedoContextMenuItem})
        Me._EditContextMenuItem.Name = "_EditContextMenuItem"
        Me._EditContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._EditContextMenuItem.Text = "&Edit"
        '
        '_SelectAllContextMenuItem
        '
        Me._SelectAllContextMenuItem.Name = "_SelectAllContextMenuItem"
        Me._SelectAllContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me._SelectAllContextMenuItem.Size = New System.Drawing.Size(164, 22)
        Me._SelectAllContextMenuItem.Text = "Select &All"
        '
        '_CopyContextMenuItem
        '
        Me._CopyContextMenuItem.Name = "_CopyContextMenuItem"
        Me._CopyContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me._CopyContextMenuItem.Size = New System.Drawing.Size(164, 22)
        Me._CopyContextMenuItem.Text = "&Copy"
        '
        '_CutContextMenuItem
        '
        Me._CutContextMenuItem.Name = "_CutContextMenuItem"
        Me._CutContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me._CutContextMenuItem.Size = New System.Drawing.Size(164, 22)
        Me._CutContextMenuItem.Text = "C&ut"
        '
        '_PasteContextMenuItem
        '
        Me._PasteContextMenuItem.Name = "_PasteContextMenuItem"
        Me._PasteContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me._PasteContextMenuItem.Size = New System.Drawing.Size(164, 22)
        Me._PasteContextMenuItem.Text = "&Paste"
        '
        '_UndoContextMenuItem
        '
        Me._UndoContextMenuItem.Name = "_UndoContextMenuItem"
        Me._UndoContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me._UndoContextMenuItem.Size = New System.Drawing.Size(164, 22)
        Me._UndoContextMenuItem.Text = "&Undo"
        '
        '_RedoContextMenuItem
        '
        Me._RedoContextMenuItem.Name = "_RedoContextMenuItem"
        Me._RedoContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me._RedoContextMenuItem.Size = New System.Drawing.Size(164, 22)
        Me._RedoContextMenuItem.Text = "&Redo"
        '
        '_FontContextMenuItem
        '
        Me._FontContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._BoldContextMenuItem, Me._ItalicContextMenuItem, Me._UnderlineContextMenuItem, Me._StrikeThroughContextMenuItem, Me._GeneralFontSettingsContextMenuItem})
        Me._FontContextMenuItem.Name = "_FontContextMenuItem"
        Me._FontContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._FontContextMenuItem.Text = "F&ont"
        '
        '_BoldContextMenuItem
        '
        Me._BoldContextMenuItem.Name = "_BoldContextMenuItem"
        Me._BoldContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me._BoldContextMenuItem.Size = New System.Drawing.Size(224, 22)
        Me._BoldContextMenuItem.Text = "&Bold"
        '
        '_ItalicContextMenuItem
        '
        Me._ItalicContextMenuItem.Name = "_ItalicContextMenuItem"
        Me._ItalicContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me._ItalicContextMenuItem.Size = New System.Drawing.Size(224, 22)
        Me._ItalicContextMenuItem.Text = "&Italic"
        '
        '_UnderlineContextMenuItem
        '
        Me._UnderlineContextMenuItem.Name = "_UnderlineContextMenuItem"
        Me._UnderlineContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me._UnderlineContextMenuItem.Size = New System.Drawing.Size(224, 22)
        Me._UnderlineContextMenuItem.Text = "&Underline"
        '
        '_StrikeThroughContextMenuItem
        '
        Me._StrikeThroughContextMenuItem.Name = "_StrikeThroughContextMenuItem"
        Me._StrikeThroughContextMenuItem.Size = New System.Drawing.Size(224, 22)
        Me._StrikeThroughContextMenuItem.Text = "&Strikethrough"
        '
        '_GeneralFontSettingsContextMenuItem
        '
        Me._GeneralFontSettingsContextMenuItem.Name = "_GeneralFontSettingsContextMenuItem"
        Me._GeneralFontSettingsContextMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me._GeneralFontSettingsContextMenuItem.Size = New System.Drawing.Size(224, 22)
        Me._GeneralFontSettingsContextMenuItem.Text = "&Font Settings..."
        '
        '_AlignmentContextMenuItem
        '
        Me._AlignmentContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LeftAlignContextMenuItem, Me._CenterAlignContextMenuItem, Me._RightAlignContextMenuItem})
        Me._AlignmentContextMenuItem.Name = "_AlignmentContextMenuItem"
        Me._AlignmentContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._AlignmentContextMenuItem.Text = "&Alignment"
        '
        '_LeftAlignContextMenuItem
        '
        Me._LeftAlignContextMenuItem.Name = "_LeftAlignContextMenuItem"
        Me._LeftAlignContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me._LeftAlignContextMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._LeftAlignContextMenuItem.Text = "&Left"
        '
        '_CenterAlignContextMenuItem
        '
        Me._CenterAlignContextMenuItem.Name = "_CenterAlignContextMenuItem"
        Me._CenterAlignContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me._CenterAlignContextMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._CenterAlignContextMenuItem.Text = "&Center"
        '
        '_RightAlignContextMenuItem
        '
        Me._RightAlignContextMenuItem.Name = "_RightAlignContextMenuItem"
        Me._RightAlignContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me._RightAlignContextMenuItem.Size = New System.Drawing.Size(149, 22)
        Me._RightAlignContextMenuItem.Text = "&Right"
        '
        '_VerticalPositionContextMenuItem
        '
        Me._VerticalPositionContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SuperscriptContextMenuItem, Me._SubscriptContextMenuItem})
        Me._VerticalPositionContextMenuItem.Name = "_VerticalPositionContextMenuItem"
        Me._VerticalPositionContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._VerticalPositionContextMenuItem.Text = "&Vertical Position"
        '
        '_SuperscriptContextMenuItem
        '
        Me._SuperscriptContextMenuItem.Name = "_SuperscriptContextMenuItem"
        Me._SuperscriptContextMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift++"
        Me._SuperscriptContextMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.Oemplus), System.Windows.Forms.Keys)
        Me._SuperscriptContextMenuItem.Size = New System.Drawing.Size(252, 22)
        Me._SuperscriptContextMenuItem.Text = "&Raised (Superscript)"
        '
        '_SubscriptContextMenuItem
        '
        Me._SubscriptContextMenuItem.Name = "_SubscriptContextMenuItem"
        Me._SubscriptContextMenuItem.ShortcutKeyDisplayString = "Ctrl+="
        Me._SubscriptContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Oemplus), System.Windows.Forms.Keys)
        Me._SubscriptContextMenuItem.Size = New System.Drawing.Size(252, 22)
        Me._SubscriptContextMenuItem.Text = "&Lowered (Subscript)"
        '
        '_SearchContextSeparator
        '
        Me._SearchContextSeparator.Name = "_SearchContextSeparator"
        Me._SearchContextSeparator.Size = New System.Drawing.Size(220, 6)
        '
        '_SearchContextMenuItem
        '
        Me._SearchContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FindContextMenuItem, Me._FindNextContextMenuItem, Me._FindPreviousContextMenuItem, Me._ReplaceContextMenuItem})
        Me._SearchContextMenuItem.Enabled = False
        Me._SearchContextMenuItem.Name = "_SearchContextMenuItem"
        Me._SearchContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._SearchContextMenuItem.Text = "&Search"
        '
        '_FindContextMenuItem
        '
        Me._FindContextMenuItem.Enabled = False
        Me._FindContextMenuItem.Name = "_FindContextMenuItem"
        Me._FindContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me._FindContextMenuItem.Size = New System.Drawing.Size(196, 22)
        Me._FindContextMenuItem.Text = "&Find..."
        '
        '_FindNextContextMenuItem
        '
        Me._FindNextContextMenuItem.Enabled = False
        Me._FindNextContextMenuItem.Name = "_FindNextContextMenuItem"
        Me._FindNextContextMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3
        Me._FindNextContextMenuItem.Size = New System.Drawing.Size(196, 22)
        Me._FindNextContextMenuItem.Text = "Find &Next"
        '
        '_FindPreviousContextMenuItem
        '
        Me._FindPreviousContextMenuItem.Enabled = False
        Me._FindPreviousContextMenuItem.Name = "_FindPreviousContextMenuItem"
        Me._FindPreviousContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Shift Or System.Windows.Forms.Keys.F3), System.Windows.Forms.Keys)
        Me._FindPreviousContextMenuItem.Size = New System.Drawing.Size(196, 22)
        Me._FindPreviousContextMenuItem.Text = "Find &Previous"
        '
        '_ReplaceContextMenuItem
        '
        Me._ReplaceContextMenuItem.Name = "_ReplaceContextMenuItem"
        Me._ReplaceContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me._ReplaceContextMenuItem.Size = New System.Drawing.Size(196, 22)
        Me._ReplaceContextMenuItem.Text = "&Replace..."
        '
        '_SpellCheckContextMenuItem
        '
        Me._SpellCheckContextMenuItem.Enabled = False
        Me._SpellCheckContextMenuItem.Name = "_SpellCheckContextMenuItem"
        Me._SpellCheckContextMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7
        Me._SpellCheckContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._SpellCheckContextMenuItem.Text = "Spell &Check..."
        '
        '_SpellCheckContextSeparator
        '
        Me._SpellCheckContextSeparator.Name = "_SpellCheckContextSeparator"
        Me._SpellCheckContextSeparator.Size = New System.Drawing.Size(220, 6)
        '
        '_ListContextMenuItem
        '
        Me._ListContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ListNoneContextMenuItem, Me._ListBulletContextMenuItem, Me._ListNumberContextMenuItem, Me._ListLowerCaseAlphaContextMenuItem, Me._ListUpperCaseAlphaContextMenuItem, Me._ListLowerCaseRomanContextMenuItem, Me._ListUpperCaseRomanContextMenuItem})
        Me._ListContextMenuItem.Name = "_ListContextMenuItem"
        Me._ListContextMenuItem.ShortcutKeyDisplayString = "Ctrl + Shift + L"
        Me._ListContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._ListContextMenuItem.Text = "Set &List Style"
        '
        '_ListNoneContextMenuItem
        '
        Me._ListNoneContextMenuItem.Name = "_ListNoneContextMenuItem"
        Me._ListNoneContextMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListNoneContextMenuItem.Tag = "-1"
        Me._ListNoneContextMenuItem.Text = "(None)"
        '
        '_ListBulletContextMenuItem
        '
        Me._ListBulletContextMenuItem.Name = "_ListBulletContextMenuItem"
        Me._ListBulletContextMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListBulletContextMenuItem.Tag = "0"
        Me._ListBulletContextMenuItem.Text = "● (Bullets)"
        '
        '_ListNumberContextMenuItem
        '
        Me._ListNumberContextMenuItem.Name = "_ListNumberContextMenuItem"
        Me._ListNumberContextMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListNumberContextMenuItem.Tag = "1"
        Me._ListNumberContextMenuItem.Text = "1, 2, 3, ..."
        '
        '_ListLowerCaseAlphaContextMenuItem
        '
        Me._ListLowerCaseAlphaContextMenuItem.Name = "_ListLowerCaseAlphaContextMenuItem"
        Me._ListLowerCaseAlphaContextMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListLowerCaseAlphaContextMenuItem.Tag = "2"
        Me._ListLowerCaseAlphaContextMenuItem.Text = "a, b, c, ..."
        '
        '_ListUpperCaseAlphaContextMenuItem
        '
        Me._ListUpperCaseAlphaContextMenuItem.Name = "_ListUpperCaseAlphaContextMenuItem"
        Me._ListUpperCaseAlphaContextMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListUpperCaseAlphaContextMenuItem.Tag = "3"
        Me._ListUpperCaseAlphaContextMenuItem.Text = "A, B, C, ..."
        '
        '_ListLowerCaseRomanContextMenuItem
        '
        Me._ListLowerCaseRomanContextMenuItem.Name = "_ListLowerCaseRomanContextMenuItem"
        Me._ListLowerCaseRomanContextMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListLowerCaseRomanContextMenuItem.Tag = "4"
        Me._ListLowerCaseRomanContextMenuItem.Text = "i, ii, iii, ..."
        '
        '_ListUpperCaseRomanContextMenuItem
        '
        Me._ListUpperCaseRomanContextMenuItem.Name = "_ListUpperCaseRomanContextMenuItem"
        Me._ListUpperCaseRomanContextMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListUpperCaseRomanContextMenuItem.Tag = "5"
        Me._ListUpperCaseRomanContextMenuItem.Text = "I, II, III, ..."
        '
        '_InsertContextMenuItem
        '
        Me._InsertContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InsertPictureContextMenuItem, Me._InsertSymbolContextMenuItem})
        Me._InsertContextMenuItem.Name = "_InsertContextMenuItem"
        Me._InsertContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._InsertContextMenuItem.Text = "&Insert"
        '
        '_InsertPictureContextMenuItem
        '
        Me._InsertPictureContextMenuItem.Name = "_InsertPictureContextMenuItem"
        Me._InsertPictureContextMenuItem.Size = New System.Drawing.Size(123, 22)
        Me._InsertPictureContextMenuItem.Text = "&Picture..."
        '
        '_InsertSymbolContextMenuItem
        '
        Me._InsertSymbolContextMenuItem.Name = "_InsertSymbolContextMenuItem"
        Me._InsertSymbolContextMenuItem.Size = New System.Drawing.Size(123, 22)
        Me._InsertSymbolContextMenuItem.Text = "&Symbol..."
        '
        '_HyphenationContextMenuItem
        '
        Me._HyphenationContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._HyphenateTextContextMenuItem, Me._RemoveHyphensContextMenuItem, Me._RemoveHiddenHyphensContextMenuItem})
        Me._HyphenationContextMenuItem.Name = "_HyphenationContextMenuItem"
        Me._HyphenationContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._HyphenationContextMenuItem.Text = "&Hyphenation"
        '
        '_HyphenateTextContextMenuItem
        '
        Me._HyphenateTextContextMenuItem.Name = "_HyphenateTextContextMenuItem"
        Me._HyphenateTextContextMenuItem.Size = New System.Drawing.Size(237, 22)
        Me._HyphenateTextContextMenuItem.Text = "&Hyphenate..."
        '
        '_RemoveHyphensContextMenuItem
        '
        Me._RemoveHyphensContextMenuItem.Name = "_RemoveHyphensContextMenuItem"
        Me._RemoveHyphensContextMenuItem.Size = New System.Drawing.Size(237, 22)
        Me._RemoveHyphensContextMenuItem.Text = "&Remove All Hyphens"
        '
        '_RemoveHiddenHyphensContextMenuItem
        '
        Me._RemoveHiddenHyphensContextMenuItem.Name = "_RemoveHiddenHyphensContextMenuItem"
        Me._RemoveHiddenHyphensContextMenuItem.Size = New System.Drawing.Size(237, 22)
        Me._RemoveHiddenHyphensContextMenuItem.Text = "Remove Hidden Hyphens &Only"
        '
        '_HyperlinksContextMenuItem
        '
        Me._HyperlinksContextMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InsertEditRemoveHyperlinkContextMenuItem, Me._RemoveHyperLinksContextMenuItem, Me._KeepHypertextContextSeparator, Me._KeepHyperTextWhenRemovingLinksContextMenuItem})
        Me._HyperlinksContextMenuItem.Enabled = False
        Me._HyperlinksContextMenuItem.Name = "_HyperlinksContextMenuItem"
        Me._HyperlinksContextMenuItem.Size = New System.Drawing.Size(223, 22)
        Me._HyperlinksContextMenuItem.Text = "Hyperlinks"
        Me._HyperlinksContextMenuItem.Visible = False
        '
        '_InsertEditRemoveHyperlinkContextMenuItem
        '
        Me._InsertEditRemoveHyperlinkContextMenuItem.Enabled = False
        Me._InsertEditRemoveHyperlinkContextMenuItem.Name = "_InsertEditRemoveHyperlinkContextMenuItem"
        Me._InsertEditRemoveHyperlinkContextMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.K), System.Windows.Forms.Keys)
        Me._InsertEditRemoveHyperlinkContextMenuItem.Size = New System.Drawing.Size(266, 22)
        Me._InsertEditRemoveHyperlinkContextMenuItem.Text = "&Insert/Edit/Remove ..."
        Me._InsertEditRemoveHyperlinkContextMenuItem.Visible = False
        '
        '_RemoveHyperLinksContextMenuItem
        '
        Me._RemoveHyperLinksContextMenuItem.Enabled = False
        Me._RemoveHyperLinksContextMenuItem.Name = "_RemoveHyperLinksContextMenuItem"
        Me._RemoveHyperLinksContextMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.F9), System.Windows.Forms.Keys)
        Me._RemoveHyperLinksContextMenuItem.Size = New System.Drawing.Size(266, 22)
        Me._RemoveHyperLinksContextMenuItem.Text = "&Remove All..."
        Me._RemoveHyperLinksContextMenuItem.Visible = False
        '
        '_KeepHypertextContextSeparator
        '
        Me._KeepHypertextContextSeparator.Name = "_KeepHypertextContextSeparator"
        Me._KeepHypertextContextSeparator.Size = New System.Drawing.Size(263, 6)
        Me._KeepHypertextContextSeparator.Visible = False
        '
        '_KeepHyperTextWhenRemovingLinksContextMenuItem
        '
        Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Enabled = False
        Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Name = "_KeepHyperTextWhenRemovingLinksContextMenuItem"
        Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Size = New System.Drawing.Size(266, 22)
        Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Text = "&Keep hypertext when removing links"
        Me._KeepHyperTextWhenRemovingLinksContextMenuItem.Visible = False
        '
        '_SpellChecker
        '
        Me._SpellChecker.Dictionary = Nothing
        '
        '_FontButton
        '
        Me._FontButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._FontButton.Image = CType(resources.GetObject("_FontButton.Image"), System.Drawing.Image)
        Me._FontButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FontButton.Name = "_FontButton"
        Me._FontButton.Size = New System.Drawing.Size(23, 22)
        Me._FontButton.Text = "Font"
        '
        '_FontColorButton
        '
        Me._FontColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._FontColorButton.Image = CType(resources.GetObject("_FontColorButton.Image"), System.Drawing.Image)
        Me._FontColorButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FontColorButton.Name = "_FontColorButton"
        Me._FontColorButton.Size = New System.Drawing.Size(23, 22)
        Me._FontColorButton.Text = "Text Color"
        Me._FontColorButton.ToolTipText = "Text Color"
        '
        '_BoldButton
        '
        Me._BoldButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._BoldButton.Image = CType(resources.GetObject("_BoldButton.Image"), System.Drawing.Image)
        Me._BoldButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._BoldButton.Name = "_BoldButton"
        Me._BoldButton.Size = New System.Drawing.Size(23, 22)
        Me._BoldButton.Text = "Bold"
        '
        '_ItalicButton
        '
        Me._ItalicButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ItalicButton.Image = CType(resources.GetObject("_ItalicButton.Image"), System.Drawing.Image)
        Me._ItalicButton.ImageTransparentColor = System.Drawing.Color.White
        Me._ItalicButton.Name = "_ItalicButton"
        Me._ItalicButton.Size = New System.Drawing.Size(23, 22)
        Me._ItalicButton.Text = "Italic"
        '
        '_UnderlineButton
        '
        Me._UnderlineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._UnderlineButton.Image = CType(resources.GetObject("_UnderlineButton.Image"), System.Drawing.Image)
        Me._UnderlineButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._UnderlineButton.Name = "_UnderlineButton"
        Me._UnderlineButton.Size = New System.Drawing.Size(23, 22)
        Me._UnderlineButton.Text = "Underline"
        '
        '_AlignmentToolStripSeparator
        '
        Me._AlignmentToolStripSeparator.Name = "_AlignmentToolStripSeparator"
        Me._AlignmentToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_LeftAlignButton
        '
        Me._LeftAlignButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._LeftAlignButton.Image = CType(resources.GetObject("_LeftAlignButton.Image"), System.Drawing.Image)
        Me._LeftAlignButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LeftAlignButton.Name = "_LeftAlignButton"
        Me._LeftAlignButton.Size = New System.Drawing.Size(23, 22)
        Me._LeftAlignButton.Text = "Align Text Left"
        Me._LeftAlignButton.ToolTipText = "Align Text Left"
        '
        '_CenterAlignButton
        '
        Me._CenterAlignButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._CenterAlignButton.Image = CType(resources.GetObject("_CenterAlignButton.Image"), System.Drawing.Image)
        Me._CenterAlignButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._CenterAlignButton.Name = "_CenterAlignButton"
        Me._CenterAlignButton.Size = New System.Drawing.Size(23, 22)
        Me._CenterAlignButton.Text = "Center Text"
        Me._CenterAlignButton.ToolTipText = "Center Text"
        '
        '_RightAlignButton
        '
        Me._RightAlignButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._RightAlignButton.Image = CType(resources.GetObject("_RightAlignButton.Image"), System.Drawing.Image)
        Me._RightAlignButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._RightAlignButton.Name = "_RightAlignButton"
        Me._RightAlignButton.Size = New System.Drawing.Size(23, 22)
        Me._RightAlignButton.Text = "Align Text Right"
        Me._RightAlignButton.ToolTipText = "Align Text Right"
        '
        '_InsertToolStripSeparator
        '
        Me._InsertToolStripSeparator.Name = "_InsertToolStripSeparator"
        Me._InsertToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_SpellCheckButton
        '
        Me._SpellCheckButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SpellCheckButton.Enabled = False
        Me._SpellCheckButton.Image = CType(resources.GetObject("_SpellCheckButton.Image"), System.Drawing.Image)
        Me._SpellCheckButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SpellCheckButton.Name = "_SpellCheckButton"
        Me._SpellCheckButton.Size = New System.Drawing.Size(23, 22)
        Me._SpellCheckButton.Text = "Spell Check"
        '
        '_SearchToolStripSeparator
        '
        Me._SearchToolStripSeparator.Name = "_SearchToolStripSeparator"
        Me._SearchToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_FindNextButton
        '
        Me._FindNextButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._FindNextButton.Enabled = False
        Me._FindNextButton.Image = CType(resources.GetObject("_FindNextButton.Image"), System.Drawing.Image)
        Me._FindNextButton.ImageTransparentColor = System.Drawing.Color.Black
        Me._FindNextButton.Name = "_FindNextButton"
        Me._FindNextButton.Size = New System.Drawing.Size(23, 22)
        Me._FindNextButton.Text = "Find Next"
        '
        '_TopToolStrip
        '
        Me._TopToolStrip.AutoSize = False
        Me._TopToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TopToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._TopToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._NewFileButton, Me._SaveButton, Me._FileSeparator, Me._FontColorButton, Me._BackColorButton, Me._FontToolStripSeparator, Me._FontButton, Me._FontNameComboBox, Me._FontSizeComboBox, Me._BoldButton, Me._ItalicButton, Me._UnderlineButton, Me._AlignmentToolStripSeparator, Me._LeftAlignButton, Me._CenterAlignButton, Me._RightAlignButton, Me._SearchToolStripSeparator, Me._FindButton, Me._FindNextButton, Me._InsertToolStripSeparator, Me._InsertPictureButton, Me._InsertSymbolButton, Me._HyphenToolStripSeparator, Me._HyphenateButton, Me._SpellCheckButton, Me._SpellingToolStripSeparator, Me._ListDropDownButton, Me._LinksToolStripSeparator, Me._HyperlinkButton, Me._LastToolStripSeparator})
        Me._TopToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._TopToolStrip.Name = "_TopToolStrip"
        Me._TopToolStrip.Size = New System.Drawing.Size(769, 25)
        Me._TopToolStrip.TabIndex = 1
        Me._TopToolStrip.Text = "Top Tool Strip"
        '
        '_NewFileButton
        '
        Me._NewFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._NewFileButton.Image = CType(resources.GetObject("_NewFileButton.Image"), System.Drawing.Image)
        Me._NewFileButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._NewFileButton.Name = "_NewFileButton"
        Me._NewFileButton.Size = New System.Drawing.Size(23, 22)
        Me._NewFileButton.Text = "New File"
        '
        '_SaveButton
        '
        Me._SaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SaveButton.Image = CType(resources.GetObject("_SaveButton.Image"), System.Drawing.Image)
        Me._SaveButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SaveButton.Name = "_SaveButton"
        Me._SaveButton.Size = New System.Drawing.Size(23, 22)
        Me._SaveButton.Text = "Save document"
        '
        '_FileSeparator
        '
        Me._FileSeparator.Name = "_FileSeparator"
        Me._FileSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_BackColorButton
        '
        Me._BackColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._BackColorButton.Image = CType(resources.GetObject("_BackColorButton.Image"), System.Drawing.Image)
        Me._BackColorButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._BackColorButton.Name = "_BackColorButton"
        Me._BackColorButton.Size = New System.Drawing.Size(23, 22)
        Me._BackColorButton.Text = "Background Color"
        '
        '_FontToolStripSeparator
        '
        Me._FontToolStripSeparator.Name = "_FontToolStripSeparator"
        Me._FontToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_FontNameComboBox
        '
        Me._FontNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me._FontNameComboBox.DropDownHeight = 182
        Me._FontNameComboBox.DropDownWidth = 155
        Me._FontNameComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me._FontNameComboBox.IntegralHeight = False
        Me._FontNameComboBox.MaxDropDownItems = 9
        Me._FontNameComboBox.Name = "_FontNameComboBox"
        Me._FontNameComboBox.Size = New System.Drawing.Size(121, 25)
        Me._FontNameComboBox.Sorted = True
        Me._FontNameComboBox.ToolTipText = "Font Name"
        '
        '_FontSizeComboBox
        '
        Me._FontSizeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me._FontSizeComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me._FontSizeComboBox.Items.AddRange(New Object() {"8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "38", "48", "72"})
        Me._FontSizeComboBox.Name = "_FontSizeComboBox"
        Me._FontSizeComboBox.Size = New System.Drawing.Size(75, 25)
        Me._FontSizeComboBox.ToolTipText = "Font Size"
        '
        '_FindButton
        '
        Me._FindButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._FindButton.Enabled = False
        Me._FindButton.Image = CType(resources.GetObject("_FindButton.Image"), System.Drawing.Image)
        Me._FindButton.ImageTransparentColor = System.Drawing.Color.Black
        Me._FindButton.Name = "_FindButton"
        Me._FindButton.Size = New System.Drawing.Size(23, 22)
        Me._FindButton.Text = "Find/Replace"
        Me._FindButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_InsertPictureButton
        '
        Me._InsertPictureButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._InsertPictureButton.Image = CType(resources.GetObject("_InsertPictureButton.Image"), System.Drawing.Image)
        Me._InsertPictureButton.ImageTransparentColor = System.Drawing.Color.Black
        Me._InsertPictureButton.Name = "_InsertPictureButton"
        Me._InsertPictureButton.Size = New System.Drawing.Size(23, 22)
        Me._InsertPictureButton.Text = "Insert Picture"
        Me._InsertPictureButton.ToolTipText = "Insert Picture"
        '
        '_InsertSymbolButton
        '
        Me._InsertSymbolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._InsertSymbolButton.Image = CType(resources.GetObject("_InsertSymbolButton.Image"), System.Drawing.Image)
        Me._InsertSymbolButton.ImageTransparentColor = System.Drawing.Color.Black
        Me._InsertSymbolButton.Name = "_InsertSymbolButton"
        Me._InsertSymbolButton.Size = New System.Drawing.Size(23, 22)
        Me._InsertSymbolButton.Text = "Insert Symbol"
        '
        '_HyphenToolStripSeparator
        '
        Me._HyphenToolStripSeparator.Name = "_HyphenToolStripSeparator"
        Me._HyphenToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_HyphenateButton
        '
        Me._HyphenateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._HyphenateButton.Image = CType(resources.GetObject("_HyphenateButton.Image"), System.Drawing.Image)
        Me._HyphenateButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._HyphenateButton.Name = "_HyphenateButton"
        Me._HyphenateButton.Size = New System.Drawing.Size(23, 22)
        Me._HyphenateButton.Text = "Hyphenate"
        '
        '_SpellingToolStripSeparator
        '
        Me._SpellingToolStripSeparator.Name = "_SpellingToolStripSeparator"
        Me._SpellingToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_ListDropDownButton
        '
        Me._ListDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ListDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ListNoneMenuItem, Me._ListBulletMenuItem, Me._ListNumbersMenuItem, Me._ListLowerCaseAlphaMenuItem, Me._ListUpperCaseAlphaMenuItem, Me._ListLowerCaseRomanMenuItem, Me._ListUpperCaseRomanMenuItem})
        Me._ListDropDownButton.Image = CType(resources.GetObject("_ListDropDownButton.Image"), System.Drawing.Image)
        Me._ListDropDownButton.ImageTransparentColor = System.Drawing.Color.Black
        Me._ListDropDownButton.Name = "_ListDropDownButton"
        Me._ListDropDownButton.Size = New System.Drawing.Size(29, 22)
        Me._ListDropDownButton.Text = "Set List Style"
        Me._ListDropDownButton.ToolTipText = "Set List Style"
        '
        '_ListNoneMenuItem
        '
        Me._ListNoneMenuItem.Name = "_ListNoneMenuItem"
        Me._ListNoneMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListNoneMenuItem.Tag = "-1"
        Me._ListNoneMenuItem.Text = "(None)"
        '
        '_ListBulletMenuItem
        '
        Me._ListBulletMenuItem.Name = "_ListBulletMenuItem"
        Me._ListBulletMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListBulletMenuItem.Tag = "0"
        Me._ListBulletMenuItem.Text = "● (Bullets)"
        '
        '_ListNumbersMenuItem
        '
        Me._ListNumbersMenuItem.Name = "_ListNumbersMenuItem"
        Me._ListNumbersMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListNumbersMenuItem.Tag = "1"
        Me._ListNumbersMenuItem.Text = "1, 2, 3, ..."
        '
        '_ListLowerCaseAlphaMenuItem
        '
        Me._ListLowerCaseAlphaMenuItem.Name = "_ListLowerCaseAlphaMenuItem"
        Me._ListLowerCaseAlphaMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListLowerCaseAlphaMenuItem.Tag = "2"
        Me._ListLowerCaseAlphaMenuItem.Text = "a, b, c, ..."
        '
        '_ListUpperCaseAlphaMenuItem
        '
        Me._ListUpperCaseAlphaMenuItem.Name = "_ListUpperCaseAlphaMenuItem"
        Me._ListUpperCaseAlphaMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListUpperCaseAlphaMenuItem.Tag = "3"
        Me._ListUpperCaseAlphaMenuItem.Text = "A, B, C, ..."
        '
        '_ListLowerCaseRomanMenuItem
        '
        Me._ListLowerCaseRomanMenuItem.Name = "_ListLowerCaseRomanMenuItem"
        Me._ListLowerCaseRomanMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListLowerCaseRomanMenuItem.Tag = "4"
        Me._ListLowerCaseRomanMenuItem.Text = "i, ii, iii, ..."
        '
        '_ListUpperCaseRomanMenuItem
        '
        Me._ListUpperCaseRomanMenuItem.Name = "_ListUpperCaseRomanMenuItem"
        Me._ListUpperCaseRomanMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ListUpperCaseRomanMenuItem.Tag = "5"
        Me._ListUpperCaseRomanMenuItem.Text = "I, II, III, ..."
        '
        '_LinksToolStripSeparator
        '
        Me._LinksToolStripSeparator.Name = "_LinksToolStripSeparator"
        Me._LinksToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        Me._LinksToolStripSeparator.Visible = False
        '
        '_HyperlinkButton
        '
        Me._HyperlinkButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._HyperlinkButton.Enabled = False
        Me._HyperlinkButton.Image = CType(resources.GetObject("_HyperlinkButton.Image"), System.Drawing.Image)
        Me._HyperlinkButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._HyperlinkButton.Name = "_HyperlinkButton"
        Me._HyperlinkButton.Size = New System.Drawing.Size(23, 22)
        Me._HyperlinkButton.Text = "Insert/Edit/Remove Hyperlink"
        Me._HyperlinkButton.Visible = False
        '
        '_LastToolStripSeparator
        '
        Me._LastToolStripSeparator.Name = "_LastToolStripSeparator"
        Me._LastToolStripSeparator.Size = New System.Drawing.Size(6, 25)
        '
        '_TopRuler
        '
        Me._TopRuler.BackColor = System.Drawing.Color.Transparent
        Me._TopRuler.BorderColor = System.Drawing.Color.Transparent
        Me._TopRuler.DefaultFontSize = 9.0!
        Me._TopRuler.DefaultFontSizeRange = 0.5!
        Me._TopRuler.Dock = System.Windows.Forms.DockStyle.Top
        Me._TopRuler.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TopRuler.Location = New System.Drawing.Point(0, 25)
        Me._TopRuler.Name = "_TopRuler"
        Me._TopRuler.PrintableWidth = 554
        Me._TopRuler.RulerWidth = 556
        Me._TopRuler.ScrollingOffset = 0
        Me._TopRuler.Size = New System.Drawing.Size(769, 20)
        Me._TopRuler.TabIndex = 2
        Me._TopRuler.TabPositions = New Integer(-1) {}
        Me._TopRuler.TabPositionsInUnits = New Single(-1) {}
        Me._TopRuler.TabsEnabled = True
        Me._TopRuler.ToolTipText = String.Empty
        Me._TopRuler.Units = isr.Controls.RichTextFormat.RulerUnitType.Inches
        Me._TopRuler.UsingSmartToolTips = True
        '
        '_RulerContextMenuStrip
        '
        Me._RulerContextMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InchesContextMenuItem, Me._CentimetersContextMenuItem})
        Me._RulerContextMenuStrip.Name = "RulerContextMenuStrip"
        Me._RulerContextMenuStrip.Size = New System.Drawing.Size(200, 48)
        '
        '_InchesContextMenuItem
        '
        Me._InchesContextMenuItem.Checked = True
        Me._InchesContextMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._InchesContextMenuItem.Name = "_InchesContextMenuItem"
        Me._InchesContextMenuItem.Size = New System.Drawing.Size(199, 22)
        Me._InchesContextMenuItem.Text = "Measure In &Inches"
        '
        '_CentimetersContextMenuItem
        '
        Me._CentimetersContextMenuItem.Name = "_CentimetersContextMenuItem"
        Me._CentimetersContextMenuItem.Size = New System.Drawing.Size(199, 22)
        Me._CentimetersContextMenuItem.Text = "Measure In &Centimeters"
        '
        'RichTextFormatControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ContextMenuStrip = Me._RulerContextMenuStrip
        Me.Controls.Add(Me._Rtb)
        Me.Controls.Add(Me._TopRuler)
        Me.Controls.Add(Me._TopToolStrip)
        Me.Name = "RichTextFormatControl"
        Me.Size = New System.Drawing.Size(769, 218)
        Me._ContextMenuStrip.ResumeLayout(False)
        Me._TopToolStrip.ResumeLayout(False)
        Me._TopToolStrip.PerformLayout()
        Me._RulerContextMenuStrip.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _SpellChecker As NetSpell.SpellChecker.Spelling
    Private WithEvents _Rtb As System.Windows.Forms.RichTextBox
    Private WithEvents _FontButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _FontColorButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _BoldButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _ItalicButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _UnderlineButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _AlignmentToolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _LeftAlignButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _CenterAlignButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _RightAlignButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _InsertToolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _SpellCheckButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _SearchToolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _FindNextButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _TopToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _FontNameComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _FindButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _ContextMenuStrip As System.Windows.Forms.ContextMenuStrip
    Private WithEvents _EditContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SelectAllContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _CopyContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _CutContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _PasteContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _UndoContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _RedoContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FontContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _BoldContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ItalicContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _UnderlineContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SearchContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FindContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FindNextContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SpellCheckContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _AlignmentContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _LeftAlignContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _CenterAlignContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _RightAlignContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ListContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _GeneralFontSettingsContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _FontSizeComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _InsertPictureButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _SpellingToolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _InsertContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _BackColorButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _FontToolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _HyphenationContextMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _HyphenateButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _SearchContextSeparator As ToolStripSeparator
    Private WithEvents _SpellCheckContextSeparator As ToolStripSeparator
    Private WithEvents _HyphenateTextContextMenuItem As ToolStripMenuItem
    Private WithEvents _RemoveHyphensContextMenuItem As ToolStripMenuItem
    Private WithEvents _RemoveHiddenHyphensContextMenuItem As ToolStripMenuItem
    Private WithEvents _ReplaceContextMenuItem As ToolStripMenuItem
    Private WithEvents _TopRuler As TextRuler
    Private WithEvents _RulerContextMenuStrip As ContextMenuStrip
    Private WithEvents _InchesContextMenuItem As ToolStripMenuItem
    Private WithEvents _CentimetersContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListNoneContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListBulletContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListNumberContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListLowerCaseAlphaContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListUpperCaseAlphaContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListLowerCaseRomanContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListUpperCaseRomanContextMenuItem As ToolStripMenuItem
    Private WithEvents _ListDropDownButton As ToolStripDropDownButton
    Private WithEvents _ListNoneMenuItem As ToolStripMenuItem
    Private WithEvents _ListBulletMenuItem As ToolStripMenuItem
    Private WithEvents _ListNumbersMenuItem As ToolStripMenuItem
    Private WithEvents _ListLowerCaseAlphaMenuItem As ToolStripMenuItem
    Private WithEvents _ListUpperCaseAlphaMenuItem As ToolStripMenuItem
    Private WithEvents _ListLowerCaseRomanMenuItem As ToolStripMenuItem
    Private WithEvents _ListUpperCaseRomanMenuItem As ToolStripMenuItem
    Private WithEvents _InsertSymbolButton As ToolStripButton
    Private WithEvents _InsertPictureContextMenuItem As ToolStripMenuItem
    Private WithEvents _InsertSymbolContextMenuItem As ToolStripMenuItem
    Private WithEvents _VerticalPositionContextMenuItem As ToolStripMenuItem
    Private WithEvents _SuperscriptContextMenuItem As ToolStripMenuItem
    Private WithEvents _SubscriptContextMenuItem As ToolStripMenuItem
    Private WithEvents _StrikeThroughContextMenuItem As ToolStripMenuItem
    Private WithEvents _HyperlinksContextMenuItem As ToolStripMenuItem
    Private WithEvents _InsertEditRemoveHyperlinkContextMenuItem As ToolStripMenuItem
    Private WithEvents _RemoveHyperLinksContextMenuItem As ToolStripMenuItem
    Private WithEvents _KeepHypertextContextSeparator As ToolStripSeparator
    Private WithEvents _KeepHyperTextWhenRemovingLinksContextMenuItem As ToolStripMenuItem
    Private WithEvents _HyperlinkButton As ToolStripButton
    Private WithEvents _HyphenToolStripSeparator As ToolStripSeparator
    Private WithEvents _LinksToolStripSeparator As ToolStripSeparator
    Private WithEvents _LastToolStripSeparator As ToolStripSeparator
    Private WithEvents _FindPreviousContextMenuItem As ToolStripMenuItem
    Private WithEvents _FileContextMenuItem As ToolStripMenuItem
    Private WithEvents _FileOpenContextMenuItem As ToolStripMenuItem
    Private WithEvents _FileSaveContextMenuItem As ToolStripMenuItem
    Private WithEvents _FileSaveAsContextMenuItem As ToolStripMenuItem
    Private WithEvents _FileCloseContextMenuItem As ToolStripMenuItem
    Private WithEvents _FileContextSeparator As ToolStripSeparator
    Private WithEvents _PrintContextMenuItem As ToolStripMenuItem
    Private WithEvents _PrintContextSeparator As ToolStripSeparator
    Private WithEvents _FileCloseContextSeparator As ToolStripSeparator
    Private WithEvents _PageSetupContextMenuItem As ToolStripMenuItem
    Private WithEvents _QuickPrintContextMenuItem As ToolStripMenuItem
    Private WithEvents _PrintPreviewContextMenuItem As ToolStripMenuItem
    Private WithEvents _SaveButton As ToolStripButton
    Friend WithEvents _FileSeparator As ToolStripSeparator
    Private WithEvents _NewFileButton As ToolStripButton
    Private WithEvents _NewFileContextMenuItem As ToolStripMenuItem
End Class
