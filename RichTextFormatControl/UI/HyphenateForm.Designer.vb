﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HyphenateForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HyphenateForm))
        Me._HyphenatedWordLabel = New System.Windows.Forms.Label()
        Me._WordTextBoxLabel = New System.Windows.Forms.Label()
        Me._HyphenateButton = New System.Windows.Forms.Button()
        Me._SkipButton = New System.Windows.Forms.Button()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._WordTextBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        '_HyphenatedWordLabel
        '
        resources.ApplyResources(Me._HyphenatedWordLabel, "_HyphenatedWordLabel")
        Me._HyphenatedWordLabel.Name = "_HyphenatedWordLabel"
        '
        '_WordTextBoxLabel
        '
        resources.ApplyResources(Me._WordTextBoxLabel, "_WordTextBoxLabel")
        Me._WordTextBoxLabel.Name = "_WordTextBoxLabel"
        '
        '_HyphenateButton
        '
        resources.ApplyResources(Me._HyphenateButton, "_HyphenateButton")
        Me._HyphenateButton.Name = "_HyphenateButton"
        Me._HyphenateButton.UseVisualStyleBackColor = True
        '
        '_SkipButton
        '
        resources.ApplyResources(Me._SkipButton, "_SkipButton")
        Me._SkipButton.Name = "_SkipButton"
        Me._SkipButton.UseVisualStyleBackColor = True
        '
        '_CancelButton
        '
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me._CancelButton, "_CancelButton")
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.UseVisualStyleBackColor = True
        '
        '_WordTextBox
        '
        resources.ApplyResources(Me._WordTextBox, "_WordTextBox")
        Me._WordTextBox.Name = "_WordTextBox"
        Me._WordTextBox.ReadOnly = True
        '
        'HyphenateForm
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me._CancelButton
        Me.Controls.Add(Me._WordTextBox)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._SkipButton)
        Me.Controls.Add(Me._HyphenateButton)
        Me.Controls.Add(Me._WordTextBoxLabel)
        Me.Controls.Add(Me._HyphenatedWordLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "HyphenateForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _WordTextBox As TextBox
    Private WithEvents _WordTextBoxLabel As Label
    Private WithEvents _HyphenatedWordLabel As Label
    Private WithEvents _CancelButton As Button
    Private WithEvents _SkipButton As Button
    Private WithEvents _HyphenateButton As Button
End Class
