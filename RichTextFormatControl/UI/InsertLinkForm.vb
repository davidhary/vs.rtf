﻿Imports System.Text.RegularExpressions
Imports isr.Controls.RichTextFormat.TextBoxExtensions

''' <summary> Form for viewing the insert link. </summary>
''' <remarks> (c) 2008 Razi Syed. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-10-17, 1.0.*">
''' https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex. 
''' https://www.codeproject.com/Articles/30799/Extended-RichTextBox </para></remarks>
Public Class InsertLinkForm

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <param name="customLinkInfo"> Information describing the custom link. </param>
    ''' <param name="exists">         True to exists. </param>
    ''' <param name="keepHypertext">  The keep hypertext. </param>
    Public Sub New(ByVal customLinkInfo As CustomLinkInfo, ByVal exists As Boolean, ByVal keepHypertext As Boolean)
        '   This call is required by the designer.
        Me.InitializeComponent()
        '   Initialize values
        Me.CustomLinkInfo = customLinkInfo
        With customLinkInfo
            Me._LinkCaptionTextBox.Text = .Text
            Me._HyperlinkTextBox.Text = .Hyperlink
        End With
        Me.Exists = exists
        Me._KeepHypertextCheckBox.Visible = exists : Me._KeepHypertextCheckBox.Enabled = exists
        If exists Then
            '   existing link
            Me._InsertOrUpdateButton.Text = "Update"
            Me._RemoveButton.Visible = True : Me._RemoveButton.Enabled = True
            Me._KeepHypertextCheckBox.Checked = keepHypertext
        End If
    End Sub

#End Region

#Region " PUBLIC COMPONENTS: PROPERTIES "

    ''' <summary> Input: Gets information describing the custom link. </summary>
    ''' <value> Information describing the custom link. </value>
    Public Property CustomLinkInfo As CustomLinkInfo   'INPUT/OUTPUT: Visible and hyperlink text

    ''' <summary> Output: Gets the link action. </summary>
    ''' <value> The link action. </value>
    Public Property LinkAction As InsertLinkActions

    ''' <summary> Output: Gets the preserve hypertext on remove. </summary>
    ''' <value> The keep hypertext. </value>
    Public Property KeepHypertext As Boolean

#End Region

#Region " PRIVATE COMPONENTS: PROPERTIES "

    ''' <summary> Gets or sets the exists. </summary>
    ''' <value> The exists. </value>
    Private Property Exists As Boolean = False

#End Region

#Region " PRIVATE COMPONENTS: EVENT PROCEDURES "

    ''' <summary> Inserts an or update button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InsertOrUpdateButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _InsertOrUpdateButton.Click
        '   prepare to insert/update link
        If Me.GetTextAndHyperlink() Then
            '   return with text, hyperlink, and action
            If Me.Exists Then
                '   pre-existing link
                Me.LinkAction = InsertLinkActions.Update
            Else
                '   new link
                Me.KeepHypertext = Me._KeepHypertextCheckBox.Checked
                Me.LinkAction = InsertLinkActions.Insert

            End If
            Me.DialogResult = DialogResult.OK
        Else
            '   error
            Me.DialogResult = DialogResult.None
        End If
    End Sub

    ''' <summary> Cancel button click. don't change anything. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _CancelButton.Click
        Me.LinkAction = InsertLinkActions.None : Me.DialogResult = DialogResult.Cancel
    End Sub

    ''' <summary> Removes the button click. Prepares to remove link </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RemoveButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _RemoveButton.Click
        Me.KeepHypertext = Me._KeepHypertextCheckBox.Checked
        Me.LinkAction = InsertLinkActions.Remove : Me.DialogResult = DialogResult.OK

    End Sub

    ''' <summary> Hyperlink scheme context menu strip item clicked. insert scheme. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tool strip item clicked event information. </param>
    Private Sub HyperlinkSchemeContextMenuStrip_ItemClicked(ByVal sender As Object, ByVal e As ToolStripItemClickedEventArgs) Handles _HyperlinkSchemeContextMenuStrip.ItemClicked

        Me._HyperlinkTextBox.Text = e.ClickedItem.Text & Me._HyperlinkTextBox.Text
    End Sub

    ''' <summary> Hyperlink text box text changed. Makes context menu available if hyperlink is missing a scheme.  </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HyperlinkTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _HyperlinkTextBox.TextChanged

        If Me.IsSchemePresent(Me._HyperlinkTextBox.Text) Then
            '   scheme present--no menu
            Me._HyperlinkTextBox.ContextMenuStrip = New ContextMenuStrip()
        Else
            '   no scheme--make menu available
            Me._HyperlinkTextBox.ContextMenuStrip = Me._HyperlinkSchemeContextMenuStrip
        End If

    End Sub

    ''' <summary> Links a caption text box key down. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub LinkCaptionTextBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles _LinkCaptionTextBox.KeyDown
        InsertSpecialCharacters(Me._LinkCaptionTextBox, e)
    End Sub

    ''' <summary> Hyperlink text box key down. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub HyperlinkTextBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles _HyperlinkTextBox.KeyDown
        InsertSpecialCharacters(Me._HyperlinkTextBox, e)
    End Sub

#End Region


#Region " PRIVATE COMPONENTS: PROCEDURES "

    ''' <summary> Query if 'textToValidate' is valid. </summary>
    ''' <param name="textToValidate"> The text to validate. </param>
    ''' <returns> <c>true</c> if valid; otherwise <c>false</c> </returns>
    Private Function IsValid(ByVal textToValidate As String) As Boolean
        '   make sure text/hyperlink is valid
        Return _
        Not String.IsNullOrEmpty(textToValidate.Trim()) _
            AndAlso Not Regex.IsMatch(textToValidate, "[\{\|\}]")
    End Function

    ''' <summary> Gets text and hyperlink. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function GetTextAndHyperlink() As Boolean
        '   get text and hyper-link if they are valid; return whether or not they are
        Dim Text As String = Me._LinkCaptionTextBox.Text.Trim(),
        Hyperlink As String = Me._HyperlinkTextBox.Text.Trim()
        If Me.IsValid(Text) AndAlso Me.IsValid(Hyperlink) Then
            '   valid--get values
            With Me.CustomLinkInfo
                .Text = Text : .Hyperlink = Hyperlink
            End With
            Return True
        Else
            '   invalid
            Beep()
            MessageBox.Show("Invalid visible and/or hyperlink text!",
            "Invalid Text!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If
    End Function

    ''' <summary> Query if 'Text' is scheme present. </summary>
    ''' <param name="text"> The text. </param>
    ''' <returns> <c>true</c> if scheme present; otherwise <c>false</c> </returns>
    Private Function IsSchemePresent(ByVal text As String) As Boolean
        '   check to see if a pre-defined scheme is already present
        For Each CMSItem As ToolStripItem In Me._HyperlinkSchemeContextMenuStrip.Items
            If TypeOf CMSItem IsNot ToolStripSeparator _
                AndAlso text.StartsWith(CMSItem.Text,
                    StringComparison.CurrentCultureIgnoreCase) Then
                '   known scheme found
                Return True
            End If
        Next CMSItem
        '   unknown scheme found?
        Return _
        text.Contains("://")
    End Function

#End Region

End Class

''' <summary> Values that represent link actions. </summary>
Public Enum InsertLinkActions
    None
    Insert
    Update
    Remove
End Enum