# Rich Text Format Control Project

A library for editing and printing Rich Text Format Files

## Status

Because this project has undergone significant cleanup, some featrures that worked on the original open source might have been broken.  Not all features have been tested and no unit tests have been implemented todate. 

* [Issues](https://bitbucket.org/davidhary/vs.rtf/issues?status=new&status=open)

## Getting Started

Clone the project to its relative path.

```
git clone git@bitbucket.org:davidhary/vs.richtextformatcontrol.git
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **Razi Syen**  
.. [Original Extended Rich Text Box](https://www.codeproject.com/Articles/30799/Extended-RichTextBox)
* **Robert Gustafson**  
.. [Exteded Original Extended Rich Text Box](https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex)  
.. [Ruler](https://www.codeproject.com/Tips/1226034/General-purpose-RULER-Control-for-Use-with-RICH-TE)  
.. [Rich Text Box Printing Extensions](https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex)
* **David Hary**  
..*Cleanup and upload to Bitbucket* - [ATE Coder](https://www.IntegratedScientificResources.com)

## License

This repository is licensed under the [MIT License](https://bitbucket.org/davidhary/vs.rtf/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com)
* [Razi Syen](https://www.codeproject.com/Articles/30799/Extended-RichTextBox) -- Extended Rich Text Box
* [Robert Gustafson](https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex) -- Extended Sayed's Rich Text Box
* [Robert Gustafson](https://www.codeproject.com/Tips/1226034/General-purpose-RULER-Control-for-Use-with-RICH-TE) -- Text Ruller
* [Robert Gustafson](https://www.codeproject.com/Articles/868653/EXTENDED-Version-of-Extended-Rich-Text-Box-RichTex) -- Print manager

## Revision Changes

* Version 2.0.7230	10/18/19	Cleanup and pass recommended Microsoft rules for code analysis.

